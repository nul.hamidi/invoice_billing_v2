<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('user')){
			redirect('auth/login');
		}
		//Do your magic here
		$this->load->model(array('Reports_model','Invoice_model'));

		
	}

	public function download_file($id_invoice)
	{
		error_reporting(0);
		$mpdf = new mPDF([
			'mode' => 'utf-8',
    		'format' => 'A4-L',
    		'default_font' => 'Times New Roman'
		]);

		$this->session->set_userdata('id_invoice',$id_invoice);
		$invoice_id = decrypt_url($id_invoice);
		/*$data['report_data'] 	= $this->Reports_model->data_report($invoice_id);
		$data['tagihan_otc']    = $this->Invoice_model->getListTagihan($invoice_id,'otc');
		$data['tagihan_mrc']    = $this->Invoice_model->getListTagihan($invoice_id,'mrc');
		$data['tagihan_cpe']    = $this->Invoice_model->getListTagihan($invoice_id,'cpe');*/

		$data['report_data'] 	= $this->Reports_model->data_report($invoice_id);
		$data_otc = $this->Invoice_model->getListTagihan($invoice_id,'otc','array');

		$new_otc = array();
		foreach ($data_otc as $val) {
			$new_otc[$val['nama_produk']][] = $val;

		}

		$data_mrc = $this->Invoice_model->getListTagihan($invoice_id,'mrc','array');

		$new_mrc = array();
		foreach ($data_mrc as $val) {
			$new_mrc[$val['nama_produk']][] = $val;

		}

		$data_cpe = $this->Invoice_model->getListTagihan($invoice_id,'cpe','array');

		$new_cpe = array();
		foreach ($data_cpe as $val) {
			$new_cpe[$val['nama_produk']][] = $val;

		}


		$data['tagihan_otc'] = $new_otc;
		$data['tagihan_mrc'] = $new_mrc;
		$data['tagihan_cpe'] = $new_cpe;
		$data['detail_inv_minus_data'] = $this->Invoice_model->get_data_all_detailminus($invoice_id);

		//$html = $this->load->view('welcome_message',[],true);
		$html = $this->load->view('report/all_report',$data,true);
		//$html .= $this->load->view('report/report_invoice',[],true);
		//$mpdf->AddPage();
		$mpdf->AddPage('P','','','','',12,12,40,25);
		$mpdf->WriteHTML($html);
		$mpdf->Output();

		//$this->load->view('report/all_report');
	}

	function show_report(){
		echo terbilang(8860323);
	}

	function preview_dok($id_invoice=null){
		$invoice_id = decrypt_url($id_invoice);
		//$this->session->set_userdata('id_invoice',$id_invoice);
		//print_r($id_invoice);die;
		$data['report_data'] 	= $this->Reports_model->data_report($invoice_id);
		$data_otc = $this->Invoice_model->getListTagihan($invoice_id,'otc','array');

		$new_otc = array();
		foreach ($data_otc as $val) {
			$new_otc[$val['nama_produk']][] = $val;

		}

		$data_mrc = $this->Invoice_model->getListTagihan($invoice_id,'mrc','array');

		$new_mrc = array();
		foreach ($data_mrc as $val) {
			$new_mrc[$val['nama_produk']][] = $val;

		}

		$data_cpe = $this->Invoice_model->getListTagihan($invoice_id,'cpe','array');

		$new_cpe = array();
		foreach ($data_cpe as $val) {
			$new_cpe[$val['nama_produk']][] = $val;

		}


		$data['tagihan_otc'] = $new_otc;
		$data['tagihan_mrc'] = $new_mrc;
		$data['tagihan_cpe'] = $new_cpe;

		//$data['tagihan_otc']    = $this->Invoice_model->getListTagihan($invoice_id,'otc');
		//$data['tagihan_mrc']    = $this->Invoice_model->getListTagihan($invoice_id,'mrc');
		//$data['tagihan_cpe']    = $this->Invoice_model->getListTagihan($invoice_id,'cpe');
		$data['detail_inv_minus_data'] = $this->Invoice_model->get_data_all_detailminus(decrypt_url($id_invoice));
		//print_r($data['report_data']);die;
		$this->load->view('report/preview',$data);
	}


	function test(){
		$no_surat = $this->custom_lib->new_generate_no_inv();
		echo $no_surat;
	}

	function import_data_proinv(){
		/*$config['allowed_types'] = 'xls|xlsx|xl';
		$config['max_size'] = '20000';
		

		$file_name="";
		$this->upload->initialize($config);
		$this->upload->do_upload('input-file');
		$upload_data  = $this->upload->data();*/

		ini_set('memory_limit',-1);
        ini_set('MAX_EXECUTION_TIME', 0);

		$data =array();
		$json = array();
		$config['upload_path'] = './file_manager/upload/excel';
		$config['allowed_types'] = 'xls|xlsx|xl';
		$config['max_size'] = '20000';
		$input = $this->input->post('input-file');
		//print_r($_FILES[$input]);
		$new_name = time().'_'.$_FILES[$input]['name'];
		$config['file_name'] = $new_name;

		//exit();
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload($this->input->post('input-file'))){
        	
            /*$data = array('error' => $this->upload->display_errors());*/
            $json = array(
				'code' => '0005',
				'message' => $this->upload->display_errors(),
				
			);
        
        } else {
        	
            $upload_data  = $this->upload->data();

			$file = $upload_data['full_path'];

			require_once APPPATH.'third_party/PHPExcel_/PHPExcel.php';
			
			$excelreader = new PHPExcel_Reader_Excel2007();
			$loadexcel = $excelreader->load($file); 
			$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

			
			//cek jika ada sudah ada di db & excel
			$duplicate = array();
			$x= 1;
			foreach($sheet as $row){
				if($x>1){

					$duplicate[] = $row['K'];
				}else{

				}
				$x++;
			}

			$this->db->select('invoice_id, kode_invoice');
			$this->db->from('invoice');
			$this->db->where_in('kode_invoice',$duplicate);
			$pengecekan = $this->db->get()->result();

			if(count($pengecekan)>0){
				unlink("./file_manager/upload/excel/".$new_name);
				$json = array(
					'rc' => '0008',
					'message' => 'Terdapat Duplicate Data Kode Invoice!',
				);
			}else{
			
				$this->db->trans_start();


					$no = 1;
					foreach($sheet as $row){
							//remove header
						if($no >1){

							///D3.400/HK200/TSAT/
							$data= array(
								'pelanggan_id' 	=> $row['A'], 
								'no_wo' 		=> $row['C'], 
							   	'nama_project' 	=> $row['D'], 
								'kode_project' 	=> $row['E'], 
								'lama_bulan' 	=> $row['F'], 
							   	'start_date' 	=> $row['G'],
								'end_date' 		=> $row['H'],
								'nilai_kontrak' => preg_replace("/[,\s]/", '', $row['I']),	 
							);

							$project_id = $this->Invoice_model->insert_project($data);

							$last_project_id = $this->db->insert_id();
							$no_surat = $this->custom_lib->new_generate_sp();
							$no_invoice = $this->custom_lib->new_generate_no_inv($last_project_id);
							$user = $this->session->userdata('user');
							$data_inv = array(
								'project_id' 	=> $last_project_id,
								'no_invoice' 	=> $no_invoice,
								'no_pjk'		=> $row['C'],
								'lama_hari' 	=> $row['L']?$row['L']:20,
								'no_surat' 		=> $no_surat,
								'created_by' 	=> $user['m_karyawan_id'],
								'created_date' 	=> date('d-m-y h:i:s'),
								//'kode_invoice' 	=> $this->custom_lib->new_generate_kode_inv(),
								'kode_invoice' 	=> $row['K'],
							);

							$invoice_id = $this->Invoice_model->insert_invoice($data_inv);
							$last_invoice_id = $this->db->insert_id();

							$data_inv_bank = array(
								'invoice_id' 	=> $last_invoice_id,
								'bank_id' 		=> $row['J'],
							);
							$invoice_bank_id = $this->Invoice_model->insert_invoice_bank($data_inv_bank);

						}
						$no++;
					}
				

					$this->db->trans_complete();
					if($this->db->trans_status() === FALSE){
						$json = array(
							'rc' => '0001',
							'message' => 'Terjadi kesalahan Database & File!',
						);
					}else{

						unlink("./file_manager/upload/excel/".$new_name);
						$json = array(
							'rc' => '0000',
							'message' => 'Data berhasil diimport',
						);
					}

				}

        }

		
    	echo json_encode(array($json));
	}

	function import_data_detinv(){

		ini_set('memory_limit',-1);
        ini_set('MAX_EXECUTION_TIME', 0);

		$data =array();
		$json = array();
		$config['upload_path'] = './file_manager/upload/excel';
		$config['allowed_types'] = 'xls|xlsx|xl';
		$config['max_size'] = '20000';
		$input = $this->input->post('input-file');
		//print_r($_FILES[$input]);
		$new_name = time().'_'.$_FILES[$input]['name'];
		$config['file_name'] = $new_name;
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload($this->input->post('input-file'))){
        	
            /*$data = array('error' => $this->upload->display_errors());*/
            $json = array(
				'rc' => '0005',
				'message' => $this->upload->display_errors(),
				
			);
        
        } else {
        	
            $upload_data  = $this->upload->data();

			$file = $upload_data['full_path'];

			require_once APPPATH.'third_party/PHPExcel_/PHPExcel.php';
			
			$excelreader = new PHPExcel_Reader_Excel2007();
			$loadexcel = $excelreader->load($file); 
			$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
			
			$no = 1;
			$this->db->trans_start();

				foreach($sheet as $row){
						//remove header
					if($no >1){
						//echo "sdfsdfsdf ".$row['A']."<br>";

						$invoice_id = $this->Invoice_model->get_data_invoice_kode($row['A']);
						
						/*$data[]= array(
						   	'invoice_id' 	=> $invoice_id, 
						   	'kode_invoice' 	=> $row['A'], 
						   	'deskripsi' 	=> $row['B'], 
						   	'jumlah' 		=> $row['C'], 
						   	'speed' 		=> $row['D'], 
						   	'tgl_awal' 		=> $row['E'],
							'tgl_akhir' 		=> $row['F'],
							'otc' => preg_replace("/[,\s]/", '', $row['G']),
							'mtc' => preg_replace("/[,\s]/", '', $row['H']),
							'cpe' => preg_replace("/[,\s]/", '', $row['I']),
							'jml_biaya' => ( ($row['C'] * preg_replace("/[,\s]/", '', $row['G'])) + ($row['C'] * preg_replace("/[,\s]/", '', $row['H'])) + ($row['C'] * preg_replace("/[,\s]/", '', $row['I'])) )
						);*/
						//$query = $this->Invoice_model->insert_invoice_detail_plus($data);
						$otc = preg_replace("/[,\s]/", '', $row['G']);
						$mtc = preg_replace("/[,\s]/", '', $row['H']);
						$cpe = preg_replace("/[,\s]/", '', $row['I']);
						
						$data= array(
						   	'invoice_id' 	=> $invoice_id->invoice_id, 
						   	'kode_invoice' 	=> $row['A'], 
						   	'deskripsi' 	=> $row['B'], 
						   	'jumlah' 		=> $row['C'], 
						   	'speed' 		=> $row['D'], 
						   	'tgl_awal' 		=> $row['E'],
							'tgl_akhir' 	=> $row['F'],
							'otc' => $otc,
							'mtc' => $mtc,
							'cpe' => $cpe,
							'jml_biaya' => ( ((float)$row['C'] * (float)$otc) + ((float)$row['C'] * (float)$mtc) + ((float)$row['C'] * (float)$cpe) )
						);

						$this->Invoice_model->insert_invoice_detail_plus($data);

						$this->custom_lib->akumulasi_total_bayar($invoice_id->invoice_id);

					}
					$no++;
				}
				

			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE){
				$json = array(
					'rc' => '0001',
					'message' => 'Terjadi kesalahan Database & File!',
				);
			}else{
				unlink("./file_manager/upload/excel/".$new_name);
				$json = array(
					'rc' => '0000',
					'message' => 'Data berhasil diimport',
				);
			}
			
        }

		
    	echo json_encode(array($json));
	}


}

/* End of file report.php */
/* Location: ./application/controllers/report.php */