<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tagihan extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('user')){
			redirect('auth/login');
		}
		//Do your magic here
		$this->load->model('Tagihan_model');
	}
	public function index($project_id, $id=null)
	{
		$data['active_menu'] = 'tagihan';
		$data['status']	= "tagihan";
		$data['css'] 			= array(
			'plugins/sweet-alert/sweetalert.css',
			'plugins/select2/select2.min.css',
			'plugins/datepicker/datepicker3.css',
		); // css tambahan
		$data['js']				= array(
			'plugins/sweet-alert/sweetalert.min.js',
			'plugins/select2/select2.full.min.js',
			'plugins/datepicker/bootstrap-datepicker.js',
			'js/jquery.validate.js',
			'plugins/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js',
		); // js tambahan
		$data['id_project'] = $project_id;
		$data['dictinct_bulan'] = $this->Tagihan_model->dictinct_bulan($project_id);
		
		if ($id == 'flush') {
			$this->session->unset_userdata('session.search');
			$this->session->unset_userdata('session.bulan');
		}

		

		$this->template->load('template','tagihan/view_tagihan',$data);
	}

	public function get_tagihan(){
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
		$bulan_tagih = $this->input->get("tagihan");
        $id_project = decrypt_url($this->input->get('data'));

		$session_search = $this->session->userdata('session.search');
		$session_bulan = $this->session->userdata('session.bulan');

		/*if($session_search==null){
			echo "true";
		}else{
			echo "false";
		}
		exit();*/

		$data_post = array(
			array(
				'search' => $session_search,
				'bulan' => $session_bulan,
				
			)
		);
		
		

        $tagihan = $this->Tagihan_model->get_datatables($id_project, $data_post,$bulan_tagih);

        $data = array();
        $no=1;
        foreach($tagihan as $in) {
        	$id_node = encrypt_url($in->node_id);
        	$project_id = encrypt_url($in->project_id);
        	$tagihan_id = encrypt_url($in->tagihan_id);
			$tag_id = $in->tagihan_id;
			$row = array();
				
			$aksi       =   'sum_data()';
			$checkbox_ = " <input type='checkbox' onchange='$aksi' 
				id='$tag_id' name='check[]' data-id='$tag_id'
				value='$tag_id'>";

			if((int)$in->invoice_id>0){
				$checkbox_ = " <i class='fa fa-check-square-o'></i>";
			}else{
				$checkbox_ = " <input type='checkbox' onchange='$aksi' 
				id='$tag_id' name='check[]' data-id='$tag_id'
				value='$tag_id'>";
			}

			//$input = "<input type='checkbox' name='tagihan_id[]' class='checkbox' data-tagihan='$tagihan_id' data-checkbox='$in->tagihan_id'>";
			//$row[] = $no;
			$biaya = $in->jumlah_biaya;
			$ppn = ($in->jumlah_biaya * 10 /100 );
			$total = ($biaya + $ppn);

			$row[] = $checkbox_;
			$row[] = $in->nama_deskripsi;
	        $row[] = $in->produk;
	        $row[] = strtoupper($in->type_biaya);
	        $row[] = rewrite_date($in->start_date) .' s/d '.  rewrite_date($in->end_date);
	        $row[] = "Rp. " . number_format($biaya, 0, ",", ".");
	        $row[] = "Rp. " . number_format($ppn, 0, ",", ".");
	        $row[] = "Rp. " . number_format($total, 0, ",", ".");
                
            $data[] = $row;
            $no++;
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->Tagihan_model->count_all($id_project, $data_post,$bulan_tagih),
	        "recordsFiltered" => $this->Tagihan_model->count_filtered($id_project, $data_post,$bulan_tagih),
            "data" => $data,
        );
        echo json_encode($output);
        exit();
	}

}

/* End of file tagihan.php */
/* Location: ./application/controllers/tagihan.php */