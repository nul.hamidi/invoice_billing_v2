<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('user')){
			redirect('auth/login');
		}
		//Do your magic here
		$this->load->model(array('Project_model','Pelanggan_model','Tagihan_model'));
	}
	public function index()
	{
		$data['active_menu'] = 'project';
		$data['project'] = $this->Project_model->get_project();
		$data['css'] 			= array(
			'plugins/sweet-alert/sweetalert.css',
			'plugins/select2/select2.min.css'
		); // css tambahan
		$data['js']				= array(
			'plugins/sweet-alert/sweetalert.min.js',
			'plugins/select2/select2.full.min.js',
			'extension/bootstrap-filestyle-2.1.0/src/bootstrap-filestyle.min.js'
		); // js tambahan

		$this->template->load('template','project/view_project',$data);
	}

	public function get_project(){
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
        $project = $this->Project_model->get_datatables();



        $data = array();
        $no=1;
        foreach($project as $in) {
        	$id_project = encrypt_url($in->project_id);
        
			$row = array();
			
			$permission = $this->session->userdata('permission');		
			
			$update = anchor(site_url('project/update_data/'.$id_project), '<button data-toggle="tooltip" data-placement="bottom" title="edit" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></button>', array('class' => ''))."&nbsp;";
	       	$hapus = '<button data-toggle="tooltip" data-placement="bottom" title="Hapus" class="btn btn-danger btn-xs hapus-project" value='.$id_project.'><i class="fa fa-trash"></i></button>&nbsp;';
	       	$detail ="<a href='".base_url('node/data_node/'.$id_project)."' data-toggle='tooltip' data-placement='bottom' title='Detail'><button class='btn btn-info btn-xs'><i class='fa fa-eye' ></i> </button></a>"; 
			
			$jumlah_node = $this->Project_model->getJumlahNode($in->project_id)->jumlah_node;
			$tag 	 	 = $this->Tagihan_model->getJumlahTagihan($in->project_id);

			$bulan_tertagih = $tag->bulan_tertagih?$tag->bulan_tertagih+1:0;
			$jumlah_bulan 	= $tag->jumlah_bulan?$tag->jumlah_bulan+1:0;
			//'nama_alias','nama_pelanggan','divisi','nama_pic','alamat_surat','alamat_invoice','npwp'
			//$row[] = $no;
	        $row[] = $in->nama_alias;
	        $row[] = $in->no_wo;
	        $row[] = $in->nama_project;
	        $row[] = rewrite_date($in->start_date,'d-m-y').' - '. rewrite_date($in->end_date,'d-m-y');
			$row[] = number_format($in->nilai_kontrak,0,',','.');
			$row[] = $jumlah_node;
			$row[] = $bulan_tertagih."/".$jumlah_bulan." bln";
			$row[] = "Rp. ".number_format($tag->biaya_tertagih,0,',','.')." / Rp. ".number_format($tag->jumlah_biaya,0,',','.');
	        
	        //$row[] = '';
           	$row[] =$update.$hapus.$detail;
                
            $data[] = $row;
            $no++;
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->Project_model->count_all(),
	        "recordsFiltered" => $this->Project_model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
        exit();
	}

	public function tambah()
	{

        $data = array();
        $data['title'] 			= 'Tambah Project';
		$data['action'] 		= 'add';
		$data['active_menu'] 	= 'add_project';
		$data['action_url'] 	= site_url('project/saves_insert');
		$data['css'] 			= array(
			'plugins/sweet-alert/sweetalert.css',
			'plugins/select2/select2.min.css',
			'plugins/datepicker/datepicker3.css',
		); // css tambahan
		$data['js']				= array(
			'plugins/sweet-alert/sweetalert.min.js',
			'plugins/select2/select2.full.min.js',
			'plugins/datepicker/bootstrap-datepicker.js',
			'js/jquery.validate.js',
			'plugins/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js'
		); // js tambahan
        $data['pelanggan'] = $this->Pelanggan_model->get_pelanggan_select();
		
		$this->template->load('template','project/tambah_project', $data);
	}


	public function simpan_project(){
		$pattern_nominal = "/[.\s]/";
		$nilai_kontrak = preg_replace($pattern_nominal, '', $this->input->post('nilai_kontrak'));
		$data = array(
			'pelanggan_id' => decrypt_url($this->input->post('pilih_pelanggan')),
			'nama_project' => $this->input->post('nama_project'),
			'no_wo' => $this->input->post('no_wo'),
			'kode_project' => $this->input->post('kode_project'),
			//'lama_bulan' => $this->input->post('lama_bulan'),
			'start_date' => date('y-m-d',strtotime($this->input->post('start_kontrak'))),
			'end_date' => date('y-m-d',strtotime($this->input->post('end_kontrak'))),
			'nilai_kontrak' => $nilai_kontrak
		);


		if($this->Project_model->insert_record_project($data)){
			$last_id = encrypt_url($this->db->insert_id());
            $this->session->set_flashdata('message', 'Data Berhasil Di Tambahkan');
            $this->session->set_flashdata('status', 'success');
                
        	redirect('node/data_node/'.$last_id);
        }else{
            $this->session->set_flashdata('message', 'Data Gagal Di Tambahkan');
            $this->session->set_flashdata('status', 'danger');
        	redirect('project/data_project');
        }
	}

	public function ubah($id_project=null)
	{
		if($id_project==null || $id_project=="" ){
			redirect('project/data_project');
		}
		

        $data = array();
        $data['title'] 			= 'Ubah Project';
		$data['action'] 		= 'edit';
		$data['status'] 		= 'project';
		$data['id_project'] 	= $id_project;
		$data['active_menu'] 	= 'project';
		$data['action_url'] 	= site_url('project/save_update/').$id_project;
		$data['css'] 			= array(
			'plugins/sweet-alert/sweetalert.css',
			'plugins/select2/select2.min.css',
			'plugins/datepicker/datepicker3.css',
		); // css tambahan
		$data['js']				= array(
			'plugins/sweet-alert/sweetalert.min.js',
			'plugins/select2/select2.full.min.js',
			'plugins/datepicker/bootstrap-datepicker.js',
			'js/jquery.validate.js',
			'plugins/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js'
		); // js tambahan
       

		$decrypt_id = decrypt_url($id_project);

		$project = $this->Project_model->get_data_project_byid($decrypt_id);

		$data['pelanggan'] = $this->Pelanggan_model->get_pelanggan_select();

		$data['nama_project'] = $project->nama_project;
		$data['no_wo'] = $project->no_wo;
		$data['kode_project'] = $project->kode_project;
		$data['pelanggan_id'] = encrypt_url($project->pelanggan_id);
		$data['start_kontrak'] = date('d-m-Y',strtotime($project->start_date));
		$data['end_kontrak'] = date('d-m-Y',strtotime($project->end_date));
		$data['nilai_kontrak'] =  $project->nilai_kontrak;
		

		$this->template->load('template','project/ubah_project', $data);
	}

	public function update_project($id_project){

		if($id_project==null || $id_project=="" ){
			redirect('project/data_project');
		}

		$decrypt_id = decrypt_url($id_project);

		$pattern_nominal = "/[.\s]/";
		$nilai_kontrak = preg_replace($pattern_nominal, '', $this->input->post('nilai_kontrak'));
		
		$data = array(
			'pelanggan_id' => decrypt_url($this->input->post('pilih_pelanggan')),
			'nama_project' => $this->input->post('nama_project'),
			'no_wo' => $this->input->post('no_wo'),
			'kode_project' => $this->input->post('kode_project'),
			//'lama_bulan' => $this->input->post('lama_bulan'),
			'start_date' => date('y-m-d',strtotime($this->input->post('start_kontrak'))),
			'end_date' => date('y-m-d',strtotime($this->input->post('end_kontrak'))),
			'nilai_kontrak' => $nilai_kontrak,
		);

		if($this->Project_model->update_data_project($data, $decrypt_id)){

			$this->session->set_flashdata('message', 'Data Berhasil Di Perbaharui');
            $this->session->set_flashdata('status', 'success');

                
        }else{
            $this->session->set_flashdata('message', 'Data Gagal Di Perbaharui');
            $this->session->set_flashdata('status', 'danger');
        }
        redirect('project/data_project');
	}



	public function delete_project(){
		$decrypt_id = decrypt_url($this->input->post('data'));

		if ($this->Project_model->delete_project_byid($decrypt_id) === FALSE){
			$message="Delete project gagal!";
			$status=true;
			$rc="0005";
		}else{
			$message="Delete project Berhasil";
			$status=false;
			$rc="0000";
		}


		$this->session->set_flashdata('message', $message);
		$this->session->set_flashdata('status', $status);

		$data['rc'] = $rc;
		$data['message'] = $message;

		echo json_encode($data);
	}

	public function import_excel_project(){
		ini_set('memory_limit',-1);
        ini_set('MAX_EXECUTION_TIME', 0);

		$data =array();
		$config['upload_path'] = './file_manager/upload/excel';
		$config['allowed_types'] = 'xls|xlsx|xls|csv';
		$config['max_size'] = '20000';
		$input = $this->input->post('input-file-project');
		//print_r($_FILES[$input]);
		$new_name = time().'_'.$_FILES[$input]['name'];
		$config['file_name'] = $new_name;

		//exit();
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload($this->input->post('input-file-project'))){
        	
            /*$data = array('error' => $this->upload->display_errors());*/
            $json = array(
				'code' => '0005',
				'message' => $this->upload->display_errors(),
				
			);
        
        } else {
        	
            $upload_data  = $this->upload->data();

			$file = $upload_data['full_path'];

			require_once APPPATH.'third_party/PHPExcel_/PHPExcel.php';
			
			$excelreader = new PHPExcel_Reader_Excel2007();
			$loadexcel = $excelreader->load($file); 
			$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

			
			//cek jika ada sudah ada di db & excel
			$duplicate = array();
			$x= 1;
			foreach($sheet as $row){
				if($x>1){

					$duplicate[] = $row['C'];
				}else{

				}
				$x++;
			}

			$this->db->select('project_id, no_wo');
			$this->db->from('project');
			$this->db->where_in('no_wo',$duplicate);
			$pengecekan = $this->db->get()->result();

			if(count($pengecekan)>0){
				unlink("./file_manager/upload/excel/".$new_name);

				$output_data_duplicate = array();
				$nomor = 1;
				$start_array = 0;
				foreach ($pengecekan as $value) {
					if($value->no_wo==$duplicate[$start_array]){
						$output_data_duplicate[] = array(
							'no' => $nomor,
							'no_wo_duplicate' => $value->no_wo
						);
					}
					$nomor++;
					$start_array++;
				}

				$json = array(
					'rc' => '0008',
					'message' => 'Terdapat Duplicate Data No Wo!',
					'data' => $output_data_duplicate
				);
			}else{
			
				$this->db->trans_start();


					$no = 1;
					foreach($sheet as $row){
							//remove header
						if($no >1){

							///D3.400/HK200/TSAT/
							$data= array(
								'pelanggan_id' 	=> $row['A'], 
								'no_wo' 		=> $row['C'], 
							   	'nama_project' 	=> $row['B'], 
								'kode_project' 	=> $row['D'], 
								/*'lama_bulan' 	=> $row['E'],*/ 
							   	'start_date' 	=> $row['E'],
								'end_date' 		=> $row['F'],
								'nilai_kontrak' => preg_replace("/[,\s]/", '', $row['G']),	 
							);

							$this->Project_model->insert_project($data);

						}
						$no++;
					}
				

					$this->db->trans_complete();
					if($this->db->trans_status() === FALSE){
						$json = array(
							'rc' => '0001',
							'message' => 'Terjadi kesalahan Database & File!',
						);
					}else{

						unlink("./file_manager/upload/excel/".$new_name);
						$json = array(
							'rc' => '0000',
							'message' => 'Data berhasil diimport',
						);
					}

				}

        }

		
    	echo json_encode(array($json));
	}


}

/* End of file project.php */
/* Location: ./application/controllers/project.php */