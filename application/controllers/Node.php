<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Node extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('user')){
			redirect('auth/login');
		}
		//Do your magic here
		$this->load->model(array('Node_model','Produk_model','Tagihan_model'));
	}
	public function index($id_project)
	{
		$data['active_menu'] = 'bank';
		$data['status']	= "node";
		$data['css'] 			= array(
			'plugins/sweet-alert/sweetalert.css',
			'plugins/select2/select2.min.css',
			'plugins/datepicker/datepicker3.css',
		); // css tambahan
		$data['js']				= array(
			'plugins/sweet-alert/sweetalert.min.js',
			'plugins/select2/select2.full.min.js',
			'plugins/datepicker/bootstrap-datepicker.js',
			'js/jquery.validate.js',
			'plugins/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js',
			'extension/bootstrap-filestyle-2.1.0/src/bootstrap-filestyle.min.js'
		); // js tambahan

		$data['id_project'] = $id_project;
		$data['action_url_add'] = site_url('node/saves_insert/'.$id_project);
		$data['action_url_update'] = site_url('node/update_data/');
		$data['select_produk'] = $this->Produk_model->get_produk_select();

		$this->template->load('template','node/view_node',$data);
	}

	public function get_node(){
		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
        $id_project = decrypt_url($this->input->get('data'));
        $node = $this->Node_model->get_datatables($id_project);



        $data = array();
        $no=1;
        foreach($node as $in) {
        	$id_node = encrypt_url($in->node_id);
        	$id_produk = encrypt_url($in->produk_id);
        
			$row = array();

			$tgl_tagih = date('d-m-Y',strtotime($in->tgl_tagih));
			$tgl_off = date('d-m-Y',strtotime($in->tgl_off));
			
			$permission = $this->session->userdata('permission');		
		
	       	$hapus = '<button data-toggle="tooltip" data-placement="bottom" title="Hapus" class="btn btn-danger btn-xs hapus-node" value='.$id_node.'><i class="fa fa-trash"></i></button>&nbsp;';
			$update = "<button class='btn btn-success btn-xs btn-update' data-toggle='modal' data-target='#Editnode' data-nama_produk='$in->nama_produk' data-speed='$in->speed' data-otc='$in->otc' data-mrc='$in->mrc' data-cpe='$in->cpe' data-value='$id_node' data-tgl_tagih='$tgl_tagih' data-tgl_off='$tgl_off' data-deskripsi='$in->nama_deskripsi' data-produk='$id_produk' ><i class='fa fa-edit'></i></button>&nbsp;";


			//'nama_alias','nama_pelanggan','divisi','nama_pic','alamat_surat','alamat_invoice','npwp'
			//$row[] = $no;
			$row[] = $in->nama_deskripsi;
			$row[] = $in->produk;
	        $row[] = $in->speed;
	        $row[] = date('d-m-Y',strtotime($in->tgl_tagih));
	        $row[] = date('d-m-Y',strtotime($in->tgl_off));
	        $row[] = "Rp. " . number_format($in->otc, 0, ",", ".");
	        $row[] = "Rp. " . number_format($in->mrc, 0, ",", ".");
	        $row[] = "Rp. " . number_format($in->cpe, 0, ",", ".");

	        
	        //$row[] = '';
           	$row[] =$update.$hapus;
                
            $data[] = $row;
            $no++;
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->Node_model->count_all($id_project),
	        "recordsFiltered" => $this->Node_model->count_filtered($id_project),
            "data" => $data,
        );
        echo json_encode($output);
        exit();
	}

	public function simpan_node($id_project){
		$pattern = "/[-\s]/";
		$tgl_awal = preg_split($pattern, $this->input->post('tgl_tagih'));
		$date_akhir = preg_split($pattern, $this->input->post('tgl_off'));

		$rewrite_date_awal = $tgl_awal[2].'-'.$tgl_awal[1].'-'.$tgl_awal[0];
		$rewrite_date_akhir = $date_akhir[2].'-'.$date_akhir[1].'-'.$date_akhir[0];

		$pattern_nominal = "/[.\s]/";
	
		$otc = preg_replace($pattern_nominal, '', $this->input->post('otc'));
		$mrc = preg_replace($pattern_nominal, '', $this->input->post('mrc'));
		$cpe = preg_replace($pattern_nominal, '', $this->input->post('cpe'));
	
		$id_produk = decrypt_url($this->input->post('pilih_produk'));

		$data = array(
			'nama_deskripsi' => $this->input->post('deskripsi'),
			'speed' => $this->input->post('speed'),
			'tgl_tagih' => $rewrite_date_awal,
			'tgl_off' => $rewrite_date_akhir,
			'otc' => $otc,
			'mrc' => $mrc,
			'cpe' => $cpe,
			'produk_id' => $id_produk,
			'nama_produk' => $this->Produk_model->get_nama_produk($id_produk),
			'project_id' => decrypt_url($id_project),

		);


		if($this->Node_model->insert_record_node($data)){
            $this->session->set_flashdata('message', 'Data Berhasil Di Tambahkan');
            $this->session->set_flashdata('status', 'success');

        }else{
            $this->session->set_flashdata('message', 'Data Gagal Di Tambahkan');
            $this->session->set_flashdata('status', 'danger');
          
        }

        //exit();
       redirect('node/data_node/'.$id_project);
	}

	public function update_node(){
		
		$id_node = decrypt_url($this->input->post('value'));
		$id_project = decrypt_url($this->input->post('segmen'));
		$id_produk = decrypt_url($this->input->post('pilih_produk'));


		$pattern = "/[-\s]/";
		$tgl_awal = preg_split($pattern, $this->input->post('tgl_tagih'));
		$date_akhir = preg_split($pattern, $this->input->post('tgl_off'));

		$rewrite_date_awal = $tgl_awal[2].'-'.$tgl_awal[1].'-'.$tgl_awal[0];
		$rewrite_date_akhir = $date_akhir[2].'-'.$date_akhir[1].'-'.$date_akhir[0];

		$pattern_nominal = "/[.\s]/";
	
		$otc = preg_replace($pattern_nominal, '', $this->input->post('otc'));
		$mrc = preg_replace($pattern_nominal, '', $this->input->post('mrc'));
		$cpe = preg_replace($pattern_nominal, '', $this->input->post('cpe'));
	
		$id_produk = decrypt_url($this->input->post('pilih_produk'));

		$data = array(
			'nama_deskripsi' => $this->input->post('deskripsi'),
			'speed' => $this->input->post('speed'),
			'tgl_tagih' => $rewrite_date_awal,
			'tgl_off' => $rewrite_date_akhir,
			'otc' => $otc,
			'mrc' => $mrc,
			'cpe' => $cpe,
			'produk_id' => $id_produk,
			'nama_produk' => $this->Produk_model->get_nama_produk($id_produk),
			'project_id' => $id_project,

		);

       
		if($this->Node_model->update_data_node($data, $id_node)){

			$this->session->set_flashdata('message', 'Data Berhasil Di Perbaharui');
            $this->session->set_flashdata('status', 'success');

                
        }else{
            $this->session->set_flashdata('message', 'Data Gagal Di Perbaharui');
            $this->session->set_flashdata('status', 'danger');
        }
       redirect('node/data_node/'.encrypt_url($id_project));

	}

	public function delete_node(){
		$decrypt_id = decrypt_url($this->input->post('data'));

		if ($this->Node_model->delete_node_byid($decrypt_id) === FALSE){
			$message="Delete node gagal!";
			$status=true;
			$rc="0005";
		}else{
			$message="Delete node Berhasil";
			$status=false;
			$rc="0000";
		}


		$this->session->set_flashdata('message', $message);
		$this->session->set_flashdata('status', $status);

		$data['rc'] = $rc;
		$data['message'] = $message;

		echo json_encode($data);
	}

	function generate_tagihan($project_id=""){
		//error_reporting(0);
		//$id_project = decrypt_url($project_id);
		$id_project = decrypt_url($this->input->post('data'));
		
		$tagihan_temp = $this->Node_model->get_node_by_projectID($id_project);

		$array_nama_produk = array();
		foreach ($tagihan_temp as $value) {
			$array_nama_produk['regroup_by_name'][] = array(
				'nama_produk' => $value->nama_produk,
				'bulan_bayar' => $this->custom_lib->prorate($value->tgl_tagih, $value->tgl_off, $value->otc,$value->mrc,$value->cpe),
			); 
		}

		//print_r($array_nama_produk['regroup_by_name']);die;
		//print_r($array_nama_produk['regroup_by_name'][0]['bulan_bayar']);die;
		
		$TglNode = $this->Node_model->getTglTagihNode($id_project);
		//print_r($TglTagihNode);
		$tgl_node_arr = $this->custom_lib->getDateNode($TglNode->tgl_tagih,$TglNode->tgl_off);
		//print_r($tgl_node_arr);die;
		$output = "";
		$output.="<table class=table table-striped table-bordered table-hover id=table-generate cellspacing=0 width=100%>";
		
		foreach ($array_nama_produk['regroup_by_name'] as $p) {
			
			$output.="	<thead>";
			$output.=	"<tr>";
			$output.=	"<th class='text-center' width='100px'>Produk</th>";
			$output.=	"<th class='text-center'>Biaya</th>";
			
			foreach ($p['bulan_bayar'] as $val) {

				$output.=	"<th class='text-center'>".$val['bulan']."</th>";
			}
		
			$output.=		"</tr>";
			$output.="	</thead>";
			$output.=		"<tr >";
			$output.=		"<td rowspan='3' >".$p['nama_produk']."</td>";
			$output.=		"<td class='text-center'>OTC</td>";
			foreach ($p['bulan_bayar'] as $val) {
				
				$output.=	"<td align='right'>".number_format($val['biaya_otc'],0,',','.')."</td>";
			}
			$output.=		"</tr>";
			$output.=		"<tr >";
			$output.=		"<td class='text-center'>MRC</td>";
			
			foreach ($p['bulan_bayar'] as $val) {
				
				$output.=	"<td align='right'>".number_format($val['biaya_mtc'],0,',','.')."</td>";
			}
			$output.=		"</tr>";
			$output.=		"</tr>";
			$output.=		"<tr >";
			$output.=		"<td class='text-center'>CPE</td>";
			foreach ($p['bulan_bayar'] as $val) {
				
				$output.=	"<td align='right'>".number_format($val['biaya_cpe'],0,',','.')."</td>";
			}
			$output.=		"</tr>";
			
		}
		$output.="	</table>";

		
		$data=array(
			'status' => true,
			'message' => "Success Generate Data",
			'output' => $output	
		);

		echo json_encode($data);
		//echo$output;
		
	}

	public function simpan_generate_tagihan(){
		
	
		$project_id = decrypt_url($this->input->post('data'));
		
		$tagihan_temp = $this->Node_model->get_node_by_projectID($project_id);

		$array_nama_produk = array();
		foreach ($tagihan_temp as $value) {
			$array_nama_produk['regroup_by_name'][] = array(
				'node_id' 		=> $value->node_id,
				'nama_produk' 	=> $value->nama_produk,
				'project_id' 	=> $value->project_id,
				'bulan_bayar' 	=> $this->custom_lib->prorate($value->tgl_tagih, $value->tgl_off, $value->otc,$value->mrc,$value->cpe),
			); 
		}
		
		//echo "<pre>";
		//print_r($array_nama_produk);
		//echo "</pre>";die;
		
		//merge data dulu
		$data =array();
		$i=0;
		foreach ($array_nama_produk['regroup_by_name'] as $p) {

			foreach ($p['bulan_bayar'] as $tag){
			
				if($i==0){
					$data[] = array(
						'project_id' => $p['project_id'],
						'node_id'=>$p['node_id'],
						'nama_produk' => $p['nama_produk'],	
						'otc' => $tag['biaya_otc'],
						'cpe' => $tag['biaya_cpe'],
						'mrc' => $tag['biaya_mtc'],
						'bulan' => $tag['bulan'],
						'start_date' => $tag['start_date'],
						'end_date' => $tag['end_date']
					);
				}else if($i>0){
					$data[] = array(
						'project_id' => $p['project_id'],
						'node_id'=>$p['node_id'],
						'nama_produk' => $p['nama_produk'],	
						'otc' => $tag['biaya_otc'],
						'cpe' => $tag['biaya_cpe'],
						'mrc' => $tag['biaya_mtc'],
						'bulan' => $tag['bulan'],
						'start_date' => $tag['start_date'],
						'end_date' => $tag['end_date']
					);

				}				
				
			}
		 	$i++;
		}
		
		$user = $this->session->userdata('user');
		// check tagihan untuk project tsb
		$check_tagihan = $this->Tagihan_model->CheckTagihanByProjectID($project_id);

		if($check_tagihan->num_rows()>0){
			// clear data yg belum d generate invoice
			$this->Tagihan_model->clear_tagihan_ungenerate($project_id);
			//kasih kondisi filter jika ot,cpe,mrc > 0
			$new_array = array();
			foreach ($data as $value) {
				
				if((int) $value['otc']>0){
					$otc = "otc";
					$check_tag = $this->Tagihan_model->CheckTagihanByBiaya($value['node_id'],$otc,$value['bulan']); 
					
					if($check_tag->num_rows()==0){
						$new_array[] = array(
							'project_id'=>$value['project_id'],
							'node_id'=>$value['node_id'],
							'deskripsi' => $value['nama_produk'],
							'type_biaya' => 'otc',
							'jumlah_biaya' => $value['otc'],
							'bulan' => $value['bulan'],
							'start_date' => $value['start_date'],
							'end_date' => $value['end_date'],
							'created_date' => date('Y-m-d h:i:s'),
							'created_by' => $user['m_karyawan_id']
	
						);
					}
					

				}

				if((int) $value['mrc']>0){
					$mrc = "mrc";
					$check_tag_mrc = $this->Tagihan_model->CheckTagihanByBiaya($value['node_id'],$mrc,$value['bulan']); 
					
					if($check_tag_mrc->num_rows()==0){
						$new_array[] = array(
							'project_id'=>$value['project_id'],
							'node_id'=>$value['node_id'],
							'deskripsi' => $value['nama_produk'],
							'type_biaya' => 'mrc',
							'jumlah_biaya' => $value['mrc'],
							'bulan' => $value['bulan'],
							'start_date' => $value['start_date'],
							'end_date' => $value['end_date'],
							'created_date' => date('Y-m-d h:i:s'),
							'created_by' => $user['m_karyawan_id']
						);
					}
					
				}

				if((int) $value['cpe']>0){
					$cpe = "cpe";
					$check_tag_cpe = $this->Tagihan_model->CheckTagihanByBiaya($value['node_id'],$cpe,$value['bulan']); 
					if($check_tag_cpe->num_rows()==0){
						$new_array[] = array(
							'project_id'=>$value['project_id'],
							'node_id'=>$value['node_id'],
							'deskripsi' => $value['nama_produk'],
							'type_biaya' => 'cpe',
							'jumlah_biaya' => $value['cpe'],
							'bulan' => $value['bulan'],
							'start_date' => $value['start_date'],
							'end_date' => $value['end_date'],
							'created_date' => date('Y-m-d h:i:s'),
							'created_by' => $user['m_karyawan_id']
						);
					}
					

				}

				

			}

		}else{

			//kasih kondisi filter jika ot,cpe,mrc > 0
			$new_array = array();
			foreach ($data as $value) {

				if((int) $value['otc']>0){
					
					$new_array[] = array(
						'project_id'=>$value['project_id'],
						'node_id'=>$value['node_id'],
						'deskripsi' => $value['nama_produk'],
						'type_biaya' => 'otc',
						'jumlah_biaya' => $value['otc'],
						'bulan' => $value['bulan'],
						'start_date' => $value['start_date'],
						'end_date' => $value['end_date'],
						'created_date' => date('Y-m-d h:i:s'),
						'created_by' => $user['m_karyawan_id']

					);

				}

				if((int) $value['cpe']>0){
					
					$new_array[] = array(
						'project_id'=>$value['project_id'],
						'node_id'=>$value['node_id'],
						'deskripsi' => $value['nama_produk'],
						'type_biaya' => 'cpe',
						'jumlah_biaya' => $value['cpe'],
						'bulan' => $value['bulan'],
						'start_date' => $value['start_date'],
						'end_date' => $value['end_date'],
						'created_date' => date('Y-m-d h:i:s'),
						'created_by' => $user['m_karyawan_id']
					);

				}

				if((int) $value['mrc']>0){
					
					$new_array[] = array(
						'project_id'=>$value['project_id'],
						'node_id'=>$value['node_id'],
						'deskripsi' => $value['nama_produk'],
						'type_biaya' => 'mrc',
						'jumlah_biaya' => $value['mrc'],
						'bulan' => $value['bulan'],
						'start_date' => $value['start_date'],
						'end_date' => $value['end_date'],
						'created_date' => date('Y-m-d h:i:s'),
						'created_by' => $user['m_karyawan_id']
					);

				}

			}

		}
		
		
	

		// echo"<pre";
		// print_r($new_array);
		// echo"</pre>";
		// exit();
		$post_data = $this->Node_model->simpan_generate_tagihan_batch($new_array, $project_id);
		if($post_data===false){
			$json=array(
				'status' => false,
				'message' => "Gagal simpan tagihan",
				'rc' => '0005'	
			);

			
		}else{
			$json=array(
				'status' => true,
				'message' => "sukses simpan tagihan",
				'rc' => '0000'	
			);
		}
		
		echo json_encode($json);
	}


	public function import_excel_node(){
		ini_set('memory_limit',-1);
        ini_set('MAX_EXECUTION_TIME', 0);

		$data =array();
		$config['upload_path'] = './file_manager/upload/excel';
		$config['allowed_types'] = 'xls|xlsx|xls|csv';
		$config['max_size'] = '20000';
		$input = $this->input->post('input-file-node');
		//print_r($_FILES[$input]);
		$new_name = time().'_'.$_FILES[$input]['name'];
		$config['file_name'] = $new_name;

		//exit();
		$this->upload->initialize($config);
		if ( ! $this->upload->do_upload($this->input->post('input-file-node'))){
        	
            /*$data = array('error' => $this->upload->display_errors());*/
            $json = array(
				'code' => '0005',
				'message' => $this->upload->display_errors(),
				
			);
        
        } else {
        	
            $upload_data  = $this->upload->data();

			$file = $upload_data['full_path'];

			require_once APPPATH.'third_party/PHPExcel_/PHPExcel.php';
			
			$excelreader = new PHPExcel_Reader_Excel2007();
			$loadexcel = $excelreader->load($file); 
			$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

	
			
				$this->db->trans_start();


					$no = 1;
					foreach($sheet as $row){
							//remove header
						if($no >1){

							///D3.400/HK200/TSAT/
							$data= array(
								'produk_id' => $row['A'], 
							   	'nama_deskripsi' => $row['B'], 
								'nama_produk' => $this->get_nama_produk($row['A']), 
								'speed' => $row['C'], 
							   	'tgl_tagih' => $row['D'],
								'tgl_off' => $row['E'],
								'otc' => preg_replace("/[,\s]/", '', $row['F']),	 
								'mrc' => preg_replace("/[,\s]/", '', $row['G']),	 
								'cpe' => preg_replace("/[,\s]/", '', $row['H']),
								'project_id' => decrypt_url($this->input->post('project_id')) 
							);

							$this->Node_model->insert_node($data);

						}
						$no++;
					}
				

					$this->db->trans_complete();
					if($this->db->trans_status() === FALSE){
						$json = array(
							'rc' => '0001',
							'message' => 'Terjadi kesalahan Database & File!',
						);
					}else{

						unlink("./file_manager/upload/excel/".$new_name);
						$json = array(
							'rc' => '0000',
							'message' => 'Data berhasil diimport',
						);
					}

			

        }

		
    	echo json_encode(array($json));
	}


	public function get_nama_produk($id_produk){
		$this->db->select('nama_produk');
		$this->db->where('produk_id',$id_produk);
		$query = $this->db->get('node');

		if($query->num_rows()>0){
			$data = $query->row();
			$output= $data->nama_produk;

		}else{
			$output= '';
		}
		return $output;

	}
}

/* End of file node.php */
/* Location: ./application/controllers/node.php */