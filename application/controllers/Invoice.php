<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		if(!$this->session->userdata('user')){
			redirect('auth/login');
		}
		//error_reporting(0);
		$this->load->library('upload');
		$this->load->model(array('Tagihan_model','Devisi_model','Invoice_model','Detail_inv_model','Bank_model','Invoice_tagihan_model'));
		
	}
	
	public function index()
	{

		
	
		$data['title'] 				= 'home';
		$data['active_menu'] 		= 'dashboard';
		$data['css'] 				= array(); // css tambahan
		$data['js']					= array(); // js tambahan
		$this->template->load('template','home/dashboard', $data);
	}


	public function lists()
	{
        
		//$data['URL'] 			= base_url('Home/LoadUnbilData/'.$status);
		
		$data['title'] 			= 'invoice';
		$data['active_menu'] 	= 'list_invoice';
		$data['css'] 			= array(
			'plugins/sweet-alert/sweetalert.css','plugins/select2/select2.min.css'
		); // css tambahan
		$data['js']				= array(
			'plugins/sweet-alert/sweetalert.min.js','plugins/select2/select2.full.min.js',
			'extension/bootstrap-filestyle-2.1.0/src/bootstrap-filestyle.min.js'
		); // js tambahan
		$data['URL'] = site_url('invoice/get_invoice');
		$data['status'] = '';
		$this->template->load('template','invoice/list_invoice', $data);
	}




	public function get_invoice(){

		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
        $invoice = $this->Invoice_model->get_datatables();



        $data = array();
        $no=1;
        foreach($invoice as $in) {
        	$id_invoice = encrypt_url($in->invoice_id);
        	$ib_invoice = encrypt_url($in->ib_id);
        
			$row = array();
			
			$permission = $this->session->userdata('permission');
			

			if($in->is_approved=='1'){
				//$approval ="<button class='btn btn-success btn-xs'><i class='fa fa-check'></i> Approved</button>"; 
				$approval ="<a class='approve_invoice' id='".$in->invoice_id."&".$in->is_approved."' data-toggle='tooltip' title='Klik untuk Approve'><button class='btn btn-success btn-xs'><i class='fa fa-check' ></i> Approved</button></a>"; 
				//$update = '';
				$hapus 	= '';
			}else{
				if($permission){
					if (in_array('Approver Invoice', $permission)){
						$approval ="<a class='approve_invoice' id='".$in->invoice_id."&".$in->is_approved."' data-toggle='tooltip' title='Klik untuk Approve'><button class='btn btn-info btn-xs'><i class='fa fa-lock' ></i> Pending</button></a>"; 
					}else{
						$approval ="<a ><button class='btn btn-info btn-xs'><i class='fa fa-lock' ></i> Pending</button></a>"; 
					}
				}else{
					$approval ="<a ><button class='btn btn-info btn-xs'><i class='fa fa-lock' ></i> Pending</button></a>"; 
				}
				
				$hapus = '<button data-toggle="tooltip" data-placement="bottom" title="Hapus" class="btn btn-danger btn-xs hapus-invoice" value='.$id_invoice.' data-ib='.$ib_invoice.'><i class="fa fa-trash"></i></button>&nbsp;';
			}

			$update = anchor(site_url('invoice/update_data/'.$id_invoice), '<button data-toggle="tooltip" data-placement="bottom" title="edit" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></button>', array('class' => ''))."&nbsp;";

			//$update = anchor(site_url('invoice/update_data/'.$id_invoice), '<button data-toogle="tooltip" data-placement="bottom" title="Edit" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></button>', array('class' => ''))."&nbsp;";
	        //$hapus = '<button data-toogle="tooltip" data-placement="bottom" title="Hapus" class="btn btn-danger btn-xs hapus-invoice" value='.$id_invoice.'><i class="fa fa-trash"></i></button>&nbsp;';
			//$cetak = anchor(site_url('report/index/'.$id_invoice), '<button data-toogle="tooltip" data-placement="bottom" title="Cetak Invoice" class="btn btn-warning btn-xs"><i class="fa fa-print"></i></button>', array('class' => ''))."&nbsp;";
			
			
	        
			$cetak = "<a target='blank' href='".base_url('report/preview/'.$id_invoice)."' data-toggle='tooltip' data-placement='bottom' title='Cetak'><button  class='btn btn-warning btn-xs'><i class='fa fa-print'></i></button></a>&nbsp;";
			$detail ="<a href='".base_url('invoice/detail_invoice/'.$id_invoice)."' data-toggle='tooltip' data-placement='bottom' title='Detail'><button class='btn btn-info btn-xs'><i class='fa fa-eye' ></i> </button></a>"; 

			$row[] = $no;
	        $row[] = $in->no_invoice;
	        // $row[] = $in->nama_pelanggan;
            // $row[] = $in->nama_project;
            $row[] = $in->no_wo;
            $row[] = $in->tgl_invoice==null?'':date('d-m-Y',strtotime($in->tgl_invoice));
            $row[] = $in->tgl_jthtempo==null?'':date('d-m-Y',strtotime($in->tgl_jthtempo));
            $row[] = "Rp. " . number_format($in->nominal, 0, ",", ".");
            $row[] = $approval;
            $row[] =$update.$hapus.$cetak.$detail;
                
            $data[] = $row;
            $no++;
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->Invoice_model->count_all(),
	        "recordsFiltered" => $this->Invoice_model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
        exit();
	}

	public function tambah()
	{

        $data = array();
        $data['title'] 			= 'Tambah Invoice';
		$data['action'] 		= 'add';
		$data['active_menu'] 	= 'add_invoice';
		$data['action_url'] 	= site_url('invoice/saves_insert');
		$data['list_divisi'] 	= $this->Devisi_model->get_data_devisi();
		$data['css'] 			= array(
			'plugins/datepicker/datepicker3.css',
			'plugins/select2/select2.min.css');
		$data['js']				= array(	// js tambahan
			'js/jquery.validate.js',
			'plugins/datepicker/bootstrap-datepicker.js',
			'plugins/select2/select2.full.min.js',
			'plugins/terbilang/terbilang.js',
			'plugins/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js',
        );
       	//$data['generate_no_inv'] = $this->generate_no_inv();
       	$data['generate_no_inv'] = '';
        //$data['generate_sp'] = $this->generate_sp();

        $get_data_wo = $this->Invoice_model->get_data_wo();
        $data['get_data_wo'] = $get_data_wo;
        $get_data_bank = $this->Bank_model->get_data_bank();
        $data['get_data_bank'] = $get_data_bank;
		
		$this->template->load('template','invoice/tambah_invoice', $data);
	}

	function generate_sp(){
		$data = '/D3.400/HK200/TSAT/'.date('m.Y');
		return $data;
	}

	public function get_data_wo_by_id($project_id){

		$decrypt_id = decrypt_url($project_id);

		$this->db->select('pjt.project_id, pjt.nama_project, pjt.no_wo, pjt.kode_project,  pjt.start_date, pjt.end_date, plg.pelanggan_id, plg.nama_pelanggan, plg.nama_alias, plg.divisi, plg.nama_pic, plg.alamat_surat, plg.alamat_invoice, plg.npwp');
        $this->db->from('project as pjt');
        $this->db->join('pelanggan as plg','plg.pelanggan_id = pjt.pelanggan_id','inner');
        $this->db->where('pjt.project_id',$decrypt_id);
        $query = $this->db->get()->row();


        if($query){
        	$data = array(
        		'project' => encrypt_url($query->project_id),
				'nama_project' => $query->nama_project,
				'no_wo' => $query->no_wo,
				'kode_project' => $query->kode_project,
				'start_date' => date('d-m-Y',strtotime($query->start_date)),
				'end_date' => date('d-m-Y',strtotime($query->end_date)),
				//'pelanggan_id' => $query->nama_project,
				'nama_pelanggan' => $query->nama_pelanggan,
				'nama_alias' => $query->nama_alias,
				'divisi' => $query->divisi,
				'nama_pic' => $query->nama_pic,
				'alamat_surat' => $query->alamat_surat,
				'alamat_invoice' => $query->alamat_invoice,
				'npwp' => $query->npwp,
        		'message' => 'sukses load data',
				'rc' => '0000'
        	);
        }else{
        	$data = array(
				'message' => 'gagal load data',
				'rc' => '0005'
			);
        }

		

		echo json_encode(array($data));
		 

	}
	

	public function calculate_total_bayar($id_invoice){


		$query_plus = $this->Tagihan_model->getNominalBiaya($id_invoice);
		
		$this->db->select('*');
		$this->db->where('invoice_id', $id_invoice);
		$query_minus = $this->db->get('invoice_detail_minus')->result();

		$hitung_nilai_penambahan =0;
        $hitung_nilai_pengurangan =0;
		foreach ($query_minus as $key) {

            if($key->type=="penambahan"){
                $jml_biaya = $key->jumlah*$key->biaya;
                $hitung_nilai_penambahan+=$jml_biaya;
            }else{
                $jml_biaya = $key->jumlah*$key->biaya;
                $hitung_nilai_pengurangan+=$jml_biaya;
            }

        }

        $kalkulasi_detail_min = $hitung_nilai_penambahan - $hitung_nilai_pengurangan;

        if($hitung_nilai_penambahan>0 && $hitung_nilai_pengurangan>0){
            
            $total_biaya = ($query_plus->jml_biaya + $hitung_nilai_penambahan) - $hitung_nilai_pengurangan;
        
        }else if($hitung_nilai_penambahan>0 && $hitung_nilai_pengurangan<=0){

            $total_biaya = ($query_plus->jml_biaya + $hitung_nilai_penambahan);

        }else if($hitung_nilai_penambahan<=0 && $hitung_nilai_pengurangan>0){

            $total_biaya = ($query_plus->jml_biaya - $hitung_nilai_pengurangan);
        
        }else if($hitung_nilai_penambahan==0 && $hitung_nilai_pengurangan==0){

            $total_biaya = ($query_plus->jml_biaya);
        }



        //$total_biaya = $total+$kalkulasi_detail_min;
        $ppn = (10/100)*$total_biaya;
        $biaya_keseluruhan = $total_biaya;
		

		//$total = $query_plus->jml_biaya - $query_minus->jml_biaya;

		$data_update = array(
			'nominal' => $biaya_keseluruhan,
			'terbilang' => terbilang($biaya_keseluruhan)
		);

		//print_r($data_update);
		//exit();
		$this->db->where('invoice_id', $id_invoice);
		$update_nominal_invoice = $this->db->update('invoice',$data_update);

	}


	public function simpan_invoice(){
		$pattern = "/[-\s]/";
		$date_inv = preg_split($pattern, $this->input->post('date_inv'));
		$date_tempo = preg_split($pattern, $this->input->post('date_tempo'));

		$rewrite_date_inv = $date_inv[2].'-'.$date_inv[1].'-'.$date_inv[0];
		$rewrite_date_tempo = $date_tempo[2].'-'.$date_tempo[1].'-'.$date_tempo[0];

		$pattern_nominal = "/[.\s]/";
		$nominal = preg_replace($pattern_nominal, '', $this->input->post('nominal_inv'));
		$no_invoice = $this->generate_no_inv(decrypt_url($this->input->post('project')));

		$user = $this->session->userdata('user');

		$data = array(
			'project_id' 	=> decrypt_url($this->input->post('project')),
			'no_surat' 		=> $this->input->post('no_sp').$this->generate_sp(),
			'no_invoice' 	=> $no_invoice,
			'no_pjk' 		=> $this->input->post('no_wo'),
			//'tgl_invoice' 	=> $rewrite_date_inv,
			//'tgl_jthtempo' 	=> $rewrite_date_tempo,
			//'nominal' => $nominal,
			'lama_hari' 	=> $this->input->post('lama_hari'),
			'kode_invoice' 	=> $no_invoice,
			'created_by' 	=> $user['m_karyawan_id'],
			'created_date' 	=> date('Y-m-d h:i:s')

		);



		if($this->Invoice_model->insert_record($data)){

            $last_id = $this->db->insert_id();

            $data_bank = array(
				'invoice_id' => $last_id,
				'bank_id' => decrypt_url($this->input->post('bank'))
			);
			$this->Bank_model->insert_record($data_bank);

            $encrypt_last_id = encrypt_url($last_id);

            $this->session->set_flashdata('message', 'Data Berhasil Di Tambahkan');
            $this->session->set_flashdata('status', 'success');

        	redirect('invoice/detail_invoice/'.$encrypt_last_id);
                
        }else{
            $this->session->set_flashdata('message', 'Data Gagal Di Tambahkan');
            $this->session->set_flashdata('status', 'danger');
        	redirect('invoice/create_invoice');
        }

	}
	

	public function generate_no_inv($id_project){
		//2001BJG79001

		$date = date('ym');
		$kode_bank = 'BJG79';
		//$getkode
			$this->db->select('kode_project');
			$this->db->where('project_id', $id_project);
			$get_kode_project = $this->db->get('project')->row();

			$kode_project = preg_replace('/\s+/', '', strtoupper($get_kode_project->kode_project));


			$this->db->select('RIGHT(invoice.no_invoice,3) as kode', FALSE);
			$this->db->order_by('invoice_id','DESC');    
			$this->db->limit(1);    
		  	$query = $this->db->get('invoice');       
		  	if($query->num_rows() <> 0){      
			   //jika kode ternyata sudah ada.      
			   $data = $query->row();      
			   $kode = intval($data->kode) + 1;    
			}else {      
				//jika kode belum ada      
				$kode = 1;    
		  	}

		  	//print(str_pad($kode, 3, "0", STR_PAD_LEFT));



		  $kodemax = str_pad($kode, 3, "0", STR_PAD_LEFT); 
		  $kodejadi = $date.''.$kode_project.''.$kodemax;    
		 // echo $kodejadi;
		  return $kodejadi;
	}


	public function ubah($id_invoice=null)
	{
		if($id_invoice==null || $id_invoice=="" ){
			redirect('invoice/lists');
		}
		

        $data = array();
        $data['title'] 			= 'Ubah Invoice';
		$data['action'] 		= 'edit';
		$data['active_menu'] 	= 'edit_invoice';
		$data['action_url'] 	= site_url('invoice/update_invoice/').$id_invoice;
		//$data['list_divisi'] 	= $this->Devisi_model->get_data_devisi();
		$data['css'] 			= array(
			'plugins/datepicker/datepicker3.css',
			'plugins/select2/select2.min.css');
		$data['js']				= array(	// js tambahan
			'js/jquery.validate.js',
			'plugins/datepicker/bootstrap-datepicker.js',
			'plugins/select2/select2.full.min.js',
			'plugins/terbilang/terbilang.js',
			'plugins/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js',
        );
        $get_data_wo = $this->Invoice_model->get_data_wo();
        $data['get_data_wo'] = $get_data_wo;

        $get_data_bank = $this->Bank_model->get_data_bank();
        $data['get_data_bank'] = $get_data_bank;

        $decrypt_id = decrypt_url($id_invoice);
        

       	$get_data_inv_by_id = $this->Invoice_model->get_data_by_id($decrypt_id);


       	$this->db->select('pjt.project_id, pjt.nama_project, pjt.no_wo, pjt.kode_project,  pjt.start_date, pjt.end_date, plg.pelanggan_id, plg.nama_pelanggan, plg.nama_alias, plg.divisi, plg.nama_pic, plg.alamat_surat, plg.alamat_invoice, plg.npwp');
        $this->db->from('project as pjt');
        $this->db->join('pelanggan as plg','plg.pelanggan_id = pjt.pelanggan_id','inner');
        $this->db->where('pjt.project_id',$get_data_inv_by_id->project_id);
        $get_data_wo_by_id = $this->db->get()->row();


       	$data['project'] = encrypt_url($get_data_wo_by_id->project_id);
		$data['nama_project'] =  $get_data_wo_by_id->nama_project;
		$data['no_wo'] =  $get_data_wo_by_id->no_wo;
		$data['kode_project'] =  $get_data_wo_by_id->kode_project;
		$data['start_date'] =  date('d-m-Y',strtotime($get_data_wo_by_id->start_date));
		$data['end_date'] =  date('d-m-Y',strtotime($get_data_wo_by_id->end_date));
			//'pelanggan_id'] =  $query->nama_project;
		$data['nama_pelanggan'] =  $get_data_wo_by_id->nama_pelanggan;
		$data['nama_alias'] =  $get_data_wo_by_id->nama_alias;
		$data['divisi'] =  $get_data_wo_by_id->divisi;
		$data['nama_pic'] =  $get_data_wo_by_id->nama_pic;
		$data['alamat_surat'] =  $get_data_wo_by_id->alamat_surat;
		$data['alamat_invoice'] =  $get_data_wo_by_id->alamat_invoice;
		$data['npwp'] =  $get_data_wo_by_id->npwp;
		$data['message'] =  'sukses load data';
		$data['rc'] =  '0000';

		$split_no_surat = explode("/", $get_data_inv_by_id->no_surat);
		$data['no_sp'] = $split_no_surat[0];
		$data['no_invoice'] = $get_data_inv_by_id->no_invoice;
		$data['tgl_invoice'] = $get_data_inv_by_id->tgl_invoice?date('Y-m-d',strtotime($get_data_inv_by_id->tgl_invoice)):'';
		$data['tgl_jthtempo'] = $get_data_inv_by_id->tgl_jthtempo?date('Y-m-d',strtotime($get_data_inv_by_id->tgl_jthtempo)):'';
		$data['nominal'] = "Rp. " . number_format($get_data_inv_by_id->nominal, 0, ",", ".");
		$data['terbilang'] = $get_data_inv_by_id->terbilang;
		$data['lama_hari'] = $get_data_inv_by_id->lama_hari;
		//$data['id_invoice'] = encrypt_url($get_data_inv_by_id->invoice_id);
		$data['bank'] = encrypt_url($get_data_inv_by_id->bank_id);
		$data['ib'] = encrypt_url($get_data_inv_by_id->ib_id);
		$data['invice_id'] = $id_invoice;
		$data['is_approved'] = $get_data_inv_by_id->is_approved;
		
		$this->template->load('template','invoice/ubah_invoice', $data);
	}


	public function update_invoice($id_invoice){
	
		if($id_invoice==null || $id_invoice=="" ){
			redirect('invoice/lists');
		}

		$decrypt_id = decrypt_url($id_invoice);

		$pattern = "/[-\s]/";
		$date_inv = preg_split($pattern, $this->input->post('date_inv'));
		$date_tempo = preg_split($pattern, $this->input->post('date_tempo'));

		$rewrite_date_inv = $date_inv[2].'-'.$date_inv[1].'-'.$date_inv[0];
		$rewrite_date_tempo = $date_tempo[2].'-'.$date_tempo[1].'-'.$date_tempo[0];

		$pattern_nominal = "/[.\s]/";
		$nominal = preg_replace($pattern_nominal, '', $this->input->post('nominal_inv'));
		

		
		$data = array(
			//'project_id' => decrypt_url($this->input->post('project')),
			//'no_surat' => "/D3.400/HK200/TSAT/",
			//'no_pjk' => $this->input->post('no_wo'),
			'lama_hari' 	=> $this->input->post('lama_hari'),
			'modified_date' => date('Y-m-d h:i:s'),
			//'no_invoice' => $this->generate_no_inv(decrypt_url($this->input->post('project'))),

		);

		if($this->input->post('tgl_inv')!="" && $this->input->post('tgl_jth_tempo')!=""){
			$data['tgl_invoice'] = $this->input->post('tgl_inv');
			$data['tgl_jthtempo'] = $this->input->post('tgl_jth_tempo');
		}

		$this->db->where('invoice_id',$decrypt_id);
		if($this->db->update('invoice',$data)){

			$decrypt_ib = decrypt_url($this->input->post('ib'));


			$data_bank = array(
				'bank_id' => decrypt_url($this->input->post('bank'))
			);
			$this->db->where('ib_id',$decrypt_ib);
			$this->db->update('invoice_bank', $data_bank);


			$this->session->set_flashdata('message', 'Data Berhasil Di Perbaharui');
            $this->session->set_flashdata('status', 'success');

                
        }else{
            $this->session->set_flashdata('message', 'Data Gagal Di Perbaharui');
            $this->session->set_flashdata('status', 'danger');
        }
        redirect('invoice/update_data/'.$id_invoice);

	}

	public function delete_invoice()
	{
		
		$decrypt_id = decrypt_url($this->input->post('data'));
		$decrypt_ib = decrypt_url($this->input->post('ib'));

		$this->db->trans_start();
			$this->Invoice_model->delete($decrypt_id);

			$condition = array(
				'ib_id'=> $decrypt_ib,
				'invoice_id' => $decrypt_id
			);	 
        	$this->db->where($condition);
        	$this->db->delete('invoice_bank');


        	$condition2 = array(
				'invoice_id' => $decrypt_id
			);	 
        	$this->db->where($condition2);
        	$this->db->delete('invoice_detail_plus');

        	$condition3 = array(
				'invoice_id' => $decrypt_id
			);	 
        	$this->db->where($condition2);
        	$this->db->delete('invoice_detail_minus');


		$this->db->trans_complete();
		if ($this->db->trans_status() === FALSE){
			$message="Delete invoice gagal!";
			$status=true;
			$rc="0005";
		}else{
			$message="Delete invoice Berhasil";
			$status=false;
			$rc="0000";
		}


		$this->session->set_flashdata('message', $message);
		$this->session->set_flashdata('status', $status);

		$data['rc'] = $rc;
		$data['message'] = $message;

		echo json_encode($data);

	}

	public function approve_invoice()
	{
		
		$invoice_id = $this->input->post('invoice_id');
		$inv = $this->Invoice_model->get_data_by_id($invoice_id);
		$date = date('Y-m-d');
		$tgl_jthTempo = date('Y-m-d', strtotime($date. ' + '.$inv->lama_hari.' days'));
		if($inv->is_approved=='0'){
			$is_approved = '1';
		}else{
			$is_approved = '0';
		}
		$data = array(
			'tgl_invoice'  => date('Y-m-d'),
			'tgl_jthtempo' => $tgl_jthTempo,
			'is_approved' => $is_approved,
		);
		$del = $this->Invoice_model->update($invoice_id,$data);
		if($del){
			$message="Approve invoice Berhasil";
			$status=200;
			$rc="0000";
        }else{
			$message="Approve invoice gagal!";
			$status=400;
			$rc="0005";
			
		}

		$this->session->set_flashdata('message', $message);
		$this->session->set_flashdata('status', $status);

		$data['rc'] = $rc;
		$data['message'] = $message;

		echo json_encode($data);

	}

	public function detail($inv_id){

		$invoice_id = decrypt_url($inv_id);
		$inv = $this->Invoice_model->get_data_by_id($invoice_id);
		
		$data = array();
		$data['active_menu'] 				= 'invoice';
		$data['uncheck_tagihan_otc']    	= $this->Invoice_model->getUncheckTagihan($inv->project_id,'otc');
		$data['uncheck_tagihan_mrc']    	= $this->Invoice_model->getUncheckTagihan($inv->project_id,'mrc');
		$data['uncheck_tagihan_cpe']    	= $this->Invoice_model->getUncheckTagihan($inv->project_id,'cpe');

		//print_r($data['uncheck_tagihan_otc']);die;
	/*	$data_otc = $this->Invoice_model->getListTagihan($invoice_id,'otc','array');

		$new_otc = array();
		foreach ($data_otc as $val) {
			$new_otc[$val['nama_produk']][] = $val;

		}

		$data['tagihan_otc'] = $new_otc;*/

		$data['tagihan_otc']    	= $this->Invoice_model->getListTagihan($invoice_id,'otc');
		$data['tagihan_mrc']    	= $this->Invoice_model->getListTagihan($invoice_id,'mrc');
		$data['tagihan_cpe']    	= $this->Invoice_model->getListTagihan($invoice_id,'cpe');
		$data['detail_minus']    	= $this->Invoice_model->get_data_all_detailminus($invoice_id);

		$data['action_url_plus'] 	= site_url('invoice/saves_detail_plus/'.$inv_id);
		$data['action_url_update_plus'] = site_url('invoice/saves_update_detail_plus');
		$data['action_url_minus'] = site_url('invoice/saves_detail_minus/'.$inv_id);
		$data['action_url_update_minus'] = site_url('invoice/saves_update_detail_minus');

		$data['action_url_tambah_otc'] = site_url('invoice/saves_otc');
		$data['action_url_tambah_cpe'] = site_url('invoice/saves_cpe');
		$data['action_url_tambah_mrc'] = site_url('invoice/saves_mrc');


		$data['css'] 			= array(
			'plugins/datepicker/datepicker3.css',
			'plugins/sweet-alert/sweetalert.css','plugins/select2/select2.min.css',
			
		);
		$data['js']				= array(	// js tambahan
			'js/jquery.validate.js',
			'plugins/datepicker/bootstrap-datepicker.js',
			'plugins/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js',
			'plugins/sweet-alert/sweetalert.min.js','plugins/select2/select2.full.min.js',
			'extension/bootstrap-filestyle-2.1.0/src/bootstrap-filestyle.min.js'
		); 
		$data['invoice_id'] = $inv_id;
		$data['inv_id'] = $invoice_id;
		$this->db->select('inv.no_invoice, inv.kode_invoice,inv.is_approved, pjt.kode_project');
        $this->db->from('invoice as inv');
        $this->db->join('project as pjt','pjt.project_id = inv.project_id','inner');
        $this->db->where('inv.invoice_id',$invoice_id);
		$get_invoice = $this->db->get()->row();

		$data['no_invoice'] = $get_invoice->no_invoice;
		$data['is_approved'] = $get_invoice->is_approved;
		$data['kode_invoice'] = $get_invoice->kode_invoice;
		$data['kode_project'] = $get_invoice->kode_project;

		
		$this->template->load('template','invoice/form_detail', $data);
	}

	public function simpan_invoice_tagihan(){
		$id_invoice = $this->input->post('invoice_id');
		$user = $this->session->userdata('user');
		$data = array(
			'invoice_id' => decrypt_url($this->input->post('invoice_id')),
			'tagihan_id' => $this->input->post('pilih_tagihan'),
			'created_date' => date('Y-m-d h:i:s'),
			'created_by' => $user['m_karyawan_id'],
		);


		if($this->Invoice_tagihan_model->save_invoice_tagihan($data)){
			$this->calculate_total_bayar(decrypt_url($id_invoice));
            $this->session->set_flashdata('message', 'Data Berhasil Di Tambahkan');
            $this->session->set_flashdata('status', 'success');

        }else{
            $this->session->set_flashdata('message', 'Data Gagal Di Tambahkan');
            $this->session->set_flashdata('status', 'danger');
            
        }
        redirect('invoice/detail_invoice/'.$id_invoice);
	}

	public function delete_invoice_tagihan(){
			

		$inv_tag_id = decrypt_url($this->input->post('inv_tag_id'));
		$id_invoice = decrypt_url($this->input->post('inv_id'));
		$del = $this->Invoice_tagihan_model->delete_invoice_tagihan($inv_tag_id);
		if($del){
			$this->calculate_total_bayar($id_invoice);
			$message="Delete Tagihan OTC Berhasil";
			$status =true;
			$rc="0000";
        }else{
			$message="Delete Tagihan OTC Gagal!";
			$status =false;
			$rc="0005";
			
		}


		$data['rc'] = $rc;
		$data['message'] = $message;

		echo json_encode($data);
       
	}

	public function add_detail_plus($id="")
	{
		//error_reporting(0);
		$data['icon'] = '<i class="fa fa-search"></i>';
		$data['title'] = '<b>Add Detail Invoice</b>';
		$data['css'] 			= array(
			'plugins/datepicker/datepicker3.css');
		$data['js']				= array(	// js tambahan
			'js/jquery.validate.js',
			'plugins/datepicker/bootstrap-datepicker.js',
			'plugins/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js',
        );
		$data['r'] = array();
		$data['action_url'] = site_url('invoice/save_detail_plus');
		$this->load->view('invoice/view_invoice', $data);
	}


	public function simpan_detail_plus($id_invoice){
		$pattern = "/[-\s]/";
		$tgl_awal = preg_split($pattern, $this->input->post('tgl_awal'));
		$date_akhir = preg_split($pattern, $this->input->post('tgl_akhir'));

		$rewrite_date_awal = $tgl_awal[2].'-'.$tgl_awal[1].'-'.$tgl_awal[0];
		$rewrite_date_akhir = $date_akhir[2].'-'.$date_akhir[1].'-'.$date_akhir[0];

		$pattern_nominal = "/[.\s]/";
		$qty = preg_replace($pattern_nominal, '', $this->input->post('qty'));
		$otc = preg_replace($pattern_nominal, '', $this->input->post('otc'));
		$mtc = preg_replace($pattern_nominal, '', $this->input->post('mtc'));
		$cpe = preg_replace($pattern_nominal, '', $this->input->post('cpe'));
	

		$jumlah_biaya =  (($qty * $otc) + ($qty * $mtc) + ($qty * $cpe));

		$data = array(
			'deskripsi' => $this->input->post('deskripsi'),
			'jumlah' => $qty,
			'speed' => $this->input->post('speed'),
			'tgl_awal' => $rewrite_date_awal,
			'tgl_akhir' => $rewrite_date_akhir,
			'otc' => $otc,
			'mtc' => $mtc,
			'cpe' => $cpe,
			'jml_biaya' => $jumlah_biaya,
			'invoice_id' => decrypt_url($id_invoice)

		);


		if($this->Detail_inv_model->insert_record_plus($data)){
			$this->calculate_total_bayar(decrypt_url($id_invoice));
            $this->session->set_flashdata('message', 'Data Berhasil Di Tambahkan');
            $this->session->set_flashdata('status', 'success');

        }else{
            $this->session->set_flashdata('message', 'Data Gagal Di Tambahkan');
            $this->session->set_flashdata('status', 'danger');
            
        }

        //exit();
        redirect('invoice/detail_invoice/'.$id_invoice);
	}

	public function simpan_detail_minus($id_invoice){
		$pattern = "/[-\s]/";
		$tgl_awal = preg_split($pattern, $this->input->post('tgl_awal'));
		$date_akhir = preg_split($pattern, $this->input->post('tgl_akhir'));

		$rewrite_date_awal = $tgl_awal[2].'-'.$tgl_awal[1].'-'.$tgl_awal[0];
		$rewrite_date_akhir = $date_akhir[2].'-'.$date_akhir[1].'-'.$date_akhir[0];

		$pattern_nominal = "/[.\s]/";
		$qty = preg_replace($pattern_nominal, '', $this->input->post('qty'));
		$biaya = preg_replace($pattern_nominal, '', $this->input->post('biaya'));
	

		//$jumlah_biaya =   $qty * ((int)$otc + (int)$mtc + (int)$cpe);
		$jumlah_biaya =   $qty * $biaya;

		/*print_r($jumlah_biaya);
		exit();*/


		$data = array(
			'deskripsi' => $this->input->post('deskripsi'),
			'jumlah' => $qty,
			'speed' => $this->input->post('speed'),
			'tgl_awal' => $rewrite_date_awal,
			'tgl_akhir' => $rewrite_date_akhir,
			'jml_biaya' => $jumlah_biaya,
			'invoice_id' => decrypt_url($id_invoice),
			'biaya' => $biaya,
			'type' => $this->input->post('type')

		);

		if($this->Detail_inv_model->insert_record_minus($data)){
			$this->calculate_total_bayar(decrypt_url($id_invoice));
            $this->session->set_flashdata('message', 'Data Berhasil Di Tambahkan');
            $this->session->set_flashdata('status', 'success');

        }else{
            $this->session->set_flashdata('message', 'Data Gagal Di Tambahkan');
            $this->session->set_flashdata('status', 'danger');
        }

        //exit();
        redirect('invoice/detail_invoice/'.$id_invoice);
	}
	

	public function delete_invoice_plus()
	{
		
		$decrypt_id_detail = decrypt_url($this->input->post('data-detail'));
		$decrypt_id_inv = decrypt_url($this->input->post('data-inv'));
		$del = $this->Detail_inv_model->delete_invoice_plus($decrypt_id_detail);
		if($del){
			$this->calculate_total_bayar($decrypt_id_inv);
			$message="Delete Detail Penambahan Berhasil";
			$status =true;
			$rc="0000";
        }else{
			$message="Delete Gagal!";
			$status =false;
			$rc="0005";
			
		}

		$this->session->set_flashdata('message', $message);
		$this->session->set_flashdata('status', $status);

		$data['rc'] = $rc;
		$data['message'] = $message;

		echo json_encode($data);

	}

	public function delete_invoice_minus()
	{
		
		$decrypt_id_detail = decrypt_url($this->input->post('data-detail'));
		$decrypt_id_inv = decrypt_url($this->input->post('data-inv'));

		$del = $this->Detail_inv_model->delete_invoice_minus($decrypt_id_detail);
		if($del){
			$this->calculate_total_bayar($decrypt_id_inv);
			$message="Delete Detail Pengurangan Berhasil";
			$status =true;
			$rc="0000";
        }else{
			$message="Delete Gagal!";
			$status =false;
			$rc="0005";
			
		}

		$this->session->set_flashdata('message', $message);
		$this->session->set_flashdata('status', $status);

		$data['rc'] = $rc;
		$data['message'] = $message;

		echo json_encode($data);

	}

	public function update_invoice_plus(){
		$decrypt_id = decrypt_url($this->input->post('value'));
		$id_inv = $this->input->post('value-invoice');

		$pattern = "/[-\s]/";
		$tgl_awal = preg_split($pattern, $this->input->post('tgl_awal'));
		$date_akhir = preg_split($pattern, $this->input->post('tgl_akhir'));

		$rewrite_date_awal = $tgl_awal[2].'-'.$tgl_awal[1].'-'.$tgl_awal[0];
		$rewrite_date_akhir = $date_akhir[2].'-'.$date_akhir[1].'-'.$date_akhir[0];

		$pattern_nominal = "/[.\s]/";
		$qty = preg_replace($pattern_nominal, '', $this->input->post('qty'));
		$otc = preg_replace($pattern_nominal, '', $this->input->post('otc'));
		$mtc = preg_replace($pattern_nominal, '', $this->input->post('mtc'));
		$cpe = preg_replace($pattern_nominal, '', $this->input->post('cpe'));
	

		//$jumlah_biaya =   $qty * ((int)$otc + (int)$mtc + (int)$cpe);
		$jumlah_biaya =  (($qty * $otc) + ($qty * $mtc) + ($qty * $cpe));

		$data = array(
			'deskripsi' => $this->input->post('deskripsi'),
			'jumlah' => $qty,
			'speed' => $this->input->post('speed'),
			'tgl_awal' => $rewrite_date_awal,
			'tgl_akhir' => $rewrite_date_akhir,
			'otc' => $otc,
			'mtc' => $mtc,
			'cpe' => $cpe,
			'jml_biaya' => $jumlah_biaya,

		);
		$this->db->where('detail_id',$decrypt_id);
		if($this->db->update('invoice_detail_plus',$data)){
			$this->calculate_total_bayar(decrypt_url($this->input->post('value-invoice')));

			$this->session->set_flashdata('message', 'Data Berhasil Di Perbaharui');
            $this->session->set_flashdata('status', 'success');

                
        }else{
            $this->session->set_flashdata('message', 'Data Gagal Di Perbaharui');
            $this->session->set_flashdata('status', 'danger');
        }
        redirect('invoice/detail_invoice/'.$id_inv);
	}

	public function update_invoice_minus(){
		/*echo decrypt_url($this->input->post('value-invoice'));
		exit();*/
		$decrypt_id = decrypt_url($this->input->post('value'));
		$id_inv = $this->input->post('value-invoice');

		$pattern = "/[-\s]/";
		$tgl_awal = preg_split($pattern, $this->input->post('tgl_awal'));
		$date_akhir = preg_split($pattern, $this->input->post('tgl_akhir'));

		$rewrite_date_awal = $tgl_awal[2].'-'.$tgl_awal[1].'-'.$tgl_awal[0];
		$rewrite_date_akhir = $date_akhir[2].'-'.$date_akhir[1].'-'.$date_akhir[0];

		$pattern_nominal = "/[.\s]/";
		$qty = preg_replace($pattern_nominal, '', $this->input->post('qty'));
		/*$otc = preg_replace($pattern_nominal, '', $this->input->post('otc'));
		$mtc = preg_replace($pattern_nominal, '', $this->input->post('mtc'));
		$cpe = preg_replace($pattern_nominal, '', $this->input->post('cpe'));*/
		$biaya = preg_replace($pattern_nominal, '', $this->input->post('biaya'));

		//$jumlah_biaya =   $qty * ((int)$otc + (int)$mtc + (int)$cpe);

		$jumlah_biaya =   $qty * $biaya;

		$data = array(
			'deskripsi' => $this->input->post('deskripsi'),
			'jumlah' => $qty,
			'speed' => $this->input->post('speed'),
			'tgl_awal' => $rewrite_date_awal,
			'tgl_akhir' => $rewrite_date_akhir,
			/*'otc' => $otc,
			'mtc' => $mtc,
			'cpe' => $cpe,*/
			'jml_biaya' => $jumlah_biaya,
			'biaya' => $biaya,
			'type' => $this->input->post('type')

		);
		$this->db->where('detail_id',$decrypt_id);
		if($this->db->update('invoice_detail_minus',$data)){
			$this->calculate_total_bayar(decrypt_url($this->input->post('value-invoice')));
			$this->session->set_flashdata('message', 'Data Berhasil Di Perbaharui');
            $this->session->set_flashdata('status', 'success');

                
        }else{
            $this->session->set_flashdata('message', 'Data Gagal Di Perbaharui');
            $this->session->set_flashdata('status', 'danger');
        }
        redirect('invoice/detail_invoice/'.$id_inv);
	}


	// fase 22
	//=============================================================================================

	public function generate_invoice(){
		$user = $this->session->userdata('user');
		$arr_tag = $this->input->post('arr_tagihan');
		$project_id = decrypt_url($this->input->post('project_id'));
		$tagihan_check = explode(',',$arr_tag);
		// $tagihan_check ini array id_tagihan yg telah d ceklis
		// lo tinggal buat invoice nya lama_hari default aja dulu isi 20 hari
		// no invoice sama kaya fase sama kode nya jadi lo tambahin inputan project_id ny ya
		$no_invoice = $this->custom_lib->new_generate_no_inv($project_id);
		$data_inv = array(
			'project_id'		=> $project_id,
			'no_invoice'		=> $no_invoice,
			'lama_hari'			=> 20,
			'created_by' 		=> $user['m_karyawan_id'],
			'created_date' 		=> date('Y-m-d H:i:s'),
		);

		$invoice_id = $this->Invoice_model->insert_invoice($data_inv);

		$data_bank = array(
			'invoice_id' 	=> $invoice_id,
			'bank_id'		=> 4
		);
		$this->Bank_model->insert_invoice_bank($data_bank);

		$data_tag = array();
		$total_nominal = 0;
		foreach ($tagihan_check as $key => $value) {
			$nominal_inv = $this->Tagihan_model->getJumlahBiaya($value)->jumlah_biaya;
			$total_nominal = $total_nominal+(int)$nominal_inv;
			$data_tag[] = array(
				'invoice_id' 	=> $invoice_id,
				'tagihan_id'	=> $value,
				'created_by' 	=> $user['m_karyawan_id'],
				'created_date' 	=> date('Y-m-d H:i:s'),	
			);
		}
		$query = $this->Invoice_model->insert_batch_invoice_tagihan($data_tag);
	
		if($query==true){
			$data_nominal = array(
				'nominal' => $total_nominal
			);
			$this->Invoice_model->update($invoice_id,$data_nominal);
			$json=array(
				'status' 		=> true,
				'message' 		=> "Sukses Generate Invoice",
				'rc' 			=> '0000',
				'invoice_id' 	=> $invoice_id	
			);
		}else{
			
			$json=array(
				'status' 		=> false,
				'message' 		=> "Gagal Generate Invoice",
				'rc' 			=> '0005'	,
				'invoice_id' 	=> $invoice_id
			);
		}
		
		echo json_encode($json);
		// echo"<pre>";
		// print_r($tagihan_check);
		// echo"</pre>";
	}

}
