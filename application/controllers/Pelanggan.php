<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('user')){
			redirect('auth/login');
		}
		//Do your magic here
		$this->load->model('Pelanggan_model');
	}

	public function index()
	{
		$data['pelanggan'] = $this->Pelanggan_model->get_pelanggan();
		$data['active_menu'] = 'pelanggan';
		$data['css'] 			= array(
			'plugins/sweet-alert/sweetalert.css',
		); // css tambahan
		$data['js']				= array(
			'plugins/sweet-alert/sweetalert.min.js',
		);

		$this->template->load('template','pelanggan/view_pelanggan',$data);
	}

	public function tambah()
	{

        $data = array();
        $data['title'] 			= 'Tambah Pelanggan';
		$data['action'] 		= 'add';
		$data['active_menu'] 	= 'add_pelanggan';
		$data['action_url'] 	= site_url('pelanggan/saves_insert');
		$data['css'] 			= array();
		$data['js']				= array(	// js tambahan
			'js/jquery.validate.js'
        );
		
		$this->template->load('template','pelanggan/tambah_pelanggan', $data);
	}

	public function simpan_pelanggan(){
		$data = array(
			'nama_pelanggan' => $this->input->post('nama_pelanggan'),
			'nama_alias' => $this->input->post('nama_alias'),
			'divisi' => $this->input->post('divisi'),
			'nama_pic' => $this->input->post('nama_pic'),
			'alamat_surat' => $this->input->post('alamat_surat'),
			'alamat_invoice' => $this->input->post('alamat_invoice'),
			'npwp' => $this->input->post('npwp'),
		);

		if($this->Pelanggan_model->insert_record_pelanggan($data)){

            $this->session->set_flashdata('message', 'Data Berhasil Di Tambahkan');
            $this->session->set_flashdata('status', 'success');
                
        }else{
            $this->session->set_flashdata('message', 'Data Gagal Di Tambahkan');
            $this->session->set_flashdata('status', 'danger');
        }
        redirect('pelanggan/data_pelanggan');
	}

	public function ubah($id_pelanggan=null)
	{
		if($id_pelanggan==null || $id_pelanggan=="" ){
			redirect('pelanggan/data_pelanggan');
		}
		

        $data = array();
        $data['title'] 			= 'Ubah Pelanggan';
		$data['action'] 		= 'edit';
		$data['active_menu'] 	= 'edit_pelanggan';
		$data['action_url'] 	= site_url('pelanggan/save_update/').$id_pelanggan;
		//$data['list_divisi'] 	= $this->Devisi_model->get_data_devisi();
		$data['css'] 			= array();
		$data['js']				= array(	// js tambahan
			'js/jquery.validate.js',
        );
       

		$decrypt_id = decrypt_url($id_pelanggan);

		$bank = $this->Pelanggan_model->get_data_pelanggan_byid($decrypt_id);

		$data['nama_pelanggan'] = $bank->nama_pelanggan;
		$data['nama_alias'] = $bank->nama_alias;
		$data['divisi'] = $bank->divisi;
		$data['nama_pic'] = $bank->nama_pic;
		$data['alamat_surat'] = $bank->alamat_surat;
		$data['alamat_invoice'] = $bank->alamat_invoice;
		$data['npwp'] = $bank->npwp;
		

		$this->template->load('template','pelanggan/ubah_pelanggan', $data);
	}

	public function update_pelanggan($id_pelanggan){

		if($id_pelanggan==null || $id_pelanggan=="" ){
			redirect('pelanggan/data_pelanggan');
		}

		$decrypt_id = decrypt_url($id_pelanggan);


		
		$data = array(
			'nama_pelanggan' => $this->input->post('nama_pelanggan'),
			'nama_alias' => $this->input->post('nama_alias'),
			'divisi' => $this->input->post('divisi'),
			'nama_pic' => $this->input->post('nama_pic'),
			'alamat_surat' => $this->input->post('alamat_surat'),
			'alamat_invoice' => $this->input->post('alamat_invoice'),
			'npwp' => $this->input->post('npwp'),
		);

		if($this->Pelanggan_model->update_data_pelanggan($data, $decrypt_id)){

			$this->session->set_flashdata('message', 'Data Berhasil Di Perbaharui');
            $this->session->set_flashdata('status', 'success');

                
        }else{
            $this->session->set_flashdata('message', 'Data Gagal Di Perbaharui');
            $this->session->set_flashdata('status', 'danger');
        }
        redirect('pelanggan/data_pelanggan');
	}

	public function get_pelanggan(){

		$draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
        $pelanggan = $this->Pelanggan_model->get_datatables();



        $data = array();
        $no=1;
        foreach($pelanggan as $in) {
        	$id_pelanggan = encrypt_url($in->pelanggan_id);
        
			$row = array();
			
			$permission = $this->session->userdata('permission');		
			
			$update = anchor(site_url('pelanggan/update_data/'.$id_pelanggan), '<button data-toggle="tooltip" data-placement="bottom" title="edit" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></button>', array('class' => ''))."&nbsp;";
	       	$hapus = '<button data-toggle="tooltip" data-placement="bottom" title="Hapus" class="btn btn-danger btn-xs hapus-pelanggan" value='.$id_pelanggan.'><i class="fa fa-trash"></i></button>&nbsp;';
			


			//'nama_alias','nama_pelanggan','divisi','nama_pic','alamat_surat','alamat_invoice','npwp'
			$row[] = $in->pelanggan_id;
	        $row[] = $in->nama_alias;
	        $row[] = $in->nama_pelanggan;
	        $row[] = $in->divisi;
	        $row[] = $in->nama_pic;
	        $row[] = $in->alamat_surat;
	        $row[] = $in->alamat_invoice;
	        $row[] = $in->npwp;
	        //$row[] = '';
           	$row[] =$update.$hapus;
                
            $data[] = $row;
            $no++;
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $this->Pelanggan_model->count_all(),
	        "recordsFiltered" => $this->Pelanggan_model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
        exit();
	}



	public function delete_pelanggan()
	{
		
		$decrypt_id = decrypt_url($this->input->post('data'));

		
		if ($this->Pelanggan_model->delete($decrypt_id)){
			
			$message="Delete data Berhasil";
			$status=true;
			$rc="0000";
		}else{
			$message="Delete data gagal!";
			$status=false;
			$rc="0005";
		}


		$this->session->set_flashdata('message', $message);
		$this->session->set_flashdata('status', $status);

		$data['rc'] = $rc;
		$data['message'] = $message;

		echo json_encode($data);

	}


}

/* End of file pelanggan.php */
/* Location: ./application/controllers/pelanggan.php */