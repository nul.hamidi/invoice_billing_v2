<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	function get_data_bank(){
		$query = $this->db->get('bank')->result();
		return $query;
	}

	function insert_record($data){
		return $this->db->insert('invoice_bank', $data);
	}

	function get_bank(){
		$data = $this->db->get('bank')->result();
		return $data;
	}

	function insert_record_bank($data){
		return $this->db->insert('bank', $data);
	}

	function get_data_bank_byid($id_bank){
		$this->db->where('bank_id', $id_bank);
		$data = $this->db->get('bank');

		return $data->row();
	}

	function update_data_bank($data, $id_bank){
		$this->db->where('bank_id',$id_bank);
		$update = $this->db->update('bank',$data);

		return $update;		

	}
	function delete_bank_byid($id_bank){
		$this->db->where('bank_id',$id_bank);
		$delete = $this->db->delete('bank');

		return $delete;		
	}

	// invoice bank
	function insert_invoice_bank($data){
		return $this->db->insert('invoice_bank', $data);
	}


}

/* End of file bank_model.php */
/* Location: ./application/models/bank_model.php */