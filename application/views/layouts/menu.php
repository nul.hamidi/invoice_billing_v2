<?php
$sess = $this->session->userdata();
?>
<section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <div class="profile sidebar-form" style="border-style: none">
        <ul>
            <li>
                 <?php
                 /*
                if (!empty($sess['karyawan'])) {
                    ?>
                    <img src="<?php echo !empty($sess['karyawan']['url']) ? base_url($sess['karyawan']['url']) : base_url('assets/img/user.jpg'); ?>" class="img-circle">
                <?php
                } else {
                    ?>
                    <img src="<?php echo base_url('assets/img/'); ?>user.jpg" class="img-circle" alt="User Image">
                <?php
                }*/
                ?> 
                <img src="<?php echo base_url('assets/img/'); ?>user.jpg" class="img-circle" alt="User Image">
            </li>
            <?php
            if (!empty($sess['karyawan'])) {
                echo '<li><b>'.$sess['karyawan']['nama'].'</b></li>';
                echo '<li>TELKOMSAT</li>';
            } else {
                echo '<li>Karyawan tidak diketahui</li>';
                echo "<li style='color:white'>TELKOMSAT</li>";
            }
            ?>
            <li style='color:white'><small>GROUP : <?php echo strtoupper($sess['subdit']['nama']); ?></small></li>
        </ul>
    </div>

    <ul class="sidebar-menu">
        <li class="<?php echo $active_menu=='dashboard'? 'active':'';?>">
            <a href="<?php echo site_url('home'); ?>">
                <i class="glyphicon glyphicon-home"></i> <span>Beranda</span>
            </a>
        </li>
        <li class="<?php echo $active_menu=='pelanggan'? 'active':'';?> ">
            <a href="<?php echo site_url('pelanggan/data_pelanggan'); ?>">
                <i class="fa fa-user"></i> <span>Pelanggan</span>
            </a>
        </li>
        <li class="<?php echo $active_menu=='bank'? 'active':'';?> ">
            <a href="<?php echo site_url('bank/data_bank'); ?>">
                <i class="fa fa-bank"></i> <span>Bank</span>
            </a>
        </li>
        <li class="<?php echo $active_menu=='project'? 'active':'';?> ">
            <a href="<?php echo site_url('project'); ?>">
                <i class="fa fa-cubes"></i> <span>Project</span>
            </a>
        </li>
        <li class="<?php echo $active_menu=='list_invoice'? 'active':'';?> ">
            <a href="<?php echo site_url('invoice/lists'); ?>">
                <i class="fa fa-file"></i> <span>Invoice</span>
            </a>
        </li>
       
       
      
    </ul>
</section>