<style type="text/css">
    .error{
        color: #a94442;
    }
</style>
<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('layouts/alert'); ?>
            <?php // echo '<pre>'; print_r($this->session->userdata()); ?>
            <div class="nav-tabs-custom-aqua">
                
               
                    
                    <!-- Horizontal Form -->
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?php echo $title;?></h3>
                            </div>
                        <!-- /.box-header -->
                            <!-- form start -->
                            <form method="post" action="<?php echo $action_url;?>" class="form-horizontal" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                       
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Nama Pelanggan</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="nama_pelanggan" id="nama_pelanggan"  class="form-control input-sm" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Nama Alias</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="nama_alias" id="nama_alias"  class="form-control input-sm" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            
                                            <label class="col-sm-2 control-label">Divisi</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="divisi" id="divisi"  class="form-control input-sm" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            
                                            <label class="col-sm-2 control-label">Nama PIC</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="nama_pic" id="nama_pic"  class="form-control input-sm" >
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Alamat Surat</label>
                                           	<div class="col-sm-4">
                                                    <textarea class="form-control" name="alamat_surat" id="alamat_surat" ></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Alamat Invoice</label>
                                           	<div class="col-sm-4">
                                                    <textarea class="form-control" name="alamat_invoice" id="alamat_invoice" ></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            
                                            <label class="col-sm-2 control-label">NPWP</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="npwp" id="npwp"  class="form-control input-sm" >
                                            </div>
                                        </div>
                                        

                                        

                                    </div> <!-- end col-12 -->
                                   
                                </div><!-- end row -->
                              
                               
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                
                                <div class="col-sm-6">
                                    <div class="pull-right"> 
                                        <a href="<?php echo base_url('pelanggan/data_pelanggan');?>" class="btn btn-default">Kembali</a>
                                        <button type="submit" class="btn btn-info ">Simpan</button>
                                        
                                    </div>
                                </div>
                                <div class="col-sm-6"> 
                                </div>
                            </div>
                            <!-- /.box-footer -->
                            </form>
                        </div>
          <!-- /.box -->
                    
               
            </div>
        </div>
    </div>
</section>
<script>
<?php //echo $this->jquery_validation->run('.form-horizontal');?>
const base_url = '<?php echo site_url(); ?>'



$(document).ready(function () {




    $(".form-horizontal").validate({
            rules: {
                nama_pelanggan: "required",
                nama_alias: "required",
                nama_pic: "required",
                divisi: "required",
                alamat_surat: "required",
                alamat_invoice: "required",
                npwp: "required",

            },
            messages: {
                nama_pelanggan:{
                    required:"<i class='fa fa-times'></i> Nama pelanggan harus diisi"
                },
                nama_alias:{
                    required:"<i class='fa fa-times'></i> Nama Alias harus diisi"
                }, 
                nama_pic:{
                    required:"<i class='fa fa-times'></i> Nama pic harus diisi"
                },
                alamat_surat:{
                    required:"<i class='fa fa-times'></i> Alamat surat harus diisi"
                }, 
                alamat_invoice:{
                    required:"<i class='fa fa-times'></i> Alamat invoice harus diisi"
                },
                npwp:{
                    required:"<i class='fa fa-times'></i> Npwp harus diisi"
                },
                
            },
            highlight: function (element) {
                $(element).parent().parent().addClass("has-error")
                $(element).parent().addClass("has-error")
            },
            unhighlight: function (element) {
                $(element).parent().removeClass("has-error")
                $(element).parent().parent().removeClass("has-error")
            }
    });

    

});
</script>
