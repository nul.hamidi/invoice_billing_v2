<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('layouts/alert'); ?>
            <?php // echo '<pre>'; print_r($this->session->userdata()); ?>

            <div class="nav-tabs-custom-aqua">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <div class="col-lg-6">
                            <h3 class="box-title" style="padding-top:5px">Daftar Pelanggan</h3>
                        </div>
                        <div class="col-lg-6 ">
                            <div class="pull-right">
                                <a href="<?php echo site_url('pelanggan/create_pelanggan'); ?>"> 
                                    <button class="btn btn-info btn-sm">
                                        <i class="glyphicon glyphicon-plus"></i> Tambah Data
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                   
                
                <div class="tab-content"  >
                     
                    <div class="tab-pane active">

                       
                        <div class="row">
                            <div class="col-md-12">
                                <table  class="table table-striped table-bordered table-hover" id="table-pelanggan" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="3%" style="text-align: center;">Pelanggan ID</th>
                                            <th width="10%">Nama Alias</th>
                                            <th width="15%">Nama Pelanggan</th>
                                            <th>Divisi</th>
                                            <th>PIC</th>
                                            <th>Alamat Surat</th>
                                            <th>Alamat Invoice</th>
                                            <th>NPWP</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    
                                    
                                   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
        oTable = $('#table-pelanggan').DataTable({
            processing: true, 
            serverSide: true, 
            order: [], 
            ajax: {
                url : "<?php echo site_url("pelanggan/get-data-pelanggan") ?>",
                type : 'get',
                          
            },
            columnDefs: [
                {
                    targets: [0,8],
                    orderable: false,
                    className: 'text-center',

                }

            ],
        });
        $(document).on("click",".hapus-pelanggan",function(){
            var encrypt = this.value;
            
            swal({
                title: "Yakin Hapus Data ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Hapus!",
                closeOnConfirm: false
            }, function () {

                $.ajax({
                    type : "POST",
                    url  : "<?php echo base_url();?>pelanggan/delete/",
                    dataType: "JSON",
                    data : "data="+encrypt,
                    success:function(data){
                        
                        if(data.rc=='0000'){
                            setTimeout(function() {
                                swal({
                                    title: "Notification!",
                                    text: "Success Delete Data",
                                    imageUrl: '<?= base_url("assets/img/success.png");?>'
                                }, function() {
                                  oTable.ajax.reload();
                                });
                            }, 1000);
                        }else{
                            setTimeout(function() {
                                swal({
                                    title: "Notification!",
                                    text: "Delete Failed",
                                    imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                                }, function() {
                                    oTable.ajax.reload();
                                });
                            }, 1000);
                        }
                        
                    }

                });
               
            });
                
        });
    </script>
