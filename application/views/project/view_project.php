<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('layouts/alert'); ?>
            <?php // echo '<pre>'; print_r($this->session->userdata()); ?>

            <div class="nav-tabs-custom-aqua">
                <div class="box box-info">
                    
                    <div class="box-header with-border">
                        <div class="col-lg-3">
                            <h3 class="box-title" style="padding-top:5px">Daftar Project</h3>
                        </div>
                        <div class="col-lg-9 ">
                            <div class="pull-right">
                                <a data-toggle='modal' data-target='#upload_excel_project'> 
                                    <button class="btn btn-info btn-sm">
                                        <i class="fa fa-upload"></i> Upload Excel Project
                                    </button>
                                </a>
                                <a href="<?php echo base_url('Export/export_project');?>"> 
                                    <button class="btn btn-success btn-sm">
                                        <i class="fa fa-download"></i> Export Excel
                                    </button>
                                </a>
                                <a href="<?php echo site_url('project/create_project'); ?>"> 
                                    <button class="btn btn-info btn-sm">
                                        <i class="glyphicon glyphicon-plus"></i> Tambah Data
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                   

                </div>

                <div class="tab-content"  >
                     
                    <div class="tab-pane active">

                       
                        <div class="row">
                            <div class="col-md-12">
                                <table  class="table table-striped table-bordered table-hover" id="table-project" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <!-- <th width="7%">No</th> -->
                                            <th width="15%">Pelanggan</th>
                                            <th width="22%">No WO</th>
                                            <th width="35%">Nama Project</th>
                                            <th>Periode</th>
                                            <th>Nilai</th>
                                            <th>Node</th>
                                            <th>Tagihan</th>
                                            <th>Nominal</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>                                    
                                   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('project/import_excel_project');?>

<script type="text/javascript">
      oTable = $('#table-project').DataTable({
            processing: true, 
            serverSide: true, 
            order: [], 
            ajax: {
                url : "<?php echo site_url("project/get-data-project") ?>",
                type : 'get',
                          
            },
            columnDefs: [
                {
                    targets: [5],
                    orderable: false,
                    className: 'text-center',

                }

            ],
        });
    $(document).on("click",".hapus-project",function(){
        var encrypt = this.value;
        
        swal({
            title: "Yakin Hapus Data ini ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, Hapus!",
            closeOnConfirm: false
        }, function () {

            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>project/delete/",
                dataType: "JSON",
                data : "data="+encrypt,
                success:function(data){
                    
                    if(data.rc=='0000'){
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Success Delete Data",
                                imageUrl: '<?= base_url("assets/img/success.png");?>'
                            }, function() {
                               oTable.ajax.reload();
                            });
                        }, 1000);
                    }else{
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Delete Failed",
                                imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                            }, function() {
                                 oTable.ajax.reload();
                            });
                        }, 1000);
                    }
                    
                }

            });
           
        });
            
    });
    
</script>