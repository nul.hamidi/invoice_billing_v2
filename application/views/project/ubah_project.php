<style type="text/css">
    .error{
        color: #a94442;
    }
</style>
<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('layouts/alert'); ?>
            <?php // echo '<pre>'; print_r($this->session->userdata()); ?>
            <div class="nav-tabs-custom-aqua">
                
               
                    
                    <!-- Horizontal Form -->
                        <div class="box box-info">
                            <div class="box-header with-border">
                    
                                <div class="col-lg-12">
                                <ul class="nav nav-tabs">
                                    <?php $this->load->view('layouts/header_menu');?>
                                    
                                </ul>
                                    
                                </div>
                            </div>
                        <!-- /.box-header -->
                            <!-- form start -->
                            <form method="post" action="<?php echo $action_url;?>" class="form-horizontal" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                       
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Pelanggan</label>
                                            <div class="col-sm-4">
                                                <select class="select2 form-control" name="pilih_pelanggan">
                                                    <option value=""></option>
                                                   <?php foreach ($pelanggan as $value) { 
                                                        if($pelanggan_id==encrypt_url($value->pelanggan_id)){
                                                    ?>
                                                        
                                                        <option value='<?= encrypt_url($value->pelanggan_id); ?>' selected="selected"><?= $value->nama_alias;?></option>
                                                    <?php
                                                            }else{
                                                    ?>
                                                        <option value='<?= encrypt_url($value->pelanggan_id); ?>'><?= $value->nama_alias;?></option>

                                                    <?php
                                                            }
                                                        }
                                                    ?>  
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">No WO</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="no_wo" id="no_wo"  class="form-control input-sm" value="<?=$no_wo?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Nama Project</label>
                                            <div class="col-sm-10">
                                                <input type="text"  name="nama_project" id="nama_project"  class="form-control input-sm" value="<?=$nama_project?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Kode Project</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="kode_project" id="kode_project"  class="form-control input-sm" value="<?=$kode_project?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Start kontrak</label>
                                            <div class="col-sm-4" id="tanggal">
                                               <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input autocomplete="off" type="text" class="form-control input-sm" name="start_kontrak" id="start_kontrak" value="<?=$start_kontrak?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">End Kontrak</label>
                                            <div class="col-sm-4" id="tanggal">
                                               <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input autocomplete="off" type="text" class="form-control input-sm" name="end_kontrak" id="end_kontrak" value="<?=$end_kontrak?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Nilai Kontrak</label>
                                            <div class="col-sm-4">
                                                <input type="text" name="nilai_kontrak" id="nilai_kontrak" placeholder="" class="form-control input-sm input_mask" value="<?= $nilai_kontrak; ?>">
                                            </div>
                                        </div>
                                        

                                        

                                    </div> <!-- end col-12 -->
                                   
                                </div><!-- end row -->
                              
                               
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                
                                <div class="col-sm-6">
                                    <div class="pull-right"> 
                                        <a href="<?php echo base_url('project/data_project');?>" class="btn btn-default">Kembali</a>
                                        <button type="submit" class="btn btn-info ">Update</button>
                                        
                                    </div>
                                </div>
                                <div class="col-sm-6"> 
                                </div>
                            </div>
                            <!-- /.box-footer -->
                            </form>
                        </div>
          <!-- /.box -->
                    
               
            </div>
        </div>
    </div>
</section>
<script>
<?php //echo $this->jquery_validation->run('.form-horizontal');?>
const base_url = '<?php echo site_url(); ?>'



$(document).ready(function () {
    $('.input_mask').mask('000.000.000.000', {reverse: true});

    $('.select2').select2({
        placeholder: "Please Select"
    });

    $('#tanggal .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });

    $(".form-horizontal").validate({
            rules: {
                name_bank: "required",
                cabang_bank: "required",
                address_bank: "required",
                jenis_rek: "required",
                norek: "required",

            },
            messages: {
                name_bank:{
                    required:"<i class='fa fa-times'></i> Nama bank harus diisi"
                },
                cabang_bank:{
                    required:"<i class='fa fa-times'></i> Cabang bank harus diisi"
                }, 
                address_bank:{
                    required:"<i class='fa fa-times'></i> Alamat bank harus diisi"
                },
                jenis_rek:{
                    required:"<i class='fa fa-times'></i> Jenis rekening harus diisi"
                }, 
                norek:{
                    required:"<i class='fa fa-times'></i> Nomor rekening harus diisi"
                },
                
            },
            highlight: function (element) {
                $(element).parent().parent().addClass("has-error")
                $(element).parent().addClass("has-error")
            },
            unhighlight: function (element) {
                $(element).parent().removeClass("has-error")
                $(element).parent().parent().removeClass("has-error")
            }
    });

    

});
</script>
