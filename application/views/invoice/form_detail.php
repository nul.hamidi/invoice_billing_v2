<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('layouts/alert'); ?>
            <?php // echo '<pre>'; print_r($this->session->userdata()); ?>

            <div class="nav-tabs-custom-aqua">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Detail Invoice</h3>
                        <div class="pull-right">
                            <a target ="blank" href="<?= base_url('report/preview/'.$this->uri->segment(3));?>"><button class='btn btn-warning btn-sm'><span class='glyphicon glyphicon-print'></span> Print Dokumen</button></a>
                        </div>
                    </div>
                </div>
                <div class="tab-content"  >

                    <!-- /.tab-pane -->
                        <!-- <label>Detail Invoice</label>
                        <div class="pull-right">
                            <a target ="blank" href="<?= base_url('report/preview/'.$this->uri->segment(3));?>"><button class='btn btn-warning btn-sm'><span class='glyphicon glyphicon-print'></span> Print Dokumen</button></a>
                        </div><hr> -->
                        <!-- <div class="pull-right">
                            <a data-toggle='modal' data-target='#upload_dok' class="btn btn-default btn-sm btn-flat">
                                <i class="fa fa-upload"></i> Import Excel
                            </a>
                        </div> -->
                        
                    

                        <div class="row">
                            <div class="col-lg-12">
                                <table width="50%">
                                    <tr>
                                        <td width="100px">No Invoice</td>
                                        <td>:</td>
                                        <td>&nbsp;<?= $no_invoice;?></td>
                                    </tr>
                                   
                                    <tr>
                                        <td>Kode Project</td>
                                        <td>:</td>
                                        <td>&nbsp;<?= $kode_project;?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <br><br>
                    <div class="tab-pane active">

                        <div class="row">
                            <div class="col-md-12">
                                <!-- <caption><b>ONE TIME CHARGE</b></caption> -->
                                <caption><b>ONE TIME CHARGE</b></caption>
                                <div class="pull-right">
                                <?php if($is_approved!='1'){?>
                                    <a data-toggle='modal' data-target='#ModalTagihan' data-type="otc"><button class='btn btn-info btn-sm'><span class='glyphicon glyphicon-plus'></span> Tambah</button></a>
                                <?php
                                }
                                ?>
                                </div><br><br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table  class="table table-striped table-bordered table-hover" id="invoice-plus" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="10">No</th>
                                            <th width="20%">Deskripsi</th>
                                            <!-- <th >Qty</th> -->
                                            <th >Speed</th>
                                            <th >Tgl Awal</th>
                                            <th >Tgl Akhir</th>
                                            <th >Biaya</th>
                                            <th width="70">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        $no=0;
                                        $total_otc=0;
                                        $jml_biaya=0;
                                        foreach ($tagihan_otc as $key) {
                                        $no++;
                                        $jml_biaya = ($key->jumlah*(int)$key->jumlah_biaya); 
                                        $total_otc = $total_otc+$jml_biaya;
                                    ?>
                                        <tr>
                                            <td><?php echo$no;?></td>
                                            <td><?php echo$key->node_deskripsi;?></td>
                                            <!-- <td><?php echo number_format($key->jumlah,'0','.','.');?></td> -->
                                            <td><?php echo $key->speed;?></td>
                                           <!--  <td><?php echo date('d/m/Y',strtotime($key->start_date));?></td>
                                            <td><?php echo date('d/m/Y',strtotime($key->end_date));?></td> -->
                                            <td>-</td>
                                            <td>-</td>
                                            <td>Rp. <?php echo number_format($key->jumlah_biaya,'0','.','.');?></td>
                                            <td><?php if($key->is_approved!=1){?>
                                                <button class="btn btn-danger btn-xs hapus-otc" data-toggle="tooltip" title="hapus-otc" value="<?= encrypt_url($key->invoice_tag_id); ?>"  data-invoice='<?= encrypt_url($key->invoice_id); ?>'><i class="fa fa-trash"></i>

                                                </button>
                                            <?php } ?>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                    <tr>
                                        <td colspan='5' align="right"><b>Total</b></td>
                                        <td colspan="2">Rp. <?php echo number_format($total_otc,0,',','.');?></td>

                                    </tr>
                                   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-md-12" >
                                <caption ><b>MONTHLY RECURRING</b></caption>
                                <div class="pull-right">
                                <?php if($is_approved!='1'){?>
                                    <a data-toggle='modal' data-target='#ModalTagihanMRC'><button class='btn btn-info btn-sm'><span class='glyphicon glyphicon-plus'></span> Tambah</button></a>
                                <?php
                                }
                                ?>
                                </div><br><br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table  class="table table-striped table-bordered table-hover" id="invoice-plus" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="10">No</th>
                                            <th width="20%">Deskripsi</th>
                                          <!--   <th >Qty</th> -->
                                            <th >Speed</th>
                                            <th >Tgl Awal</th>
                                            <th >Tgl Akhir</th>
                                            <th >Biaya</th>
                                            <th width="70">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        $no=0;
                                        $total_mrc=0;
                                        $jml_biaya=0;
                                        foreach ($tagihan_mrc as $key) {
                                        $no++;
                                        $jml_biaya = ($key->jumlah*(int)$key->jumlah_biaya); 
                                        $total_mrc = $total_mrc+$jml_biaya;
                                    ?>
                                        <tr>
                                            <td><?php echo$no;?></td>
                                            <td><?php echo$key->node_deskripsi;?></td>
                                           <!--  <td><?php echo number_format($key->jumlah,'0','.','.');?></td> -->
                                            <td><?php echo $key->speed;?></td>
                                            <td><?php echo date('d/m/Y',strtotime($key->start_date));?></td>
                                            <td><?php echo date('d/m/Y',strtotime($key->end_date));?></td>
                                            <td>Rp. <?php echo number_format($key->jumlah_biaya,'0','.','.');?></td>
                                            <td><?php if($key->is_approved!=1){?>
                                                <button class="btn btn-danger btn-xs hapus-mrc" data-toggle="tooltip" title="hapus-mrc" value="<?= encrypt_url($key->invoice_tag_id); ?>"  data-invoice='<?= encrypt_url($key->invoice_id); ?>'><i class="fa fa-trash"></i></button>
                                            <?php } ?>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                    <tr>
                                        <td colspan='5' align="right"><b>Total</b></td>
                                        <td colspan="2">Rp. <?php echo number_format($total_mrc,0,',','.');?></td>

                                    </tr>
                                   
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <caption><b>CPE</b></caption>
                                <div class="pull-right">
                                <?php if($is_approved!='1'){?>
                                    <a data-toggle='modal' data-target='#ModalTagihanCPE'><button class='btn btn-info btn-sm'><span class='glyphicon glyphicon-plus'></span> Tambah</button></a>
                                <?php
                                }
                                ?>
                                </div><br><br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table  class="table table-striped table-bordered table-hover" id="invoice-plus" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="10">No</th>
                                            <th width="20%">Deskripsi</th>
<!--                                             <th >Qty</th> -->
                                            <th >Speed</th>
                                            <th >Tgl Awal</th>
                                            <th >Tgl Akhir</th>
                                            <th >Biaya</th>
                                            <th width="70">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        $no=0;
                                        $total_cpe=0;
                                        $jml_biaya=0;
                                        $total = 0;
                                        foreach ($tagihan_cpe as $key) {
                                        $no++;
                                        $jml_biaya = ($key->jumlah*(int)$key->jumlah_biaya); 
                                        $total_cpe = $total_cpe+$jml_biaya;
                                    ?>
                                        <tr>
                                            <td><?php echo$no;?></td>
                                            <td><?php echo$key->node_deskripsi;?></td>
                                            <!-- <td><?php echo number_format($key->jumlah,'0','.','.');?></td> -->
                                            <td><?php echo $key->speed;?></td>
                                            <td><?php echo date('d/m/Y',strtotime($key->start_date));?></td>
                                            <td><?php echo date('d/m/Y',strtotime($key->end_date));?></td>
                                            <td>Rp. <?php echo number_format($key->jumlah_biaya,'0','.','.');?></td>
                                            <td><?php if($key->is_approved!=1){?>
                                                <button class="btn btn-danger btn-xs hapus-cpe" data-toggle="tooltip" title="hapus-cpe" value="<?= encrypt_url($key->invoice_tag_id); ?>"  data-invoice='<?= encrypt_url($key->invoice_id); ?>'><i class="fa fa-trash"></i></button>
                                            <?php } ?>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                    <tr>
                                        <td colspan='5' align="right"><b>Total</b></td>
                                        <td colspan="2">Rp. <?php echo number_format($total_cpe,0,',','.');?></td>

                                    </tr>
                                   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <caption><b>Adjusment</b></caption>
                                <div class="pull-right">
                                <?php if($is_approved!='1'){?>
                                    <a data-toggle='modal' data-target='#AddModalMinus'><button class='btn btn-info btn-sm'><span class='glyphicon glyphicon-plus'></span> Tambah</button></a>
                                <?php
                                }
                                ?>
                                    
                                </div><br><br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table  class="table table-striped table-hover table-bordered" id="invoice-minus" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="10">No</th>
                                            <th width="20%">Deskripsi</th>
                                           <!--  <th >Qty</th> -->
                                            <th >Speed</th>
                                            <th >Tgl Awal</th>
                                            <th >Tgl Akhir</th>
                                            <th>Type</th>
                                            <th >Biaya</th>
                                            <!-- <th >MTC/Biaya</th>
                                            <th >CPE/Prorate</th> -->
                                            <th >Jml Biaya</th>
                                            <th width="70">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        $no=0;
                                        $total_min =0;
                                        $jml_biaya=0;
                                        $hitung_nilai_penambahan =0;
                                        $hitung_nilai_pengurangan =0;
                                        $total_biaya = 0;
                                        $total = $total_otc+$total_mrc+$total_cpe;
                                        foreach ($detail_minus as $key) {

                                            if($key->type=="penambahan"){
                                                $jml_biaya = $key->jumlah*$key->biaya;
                                                $hitung_nilai_penambahan+=$jml_biaya;
                                            }else{
                                                $jml_biaya = $key->jumlah*$key->biaya;
                                                $hitung_nilai_pengurangan+=$jml_biaya;
                                            }
                                        $no++;
                                        //$jml_biaya = $key->jumlah* ( (int)$key->otc + (int)$key->mtc + (int) $key->cpe );
                                        
                                        //$total_min=$total_min+$jml_biaya;
                                    ?>
                                        <tr>
                                            <td><?php echo$no;?></td>
                                            <td><?php echo$key->deskripsi;?></td>
                                            <!-- <td><?php echo number_format($key->jumlah,'0','.','.');?></td> -->
                                            <td><?php echo $key->speed;?></td>
                                            <td><?php echo date('d/m/Y',strtotime($key->tgl_awal));?></td>
                                            <td><?php echo date('d/m/Y',strtotime($key->tgl_akhir));?></td>
                                            <td><?php echo ucfirst($key->type);?></td>
                                            <td><?php echo number_format($key->biaya,'0','.','.');?></td>
                                            <!-- <td><?php echo number_format($key->mtc,'0','.','.');?></td>
                                            <td><?php echo number_format($key->cpe,'0','.','.');?></td> -->
                                            <td><?php echo number_format($jml_biaya,'0','.','.');?></td>
                                            <td>
                                            <?php if($is_approved!='1'){?>
                                                <button class="btn btn-info btn-xs edit-pengurangan" data-toggle="modal" 
                                                    data-deskripi='<?= $key->deskripsi;?>' 
                                                    data-qty='<?= number_format($key->jumlah,'0','.','.');?>' 
                                                    data-biaya='<?= number_format($key->biaya,'0','.','.');?>' 
                                                    data-speed='<?= $key->speed;?>'
                                                    data-type='<?= $key->type;?>'
                                                    data-tgl_awal='<?= date('d-m-Y',strtotime($key->tgl_awal));?>'
                                                    data-tgl_akhir='<?= date('d-m-Y',strtotime($key->tgl_akhir));?>'
                                                    data-value='<?= encrypt_url($key->detail_id); ?>'
                                                    data-invoice='<?= encrypt_url($key->invoice_id); ?>'
                                                    data-target='#EditModalPengurangan'
                                                    title="edit"><i class="fa fa-edit"></i></button>
                                                
                                                <button class="btn btn-danger btn-xs hapus-pengurangan" data-toggle="tooltip" title="hapus-pengurangan" value="<?= encrypt_url($key->detail_id); ?>"  data-invoice='<?= encrypt_url($key->invoice_id); ?>'><i class="fa fa-trash"></i></button>
                                            <?php } ?>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    ?>

                                    <?php
                                        
                                        $kalkulasi_detail_min = $hitung_nilai_penambahan - $hitung_nilai_pengurangan;

                                        if($hitung_nilai_penambahan>0 && $hitung_nilai_pengurangan>0){
                                            
                                            $total_biaya = ($total + $hitung_nilai_penambahan) - $hitung_nilai_pengurangan;
                                        
                                        }else if($hitung_nilai_penambahan>0 && $hitung_nilai_pengurangan<=0){

                                            $total_biaya = ($total + $hitung_nilai_penambahan);

                                        }else if($hitung_nilai_penambahan<=0 && $hitung_nilai_pengurangan>0){

                                            $total_biaya = ($total - $hitung_nilai_pengurangan);
                                        
                                        }else if($hitung_nilai_penambahan==0 && $hitung_nilai_pengurangan==0){

                                            $total_biaya = ($total);
                                        }



                                        //$total_biaya = $total+$kalkulasi_detail_min;
                                        $ppn = ceil((10/100)*$total_biaya);
                                        $biaya_keseluruhan = $total_biaya+$ppn;

                                    ?>
                                    <tr>
                                        <td colspan='7' align="right"><b>Total Adjusment</b></td>
                                        <td colspan="2">Rp. <?php echo number_format($kalkulasi_detail_min,0,',','.');?></td>
                                    </tr>
                                    <tr>
                                        <td colspan='7' align="right"><b>Total Biaya (Tagihan & Adjusment)</b></td>
                                        <td colspan="2">Rp. <?php echo number_format($total_biaya,0,',','.');?></td>
                                    </tr>
                                    <tr>
                                        <td colspan='7' align="right"><b>PPN 10%</b></td>
                                        <td colspan="2">Rp. <?php echo number_format($ppn,0,',','.');?></td>
                                    </tr>
                                    <tr>
                                        <td colspan='7' align="right"><b>Total Biaya Keseluruhan</b></td>
                                        <td colspan="2">Rp. <?php echo number_format($biaya_keseluruhan,0,',','.');?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>
<?php
//print_r($uncheck_tagihan_otc);
?>


<style type="text/css">
    .select2 {
width:100%!important;
}
</style>

<?php $this->load->view('invoice/modal_tagihan');?>
<?php $this->load->view('invoice/modal_tagihan_mrc');?>
<?php $this->load->view('invoice/modal_tagihan_cpe');?>
<?php $this->load->view('invoice/modal_pengurangan');?>

<script type="text/javascript">
$(document).ready(function () {
    $('body').on('hidden.bs.modal', '.modal', function () {
      $(this).removeData('bs.modal');
    });

    $('.select2').select2({
        placeholder: "Silahkan pilih tagihan"
    });

   /* $('#invoice-plus').DataTable({});
    $('#invoice-minus').DataTable({});*/

    $('.input_mask').mask('000.000.000.000', {reverse: true});

    $('#tanggal-add-tgl-awal .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });
    $('#tanggal-add-tgl-akhir .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });
    $('#tanggal-edit-tgl-awal .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    }); 
    $('#tanggal-edit-tgl-akhir .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });

    $('#tanggal-add-minus-tgl-awal .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });
    $('#tanggal-add-minus-tgl-akhir .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });
    $('#tanggal-edit-minus-tgl-awal .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });
    $('#tanggal-edit-minus-tgl-akhir .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });
   



    $(document).on("click",".hapus-penambahan",function(){
        var detail_min = this.value;
        var invoice = $(this).attr('data-invoice');

        swal({
            title: "Yakin Hapus Data ini ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, Hapus!",
            closeOnConfirm: false
        }, function () {

            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>invoice/delete_detail_plus/",
                dataType: "JSON",
                data : "data-detail="+detail_min+"&data-inv="+invoice,
                success:function(data){
                    
                    if(data.rc=='0000'){
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Success Delete Data",
                                imageUrl: '<?= base_url("assets/img/success.png");?>'
                            }, function() {
                               location.reload();
                            });
                        }, 1000);
                    }else{
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Delete Failed",
                                imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                            }, function() {
                                location.reload();
                            });
                        }, 1000);
                    }
                    
                }

            });
           
        });
            
    });
    $(document).on("click",".hapus-otc",function(){
        var value = this.value;
        var inv_id = "<?= $this->uri->segment(3)?>";


        swal({
            title: "Yakin Hapus Data ini ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, Hapus!",
            closeOnConfirm: false
        }, function () {

            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>invoice/delete_otc/",
                dataType: "JSON",
                data : "inv_tag_id="+value+"&inv_id="+inv_id,
                success:function(data){
                    
                    if(data.rc=='0000'){
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Success Delete Data",
                                imageUrl: '<?= base_url("assets/img/success.png");?>'
                            }, function() {
                               location.reload();
                            });
                        }, 1000);
                    }else{
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Delete Failed",
                                imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                            }, function() {
                                location.reload();
                            });
                        }, 1000);
                    }
                    
                }

            });
           
        });
            
    });

    $(document).on("click",".hapus-cpe",function(){
        var value = this.value;
        var inv_id = "<?= $this->uri->segment(3)?>";


        swal({
            title: "Yakin Hapus Data ini ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, Hapus!",
            closeOnConfirm: false
        }, function () {

            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>invoice/delete_otc/",
                dataType: "JSON",
                data : "inv_tag_id="+value+"&inv_id="+inv_id,
                success:function(data){
                    
                    if(data.rc=='0000'){
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Success Delete Data",
                                imageUrl: '<?= base_url("assets/img/success.png");?>'
                            }, function() {
                               location.reload();
                            });
                        }, 1000);
                    }else{
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Delete Failed",
                                imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                            }, function() {
                                location.reload();
                            });
                        }, 1000);
                    }
                    
                }

            });
           
        });
            
    });
    $(document).on("click",".hapus-mrc",function(){
        var value = this.value;
        var inv_id = "<?= $this->uri->segment(3)?>";


        swal({
            title: "Yakin Hapus Data ini ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, Hapus!",
            closeOnConfirm: false
        }, function () {

            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>invoice/delete_otc/",
                dataType: "JSON",
                data : "inv_tag_id="+value+"&inv_id="+inv_id,
                success:function(data){
                    
                    if(data.rc=='0000'){
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Success Delete Data",
                                imageUrl: '<?= base_url("assets/img/success.png");?>'
                            }, function() {
                               location.reload();
                            });
                        }, 1000);
                    }else{
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Delete Failed",
                                imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                            }, function() {
                                location.reload();
                            });
                        }, 1000);
                    }
                    
                }

            });
           
        });
            
    });

    $(document).on("click",".hapus-pengurangan",function(){
        var detail_min = this.value;
        var invoice = $(this).attr('data-invoice');
        
        swal({
            title: "Yakin Hapus Data ini ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, Hapus!",
            closeOnConfirm: false
        }, function () {

            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>invoice/delete_detail_minus/",
                dataType: "JSON",
                data : "data-detail="+detail_min+"&data-inv="+invoice,
                success:function(data){
                    
                    if(data.rc=='0000'){
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Success Delete Data",
                                imageUrl: '<?= base_url("assets/img/success.png");?>'
                            }, function() {
                               location.reload();
                            });
                        }, 1000);
                    }else{
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Delete Failed",
                                imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                            }, function() {
                                location.reload();
                            });
                        }, 1000);
                    }
                    
                }

            });
           
        });
            
    });

    $('.edit-penambahan').click(function(){
        $('#edit-value').val($(this).attr('data-value'));
        $('#edit-cpe').val($(this).attr('data-cpe'));
        $('#edit-qty').val($(this).attr('data-qty'));
        $('#edit-speed').val($(this).attr('data-speed'));
        $('#edit-deskripsi').val($(this).attr('data-deskripi'));
        $('#edit-tgl_awal').val($(this).attr('data-tgl_awal'));
        $('#edit-tgl_akhir').val($(this).attr('data-tgl_akhir'));
        $('#edit-otc').val($(this).attr('data-otc'));
        $('#edit-mtc').val($(this).attr('data-mtc'));
        $('#edit-value-inv').val($(this).attr('data-invoice'));
    });
    $('.edit-pengurangan').click(function(){
        $('#edit-value-pengurangan').val($(this).attr('data-value'));
        //$('#edit-cpe-pengurangan').val($(this).attr('data-cpe'));
        $('#edit-qty-pengurangan').val($(this).attr('data-qty'));
        $('#edit-biaya-pengurangan').val($(this).attr('data-biaya'));
        $('#edit-speed-pengurangan').val($(this).attr('data-speed'));
        $('#edit-deskripsi-pengurangan').val($(this).attr('data-deskripi'));
        $('#edit-tgl_awal-pengurangan').val($(this).attr('data-tgl_awal'));
        $('#edit-tgl_akhir-pengurangan').val($(this).attr('data-tgl_akhir'));
        $('#edit-value-inv-pengurangan').val($(this).attr('data-invoice'));

        if($(this).attr('data-type')=="penambahan"){
            $('.type-plus').prop('checked', true);
        }else{
            $('.type-minus').prop('checked', true);

        }
    });
    



});
</script>