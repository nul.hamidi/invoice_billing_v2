<div class="modal-header bg-info">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="false">&times;</button>
    <h4 class="modal-title" id="myModalLabel"><?php echo $icon; ?> <?php echo $title; ?></h4>
</div>
<div class="modal-body">
    <div style="overflow-y:auto">
    <form method="post" action="<?php echo $action_url;?>" class="form-horizontal" enctype="multipart/form-data">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-12">

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Deskiripsi</label>
                        <div class="col-sm-9">
                            <input type="text"  name="deskripsi" id="deskripsi" placeholder="" class="form-control input-sm">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Qty</label>
                        <div class="col-sm-9">
                            <input type="number" min='0'  name="qty" id="qty" placeholder="" class="form-control input-sm">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Speed</label>
                        <div class="col-sm-9">
                            <input type="text"  name="speed" id="speed" placeholder="" class="form-control input-sm">
                        </div>
                    </div>
                    
                    <div class="form-group">                    
                        <label class="col-sm-3 control-label">Tgl Awal</label>
                        <div class="col-sm-9" id="tanggal">
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input autocomplete="off" type="text" class="form-control input-sm" name="tgl_awal" id="tgl_awal" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">                    
                        <label class="col-sm-3 control-label">Tgl Akhir</label>
                        <div class="col-sm-9" id="tanggal">
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input autocomplete="off" type="text" class="form-control input-sm" name="tgl_akhir" id="tgl_akhir" >
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">OTC</label>
                        <div class="col-sm-9">
                            <input type="number" min='0'  name="qty" id="qty" placeholder="" class="form-control input-sm">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">CPE/Prorate</label>
                        <div class="col-sm-9">
                            <input type="number" min='0'  name="qty" id="qty" placeholder="" class="form-control input-sm">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Jmlh Biaya</label>
                        <div class="col-sm-9">
                            <input type="number" min='0' readonly name="jlm_biaya_show" id="jlm_biaya_show" placeholder="" class="form-control input-sm">
                            <input type="hidden" min='0' readonly name="jlm_biaya" id="jlm_biaya" placeholder="" class="form-control input-sm">
                        </div>
                    </div>

                </div> <!-- end col-12 -->
                
            </div><!-- end row -->
            
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            
            <div class="col-sm-12">
                <div class="pull-right"> 
                    <a href="<?php echo base_url('invoice/lists');?>" class="btn btn-default">Kembali</a>
                    <button type="submit" class="btn btn-info ">Simpan</button>
                    
                </div>
            </div>
            <div class="col-sm-6"> 
            </div>
        </div>
        <!-- /.box-footer -->
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-flat btn-default" data-dismiss="modal">Close</button>
</div>
<script>
    <?php 
        if (isset($css)) {
            for ($i = 0; $i < count($css); ++$i) {
                echo '<link href="'.base_url().'assets/'.$css[$i].'" rel="stylesheet" />';
            }
        }
        ?>
    <?php 
        if (isset($js)) {
            for ($i = 0; $i < count($js); ++$i) {
                echo '<script type="text/javascript" src="'.base_url().'assets/'.$js[$i].'"></script>';
            }
        }
        ?>
<?php //echo $this->jquery_validation->run('.form-horizontal');?>
const base_url = '<?php echo site_url(); ?>'

$(document).ready(function () {

   

    $('#nominal_inv').mask('000.000.000.000', {reverse: true});
    $('#tanggal .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });
    

});
</script>
