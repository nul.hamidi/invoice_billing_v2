<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('layouts/alert'); ?>
            <?php // echo '<pre>'; print_r($this->session->userdata()); ?>

            <div class="nav-tabs-custom-aqua">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <div class=" pull-right">
                            <a href="<?php echo base_url('Export/export_invoice');?>"> 
                                <button class="btn btn-success btn-sm">
                                    <i class="fa fa-download"></i> Export Excel
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
                
                <div class="tab-content"  style="overflow-y:auto">
                    <!-- /.tab-pane -->
                    
                    <div class="tab-pane active">
                        <table  class="table table-striped" id="invoice" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="3%">No</th>
                                    <th width="8%">No Invoice</th>
                                    
                                    <th width="10%">No WO</th>
                                    <th width="10%">Tanggal Invoice</th>
                                    <th width="10%">Tanggal Jth Tempo</th>
                                    <th width="10%">Nominal</th>
                                    <th width="5%">Status</th>
                                    <th width="8%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('report/import_project');?>

<script type="text/javascript">
$(document).ready(function () {
    oTable = $('#invoice').DataTable({
            processing: true, 
            serverSide: true, 
            order: [], 
            ajax: {
                url : "<?php echo site_url("invoice/get_invoice") ?>",
                type : 'get',
                data: function ( data ) {
                    data.status = $('#arr_filter').val(); 
                }
                          
            },
            columnDefs: [
                {
                    targets: 6,
                    orderable: true,
                    className: 'text-center',

                }

            ],
        });

    $(document).on("click",".hapus-invoice",function(){
        var encrypt = this.value;
        
        swal({
            title: "Yakin Hapus Data ini ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, Hapus!",
            closeOnConfirm: false
        }, function () {

            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>invoice/delete/",
                dataType: "JSON",
                data : "data="+encrypt+'&ib='+$(this).attr('data-ib'),
                success:function(data){
                    
                    if(data.rc=='0000'){
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Success Delete Data",
                                imageUrl: '<?= base_url("assets/img/success.png");?>'
                            }, function() {
                               oTable.ajax.reload();
                            });
                        }, 1000);
                    }else{
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Delete Failed",
                                imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                            }, function() {
                                oTable.ajax.reload();
                            });
                        }, 1000);
                    }
                    
                }

            });
           
        });
            
    });

    $(document).on("click",".approve_invoice",function(){

       var inv=$(this).attr("id");
       var u = inv.split('&');
       var invoice_id = u[0];
       var is_approve = u[1];
       var label = "";
       var confirm = "";
        
       if(is_approve=='1'){
            label = "Ya, Un- Approve";
            confirm = "Un-Approve Invoice ini ?";
       }else{
            label = "Ya, Approve";
            confirm = "Approve Invoice ini ?"
       }
    
        swal({
            title: confirm,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: label,
            closeOnConfirm: false
        }, function () {

            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>invoice/approve_invoice/",
                dataType: "JSON",
                data : "invoice_id="+invoice_id,
                success:function(data){
                    
                    setTimeout(function() {
                        swal({
                            title: "Notification!",
                            text: "Success, Invoice Approved",
                            imageUrl: '<?= base_url("assets/img/success.png");?>'
                        }, function() {
                            oTable.ajax.reload();
                        });
                    }, 1000);
                    
                }
            });
        });    
    });


});
</script>