<!-- Modal -->
<div class="modal fade" id="AddModalMinus" tabindex="-1" role="dialog" aria-hidden="true" enctype="multipart/form-data">
    <div class="modal-dialog modal-md" >
        <div class="modal-content">
            <div class="modal-header bg-info">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="false">&times;</button>
                <h4 class="modal-title" id="myModalLabel"> Tambah Detail Pengurangan</h4>
            </div>
            <div class="modal-body">
                <div>
                <form method="post" action="<?php echo $action_url_minus;?>" class="form-horizontal" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12">

                                 <div class="form-group">
                                    <label class="col-sm-3 control-label">Deskripsi</label>
                                    <div class="col-sm-9">
                                        <input type="text"  name="deskripsi" id="deskripsi-minus" placeholder="" class="form-control input-sm">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Qty</label>
                                    <div class="col-sm-9">
                                        <input type="text" min='0'  name="qty" id="qty-minus" placeholder="" class="form-control input- input_mask">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Speed</label>
                                    <div class="col-sm-9">
                                        <input type="text"  name="speed" id="speed-minus" placeholder="" class="form-control input-sm">
                                    </div>
                                </div>
                                
                                <div class="form-group">                    
                                    <label class="col-sm-3 control-label">Tgl Awal</label>
                                    <div class="col-sm-9" id="tanggal-add-minus-tgl-awal">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input autocomplete="off" type="text" class="form-control input-sm" name="tgl_awal" id="tgl_awal-minus" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">                    
                                    <label class="col-sm-3 control-label">Tgl Akhir</label>
                                    <div class="col-sm-9" id="tanggal-add-minus-tgl-akhir">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input autocomplete="off" type="text" class="form-control input-sm" name="tgl_akhir" id="tgl_akhir-plus" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Biaya</label>
                                    <div class="col-sm-9">
                                        <input type="text" min='0'  name="biaya" id="biaya-minus" placeholder="" class="form-control input-sm input_mask">
                                    </div>
                                </div>
                                <!-- radio -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Type</label>
                                    <div class="col-sm-9">
                                        <label>
                                            <input type="radio" required name="type" class="minimal" value="pengurangan">
                                            Pengurangan
                                        </label>
                                        <label>
                                            <input type="radio" required name="type" class="minimal" checked value="penambahan">
                                            Penambahan
                                        </label>
                                    </div>
                                   
                                </div>
                                <!-- <div class="form-group">
                                    <label class="col-sm-3 control-label">OTC</label>
                                    <div class="col-sm-9">
                                        <input type="text" min='0'  name="otc" id="otc-minus" placeholder="" class="form-control input-sm input_mask">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">MTC/Biaya</label>
                                    <div class="col-sm-9">
                                        <input type="text" min='0'  name="mtc" id="mtc-minus" placeholder="" class="form-control input-sm input_mask">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">CPE/Prorate</label>
                                    <div class="col-sm-9">
                                        <input type="text" min='0'  name="cpe" id="cpe-minus" placeholder="" class="form-control input-sm input_mask">
                                    </div>
                                </div> -->
                                <!-- <div class="form-group">
                                    <label class="col-sm-3 control-label">Jumlah Biaya</label>
                                    <div class="col-sm-9">
                                        <input type="number" min='0' readonly name="jlm_biaya_show" id="jlm_biaya_show" placeholder="" class="form-control input-sm">
                                        <input type="hidden" min='0' readonly name="jlm_biaya" id="jlm_biaya" placeholder="" class="form-control input-sm">
                                    </div>
                                </div> -->

                            </div> <!-- end col-12 -->
                            
                        </div><!-- end row -->
                        
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        
                        <div class="">
                            <div class="pull-right"> 
                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="false">Close</button>
                                <button type="submit" class="btn btn-info ">Simpan</button>
                                
                            </div>
                        </div>
                        
                    </div>
                    <!-- /.box-footer -->
                    </form>
                </div>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="EditModalPengurangan" tabindex="-1" role="dialog" aria-hidden="true" enctype="multipart/form-data">
    <div class="modal-dialog modal-md" >
        <div class="modal-content">
            <div class="modal-header bg-info">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="false">&times;</button>
                <h4 class="modal-title" id="myModalLabel"> Ubah Detail Pengurangan</h4>
            </div>
            <div class="modal-body">
                <div>
                <form method="post" action="<?php echo $action_url_update_minus;?>" class="form-horizontal" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12">

                                 <div class="form-group">
                                    <label class="col-sm-3 control-label">Deskripsi</label>
                                    <div class="col-sm-9">
                                        <input type="text"  name="deskripsi" id="edit-deskripsi-pengurangan" placeholder="" class="form-control input-sm">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Qty</label>
                                    <div class="col-sm-9">
                                        <input type="text" min='0'  name="qty" id="edit-qty-pengurangan" placeholder="" class="form-control input- input_mask">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Speed</label>
                                    <div class="col-sm-9">
                                        <input type="text"  name="speed" id="edit-speed-pengurangan" placeholder="" class="form-control input-sm">
                                    </div>
                                </div>
                                
                                <div class="form-group">                    
                                    <label class="col-sm-3 control-label">Tgl Awal</label>
                                    <div class="col-sm-9" id="tanggal-edit-minus-tgl-awal">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input autocomplete="off" type="text" class="form-control input-sm" name="tgl_awal" id="edit-tgl_awal-pengurangan" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">                    
                                    <label class="col-sm-3 control-label">Tgl Akhir</label>
                                    <div class="col-sm-9" id="tanggal-edit-minus-tgl-akhir">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input autocomplete="off" type="text" class="form-control input-sm" name="tgl_akhir" id="edit-tgl_akhir-pengurangan" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Biaya</label>
                                    <div class="col-sm-9">
                                        <input type="text" min='0'  name="biaya" id="edit-biaya-pengurangan" placeholder="" class="form-control input-sm input_mask">
                                    </div>
                                </div>
                                <!-- radio -->
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Type</label>
                                    <div class="col-sm-9">
                                        <label>
                                            <input type="radio" required name="type" class="minimal type-minus" value="pengurangan" >
                                            Pengurangan
                                        </label>
                                        <label>
                                            <input type="radio" required name="type" class="minimal type-plus"  value="penambahan">
                                            Penambahan
                                        </label>
                                    </div>
                                   
                                </div>
                                <input type="hidden" name="value" id="edit-value-pengurangan">
                                <input type="hidden" name="value-invoice" id="edit-value-inv-pengurangan">

                            </div> <!-- end col-12 -->
                            
                        </div><!-- end row -->
                        
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        
                        <div class="">
                            <div class="pull-right"> 
                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="false">Close</button>
                                <button type="submit" class="btn btn-info ">Update</button>
                                
                            </div>
                        </div>
                        
                    </div>
                    <!-- /.box-footer -->
                    </form>
                </div>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->





