<div class="modal fade" id="ModalTagihanCPE" tabindex="-1" role="dialog" aria-hidden="true" enctype="multipart/form-data">
    <div class="modal-dialog modal-md" >
        <div class="modal-content">
            <div class="modal-header bg-info">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="false">&times;</button>
                <h4 class="modal-title" id="myModalLabel"> Tambah Tagihan CPE</h4>
            </div>
            <div class="modal-body">
                <div>
                <form method="post" action="<?php echo $action_url_tambah_cpe;?>" class="form-horizontal" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="form-group">
                                    
                                    <div class="col-sm-12">
                                        Pilih Tagihan
                                        <select class="select2 form-control" name="pilih_tagihan" id="pilih_tagihan">
                                            <option value=""></option>
                                            <?php
                                            foreach ($uncheck_tagihan_cpe as $key ) {
                                            ?>
                                                <option value="<?php echo$key->tagihan_id;?>"><?php echo$key->deskripsi." | ".date('d M Y',strtotime($key->start_date)).' - '.date('d M Y',strtotime($key->end_date)).' | '.number_format($key->jumlah_biaya,0,',','.');?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                 <input type="hidden" name="invoice_id" value="<?= $this->uri->segment(3);?>">

                            </div> <!-- end col-12 -->
                            
                        </div><!-- end row -->
                        
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        
                        <div class="">
                            <div class="pull-right"> 
                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="false">Close</button>
                                <button type="submit" class="btn btn-info ">Tambah</button>
                                
                            </div>
                        </div>
                        
                    </div>
                    <!-- /.box-footer -->
                    </form>
                </div>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->
