<style type="text/css">
    .error{
        color: #a94442;
    }
</style>
<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('layouts/alert'); ?>
            <?php // echo '<pre>'; print_r($this->session->userdata()); ?>
            <div class="nav-tabs-custom-aqua">                            

                    <!-- Horizontal Form -->
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?php echo $title;?></h3>
                            </div>
                        <!-- /.box-header -->
                            <!-- form start -->
                            <form method="post" action="<?php echo $action_url;?>" class="form-horizontal" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Pilih No WO</label>
                                            <div class="col-sm-4">
                                                <select class="select2 form-control" name="pilih_no_wo" disabled>
                                                    <option value=""></option>
                                                   <?php foreach ($get_data_wo as $value) { 
                                                        if($project==encrypt_url($value->project_id)){
                                                    ?>
                                                        
                                                        <option value='<?= encrypt_url($value->project_id); ?>' selected="selected"><?= $value->no_wo;?></option>

                                                    <?php
                                                            }else{
                                                    ?>
                                                        <option value='<?= encrypt_url($value->project_id); ?>'><?= $value->no_wo;?></option>
                                                    <?php
                                                            }
                                                        }
                                                    ?>  
                                                </select>
                                            </div>
                                            <label class="col-sm-2 control-label">Nama Pelanggan</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="name_client" id="name_client"  class="form-control input-sm" readonly="readonly" value="<?= $nama_pelanggan;?>">
                                            </div>
                                            
                                        </div> 
                                        <div class="form-group" style="display:none">
                                            
                                            <label class="col-sm-2 control-label">Atas Nama/PIC</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="pic_name" id="pic_name"  class="form-control input-sm" readonly="readonly" value="<?= $nama_pic;?>">
                                            </div>
                                            
                                            <label class="col-sm-2 control-label">Alamat Surat</label>
                                            <div class="col-sm-4">
                                                    <textarea class="form-control" name="address_mail" id="address_mail" readonly="readonly"><?= $alamat_surat;?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group" style="display:none">
                                           
                                            <label class="col-sm-2 control-label">Nama Alias Pelanggan</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="name_alias" id="name_alias"  class="form-control input-sm" readonly="readonly" value="<?=$nama_alias;?>">
                                            </div>
                                          
                                            <label class="col-sm-2 control-label">Alamat Invoice</label>
                                            <div class="col-sm-4">
                                                <textarea class="form-control" name="address_inv" id="address_inv" readonly="readonly"><?= $alamat_invoice;?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group" style="display:none">
                                       
                                            <label class="col-sm-2 control-label">Divisi / Bagian</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="devisi" id="devisi"  class="form-control input-sm" readonly="readonly"  value="<?=$divisi;?>">
                                            </div>
                    
                                            <label class="col-sm-2 control-label">NPWP Klien</label>
                                            <div class="col-sm-4">
                                                 <input type="text"  name="npwp_client" id="npwp_client"  class="form-control input-sm" readonly="readonly" value="<?=$npwp;?>">
                                            </div>
                                        </div>

                                        

                                    </div> <!-- end col-12 -->
                                   
                                </div><!-- end row -->
                               <br>
                                <div class="row">
                                    <div class="col-lg-12">
                                    
                                        <div class="form-group" style="display:none">
                                            <label class="col-sm-2 control-label">No Surat Permohonan</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="no_sp" id="no_sp" placeholder="NO SP" class="form-control input-sm" value="<?=$no_sp;?>">
                                                <span>xxxx/D3.400/HK200/TSAT/11.2020</span>
                                            </div>

                                            <label class="col-sm-2 control-label">Kode Project</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="project_code" id="project_code"  class="form-control input-sm" readonly="readonly" value="<?=$kode_project;?>">
                                            </div>
                                            
                                            
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Nama Project</label>
                                            <div class="col-sm-10">
                                                <input type="text"  name="project_name" id="project_name"  class="form-control input-sm" readonly="readonly" value="<?= $nama_project;?>">
                                            </div>
                                            
                                            
                                            
                                        </div>

                                        <div class="form-group" style="display:none">
                                            <label class="col-sm-2 control-label">No WO</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="no_wo" id="no_wo"  class="form-control input-sm" readonly="readonly" value="<?= $no_wo;?>">
                                            </div>

                                            <label class="col-sm-2 control-label">Tanggal Invoice</label>
                                            <div class="col-sm-4" id="tanggal">
                                               <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input autocomplete="off" type="text" class="form-control input-sm" name="date_inv" id="date_inv" value="<?= $tgl_invoice;?>" >
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group" style="display:none">
                                        
                                            <label class="col-sm-2 control-label">Tanggal Start Project</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="date_start" id="date_start" class="form-control input-sm" readonly="readonly" value="<?= $start_date;?>">
                                            </div>

                                            <label class="col-sm-2 control-label">Tanggal Jth Tempo</label>
                                            <div class="col-sm-4">
                                                 <input type="text"  name="date_tempo" id="date_tempo" class="form-control input-sm" readonly="readonly" value="<?= $tgl_jthtempo;?>">
                                            </div>
                                            
                                        </div>

                                        <div class="form-group" style="display:none">
                                            <label class="col-sm-2 control-label">Tanggal End Project</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="date_end" id="date_end" class="form-control input-sm" readonly="readonly" value="<?= $end_date;?>">
                                            </div>

                                            <label class="col-sm-2 control-label">Nominal Invoice</label>
                                            <div class="col-sm-4">
                                                 <input type="text"  name="nominal_inv" id="nominal_inv" class="form-control input-sm" readonly="readonly" value="<?= $nominal?>">
                                            </div>
                                            
                                        </div>
                                        <div class="form-group">
                                             <label class="col-sm-2 control-label" >No Invoice</label>
                                            <div class="col-sm-4">
                                                <input type="text"  name="inv_no" id="inv_no"  class="form-control input-sm" readonly="readonly" value="<?= $no_invoice;?>">
                                            </div>
                                            <label class="col-sm-2 control-label">Lama hari (Due Date)</label>
                                            <div class="col-sm-4">
                                                 <input type="text"  name="lama_hari" id="lama_hari" placeholder="" class="form-control input-sm" value="<?= $lama_hari;?>">
                                            </div>
                                        </div>
                                        <?php
                                        if($is_approved=='0'){
                                            $show= "display:none";
                                        }else{
                                            $show= "";
                                        }
                                        ?>

                                        <div class="form-group" style="<?php echo$show;?>">
                                            <label class="col-sm-2 control-label">Tanggal Invoice</label>
                                            <div class="col-sm-4" id="tanggal">
                                               <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input autocomplete="off" type="text" class="form-control input-sm" name="tgl_inv" id="tgl_inv" value="<?php echo isset($tgl_invoice)?$tgl_invoice:'';?>" >
                                                </div>
                                            </div>

                                            <label class="col-sm-2 control-label">Tanggal Jatuh Tempo</label>
                                            <div class="col-sm-4" id="tanggal">
                                               <div class="input-group date">
                                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input autocomplete="off" type="text" class="form-control input-sm" name="tgl_jth_tempo" id="tgl_jth_tempo"  value="<?php echo isset($tgl_jthtempo)?$tgl_jthtempo:'';?>">
                                                </div>
                                            </div>
                                            
                                        </div>
                                    
                                        <div class="form-group">
                                           
                                            <label class="col-sm-2 control-label">Pilih Bank</label>
                                            <div class="col-sm-4">
                                                <select class="select2 form-control" name="bank">
                                                    <option value=""></option>
                                                    <?php foreach ($get_data_bank as $value) { 
                                                        if($bank == encrypt_url($value->bank_id)){
                                                    ?>
                                                        
                                                        <option value='<?= encrypt_url($value->bank_id); ?>' selected="selected"><?= $value->nama_bank.' - '.$value->no_rekening;?></option>

                                                    <?php }else{ ?>
                                                        <option value='<?= encrypt_url($value->bank_id); ?>'><?= $value->nama_bank.' - '.$value->no_rekening;?></option>
                                                    
                                                    <?php
                                                            }
                                                        }
                                                    ?>  
                                                </select>
                                            </div>
                                          
                                        </div>
                                        <input type="hidden" name="project" id="project" value="<?= $project;?>">
                                        <input type="hidden" name="invoice_id" id="invoice_id" value="<?= $ib;?>">
                                        <input type="hidden" name="ib" id="ib" value="<?= $ib;?>">
                                    </div><!-- end col-12 -->
                                </div> <!-- end row -->
                               
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                
                                <div class="col-sm-6">
                                    <div class="pull-right"> 
                                        <a href="<?php echo base_url('invoice/lists');?>" class="btn btn-default">Kembali</a>
                                        <button type="submit" class="btn btn-info ">Update</button>
                                        
                                    </div>
                                </div>
                                <div class="col-sm-6"> 
                                </div>
                            </div>
                            <!-- /.box-footer -->
                            </form>
                        </div>
          <!-- /.box -->
                    
               
            </div>
        </div>
    </div>
</section>
<script>
<?php //echo $this->jquery_validation->run('.form-horizontal');?>
const base_url = '<?php echo site_url(); ?>'

function nominal_change(value){
    //console.log();
    var x = terbilang(value.replace(/\./g,''));
    $("#terbilang").val(x);
}


$(document).ready(function () {



    $('.select2').select2({
        placeholder: "Please Select"
    });

    $('.select2').on('select2:select', function (e) {
        var data = $(".select2 option:selected").val();
        //alert(data);
            $.ajax({
                type : "GET",
                url  : "<?php echo base_url('invoice/get_wo_id/')?>"+data,
                success:function(response){
                
                    $.each(JSON.parse(response), function( index, item ) {
                        
                        if(item.rc!='0005'){
                            $('#project_name').val(item.nama_project);
                            $('#project_code').val(item.kode_project);
                            $('#date_start').val(item.start_date);
                            $('#date_end').val(item.end_date);
                            $('#project').val(item.project);
                            $('#name_client').val(item.nama_pelanggan);
                            $('#name_alias').val(item.nama_alias);
                            $('#devisi').val(item.divisi);
                            $('#pic_name').val(item.nama_pic);
                            $('#address_mail').val(item.alamat_surat);
                            $('#address_inv').val(item.alamat_invoice);
                            $('#npwp_client').val(item.npwp);
                            $('#no_wo').val(item.no_wo);
                        }else{

                            alert('Gagal load data, silahkan pilih kembali');
                        }
                       

                    });

                     
                }
            });
    });
    $('#nominal_inv').mask('000.000.000.000', {reverse: true});

    $('#tanggal .input-group.date').datepicker({
        format: "yyyy-m-d",
        viewMode: "date", 
        minViewMode: "date"
    });
   

    $(".form-horizontal").validate({
            rules: {
                date_inv: "required",

            },
            messages: {
                date_inv:{
                    required:"<i class='fa fa-times'></i> Tanggal invoice harus diisi"
                },
                
            },
            highlight: function (element) {
                $(element).parent().parent().addClass("has-error")
                $(element).parent().addClass("has-error")
            },
            unhighlight: function (element) {
                $(element).parent().removeClass("has-error")
                $(element).parent().parent().removeClass("has-error")
            }
    });

    

});
</script>
