<!DOCTYPE  html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="utf-8">
        <title>Reports</title>

        <style type="text/css">
             @media all {
                .page-break { display: none; }
            }

            @media print {
                
                @page {
                    margin-top: 25px;
                    margin-bottom: 25px;
                    margin-left: 10px;
                    margin-right: 10px;
                }
                .page-break  { display: block; page-break-before: always; }
                .page-break-after { display: block; page-break-after: always; }
                .page-break  {page-break-after: always;}
                .page {
                    margin: ;
                    border: none;
                    border-radius: none;
                    width: initial;
                    min-height: initial;
                    box-shadow: none;
                    background: initial;
                    /*page-break-after: always;*/
                }
                .page_landscape {
                    margin: 0;
                    border: initial;
                    border-radius: initial;
                    width: initial;
                    min-height: initial;
                    box-shadow: initial;
                    background: initial;
                    page-break-after: always;
                }
                
                thead {display: table-header-group;}
                
                .page {
                    -webkit-box-shadow: none;
                    -moz-box-shadow:    none;
                    box-shadow:         none; 
                }

            }
            body {
                margin:0;
                padding:0;
                line-height: 1.5em;
                font-family: Times New Roman;
                font-size: 12px;
                color: #000000;
                background-color: #ffffff;
            }
            a:link, a:visited { color: #0066CC; text-decoration: none} 
            a:active, a:hover { color: #008800; text-decoration: underline}

            #templatemo_container_wrapper {
                /*background: url(images/templatemo_side_bg.gif) repeat-x;*/
                background: #ffffff;
            }
            #templatemo_container {
                margin: 0px auto;
                /*background: url(images/templatemo_content_bg.gif);*/
                background: #FFFFFF;
            }
            #templatemo_top {
                clear: left;
                height: 25px;   /* 'padding-top' + 'height' must be equal to the 'background image height' */
                padding-top: 42px;
                padding-left: 30px;
                background: url(images/templatemo_top_bg.gif) no-repeat bottom;
            }
            #templatemo_header {
                clear: left;
                padding-top: 2px;
                height: 60px;
                text-align: center;
                font-weight: bold;
                font-size: 24px;
                color: #000000;
                border: 1px solid;
                /*background: url(images/templatemo_header_bg.gif) no-repeat;*/
            }
            #inner_header {
                height: 30px;
                background: url(images/templatemo_header.jpg) no-repeat center center;
            }
            #templatemo_left_column {
                clear: left;
                float: left;
                width: 100%; 
            }
            #templatemo_right_column {
                float: right;
                width: 216px;
                padding-right: 15px;
            }
            #templatemo_footer {
                clear: both;
                /*padding-top: 18px;*/
                height: 15px;
                text-align: center;
                font-size: 11px;
                /*background: url(images/templatemo_footer_bg.gif) no-repeat;*/
                color: #ffffff;
            }
            #templatemo_footer a {
                color: #666666;
            }
            #templatemo_site_title {
                padding-top: 65px;
                font-weight: bold;
                font-size: 28px;
                color: #000000;
            }
            #templatemo_site_slogan {
                padding-top: 14px;
                font-weight: bold;
                font-size: 13px;
                color: #AAFFFF;
            }
            .templatemo_spacer {
                clear: left;
                height: 18px;
            }
            .templatemo_pic {
                float: left;
                margin-right: 10px;
                margin-bottom: 10px;
                border: 1px solid #000000;
            }
            .section_box {
                margin: 10px;
                padding: 10px;
                border: 1px dashed #ffffff;
                background: #FFFFFF;
                border: 1px solid #000000;
            }
            .section_box2 {
                clear: left;
                margin-top: 10px;
                background: #ffffff;
                color: #000000;
                font-weight: bold;
                border: 1px solid #000000;
            }
            .section_box3 {
                clear: left;
                margin-top: 10px;
                background: #ffffff;
                color: #000000;
                border: 1px;
            }
            .text_area {
                padding-top: 10px;
            }
            .publish_date {
                clear: both;
                margin-top: 10px;
                color: #999999;
                font-size: 11px;
                font-weight: bold;
            }
            .title {
                padding-bottom: 12px;
                font-size: 18px;
                font-weight: bold;
                color: #000000;
            }
            .subtitle {
                padding-bottom: 6px;
                font-size: 14px;
                font-weight: bold;
                color: #666666;
            }
            .post_title_main {
                padding: 6px;
                padding-left: 10px;
                background: #cccccc;
                font-size: 14px;
                font-weight: bold;
                color: #000000;
                border-bottom: 1px solid #000000;
                text-align:left;
            }
            .post_title {
                padding: 6px;
                padding-left: 10px;
                background: #cccccc;
                font-size: 14px;
                font-weight: bold;
                color: #000000;
                border-bottom: 1px solid #000000;
                text-align:left;
            }
            .templatemo_menu {
                list-style-type: none;
                margin: 10px;
                margin-top: 0px;
                padding: 0px;
                width: 195px;
            }
            .templatemo_menu li a{
                background: #F4F4F4 url(images/button_default.gif) no-repeat;
                font-size: 13px;
                font-weight: bold;
                color: #000000;
                display: block;
                width: auto;
                margin-bottom: 2px;
                padding: 5px;
                padding-left: 12px;
                text-decoration: none;
            }
            * html .templatemo_menu li a{ 
                width: 190px;
            }
            .templatemo_menu li a:visited, .templatemo_menu li a:active{
                color: #000000;
            }
            .templatemo_menu li a:hover{
                background: #EEEEEE url(images/button_active.gif) no-repeat;
                color: #FF3333;
            }#templatemo_container_wrapper #templatemo_container #templatemo_left_column .text_area .section_box2 .post_title_main strong td {
                color: #000000;
            }
            #templatemo_container_wrapper #templatemo_container #templatemo_left_column .text_area .section_box2 .post_title_main {
                color: #000000;
            }
            div {
                color: #000000;
            }
            .page {
                width: 25cm;
               /* height:26.7cm;*/
                /*padding: 1cm;*/
                margin: auto;
                /*border: 1px solid;*/
                /*border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);*/
                
            }
            .page_mini {
                width: 8.5cm;
                height:10cm;
                padding: 0cm;
                margin: 0.5cm auto;
                border: 1px #D3D3D3 solid;
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }
            .page_landscape {
                width: 28cm;
                height:21cm;
                padding: 2cm;
                margin: 1cm auto;
                border: 1px #D3D3D3 solid;
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }
            .table-print {
                /*font-size: 10pt;*/
                /*font-weight: normal;*/
                padding: 0px;
                margin: 0px;
                border-top: 1.5px solid #333333;
                border-left: 1.5px solid #333333;
                border-collapse: collapse;
                font-family: Times New Roman;
            }

            .table-print th {
                text-align: center;
                /*font-weight: bold;*/
                border-right: 1.5px solid #333333;
                border-bottom: 1.5px solid #333333;
                font-size: 11px;
               /* padding: 5px;
                margin:0px;*/
                font-family: 'Times New Roman';
            }

            .table-print td {
                
                border-right: 1.5px solid #333333;
                border-bottom: 1.5px solid #333333;
                font-size: 11px;
                /*padding: 1px;
                margin:0px;*/
              /*  font-weight: normal;*/
            }
            .table-biaya {
                font-size: 12px;
                font-family: Arial;
                font-weight: normal;
                padding: 0px;
                margin: 0px;
                border-top: 1.5px solid #333333;
                border-left: 1.5px solid #333333;
                border-collapse: collapse;
            }

            .table-biaya th {
                text-align: center;
                font-weight: bold;
                border-right: 1.5px solid #333333;
                border-bottom: 1.5px solid #333333;
                padding: 5px;
                margin:0px;
            }

            .table-biaya td {
                
                border-right: 1.5px solid #333333;
                border-bottom: 1.5px solid #333333;
                padding: 1px;
                margin:0px;
                font-weight: normal;
            }
            .text-center{
                text-align:center;
            }
            .text-left{
                text-align:left;
            }
            .text-right{
                text-align:right;
            }

           .transform{
            -ms-transform: rotate(180deg); /* IE 9 */
            -webkit-transform: rotate(180deg); /* Safari 3-8 */
            transform: rotate(180deg);
           }


        </style>
    </head>
    <body>

        <?php

            //echo $this->session->userdata('id_invoice');
            function tanggal($date, $format=null){
                $d = explode("-", $date);
                if($format==null){
                    if($d[1]=='01'){
                        $bulan = "Januari";
                    }else if($d[1]=='02'){
                        $bulan = "Februari";
                    }else if($d[1]=='03'){
                        $bulan = "Maret";
                    }else if($d[1]=='04'){
                        $bulan = "April";
                    }else if($d[1]=='05'){
                        $bulan = "Mei";
                    }else if($d[1]=='06'){
                        $bulan = "Juni";
                    }else if($d[1]=='07'){
                        $bulan = "Juli";
                    }else if($d[1]=='08'){
                        $bulan = "Agustus";
                    }else if($d[1]=='09'){
                        $bulan = "September";
                    }else if($d[1]=='10'){
                        $bulan = "Oktober";
                    }else if($d[1]=='11'){
                        $bulan = "November";
                    }else if($d[1]=='12'){
                        $bulan = "Desember";
                    }else{
                        $bulan = "";
                    }
                
                }else{
                    if($d[1]=='01'){
                        $bulan = "Jan";
                    }else if($d[1]=='02'){
                        $bulan = "Feb";
                    }else if($d[1]=='03'){
                        $bulan = "Mar";
                    }else if($d[1]=='04'){
                        $bulan = "Apr";
                    }else if($d[1]=='05'){
                        $bulan = "Mei";
                    }else if($d[1]=='06'){
                        $bulan = "Jun";
                    }else if($d[1]=='07'){
                        $bulan = "Jul";
                    }else if($d[1]=='08'){
                        $bulan = "Agus";
                    }else if($d[1]=='09'){
                        $bulan = "Sep";
                    }else if($d[1]=='10'){
                        $bulan = "Okt";
                    }else if($d[1]=='11'){
                        $bulan = "Nov";
                    }else if($d[1]=='12'){
                        $bulan = "Des";
                    }else{
                        $bulan = "";
                    }
                }

                if($format==null){
                    $date_fix = intval($d[2])." ".$bulan." ".$d[0];
                }else{
                    $date_fix = intval($d[2])."-".$bulan."-".$d[0];
                }
                return $date_fix;
            }


           
        ?>
        
        <div class="page">
            <div id="templatemo_container_wrapper">
                <div id="templatemo_container">
                    <!-- <div id="" style="margin-top: 100px">
                        <table width="100%" border="0">
                            <tr>
                                <td width="250px" align="center" style="vertical-align:middle;"></td>
                                <td width="auto"></td>
                                <td width="300px" height="60" align="right" style="vertical-align:middle;font-size: 11px;color:#000000">
                                   <img src="<?= base_url('/assets/img/logo_telkomsat.png');?>" style="text-align: right; margin-right: 5px; width: 30%">
                                    
                                </td>
                            </tr>   
                            <tr><td colspan="3" style="border-top: 3px; padding-top: 60px;"></td></tr>
                        </table>
                    </div> -->
                    <!-- <center><h1><b><u>Surat Jalan</u></b></h1></center> -->
                    

                    <div class="text_area">
                        <div>
                            <table width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td width="40%"></td>
                                        <td width="25%"></td>
                                        <td>Jakarta, <?= tanggal($report_data->tgl_invoice);?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <table width="50%">
                                <tbody>
                                    <tr>
                                        <td>No SL</td>
                                        <td>:</td>
                                        <td>............./D3.400/HK200/TSAT/<?= $report_data->tgl_invoice? date('m.Y',strtotime($report_data->tgl_invoice)):date('m.Y');?></td>
                                    </tr>
                                    <tr>
                                        <td>Lampiran</td>
                                        <td>:</td>
                                        <td>Satu Berkas Invoice</td>
                                    </tr>
                                    <tr>
                                        <td>Perihal</td>
                                        <td>:</td>
                                        <td><u>Permohonan Pembayaran Invoice</u></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="padding-top: 10px">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td>Kepada Yth.</td>
                                    </tr>
                                    <tr>
                                        <td><b><?= $report_data->nama_pelanggan?></b></td>
                                    </tr>
                                    <tr>
                                        <td><b><i><?= $report_data->divisi?></i></b></td>
                                    </tr>
                                    <tr>
                                        <td><?= $report_data->alamat_surat?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                        <div style="padding-top: 15px; ">
                            <table>
                                <tbody>
                                    <tr>
                                        <td> Dengan Hormat,</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="line-height: 18px; margin-bottom: 10px">
                                           Kami atas nama PT. Telkom Satelit Indonesia (TELKOMSAT) mengucapkan terima kasih atas kerjasama yang<br> telah berjalan dengan baik, semoga kerjasama ini dapat berjalan lancar dan lebih baik lagi.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Melalui surat ini, mohon <?php echo $report_data->nama_alias;?> dapat melakukan pembayaran atas invoice kami sebagai berikut:</td>
                                    </tr>
                                </tbody>
                            </table>
                           
                        </div>
                        <div style=" margin-left: 4%;margin-right: 0%; margin-top: -5px">
                            <table width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td width="30%">Nomor Invoice</td>
                                        <td width="3%">:</td>
                                        <td><?= $report_data->no_invoice?></td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Project</td>
                                        <td>:</td>
                                        <td><?= $report_data->nama_project?></td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>WO</td>
                                        <td>:</td>
                                        <td><?= $report_data->no_wo?></td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td>Tanggal Invoice</td>
                                        <td>:</td>
                                        <td><?= date('d-M-Y',strtotime($report_data->tgl_invoice))?></td>
                                    </tr>
                                    <tr>
                                        <td>5.</td>
                                        <td>Nominal Invoice</td>
                                        <td>:</td>
                                        <td>Rp. &nbsp; &nbsp;&nbsp;&nbsp;
                                        <?php
                                             $nominal = $report_data->nominal;
                                             $ppn = ceil((10 / 100) * $report_data->nominal);
                                             $total = round($nominal + $ppn);

                                            echo number_format($total, 0, ",", ".");
                                        ?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><?= terbilang($total);?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                                            
                        <div>
                            <table>
                                <tbody>
                                    <tr>
                                        <td><p>Pembayaran Melalui rekening Bank:</p></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="margin-left: 4%;margin-right: 4%; margin-top: -3px;line-height: 15px;">
                            <table width="100%">
                                <tbody>
                                    <tr>
                                        <td colspan="3"><b><?= $report_data->nama_bank;?></b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><?= $report_data->alamat1;?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><i><?= $report_data->alamat2;?></i></td>
                                    </tr>
                                    <tr>
                                        <td width="18%">Rekening <?= $report_data->jenis_rekening;?></td>
                                        <td width="5%">:</td>
                                        <td><?= $report_data->no_rekening;?></td>
                                    </tr>
                                    <tr>
                                        <td width="18%">Atas Nama</td>
                                        <td width="5%">:</td>
                                        <td>PT. TELKOM SATELIT INDONESIA</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div style="margin-top: 10px;">
                            <table>
                                <tbody>
                                    <tr>
                                        <td><p> Demikian kami sampaikan, atas perhatian dan kerjasamanya diucapkan terima kasih.</p></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div style="margin-top: 10px;">
                            <table width="95%">
                                <tbody>
                                    <tr>
                                        <td width="61%"></td>
                                        <td>Hormat Kami.</td>
                                    </tr>
                                    <tr>
                                        <td width="61%"></td>
                                        <td>PT. TELKOM SATELIT INDONESIA</td>
                                    </tr>
                                    <tr>
                                        <td width="61%"></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="61%"></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="61%"></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="61%"></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="61%"></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="61%"></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="61%"></td>
                                        <td style="border-bottom: 1px solid"><b>Erma Susilowati</b></td>
                                    </tr>
                                    <tr>
                                        <td width="61%"></td>
                                        <td>AVP Billing Management</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
            
                    </div>
                </div>
            </div>
        </div>
        
        <div class="page-break">
        <div class="page"> 
            <div id="templatemo_container_wrapper">
                <div id="templatemo_container">
                    <!-- <div id="" style="margin-top: 100px">
                        <table width="100%" border="0">
                            <tr>
                                <td width="250px" align="center" style="vertical-align:middle;"></td>
                                <td width="auto"></td>
                                <td width="300px" height="60" align="right" style="vertical-align:middle;font-size: 11px;color:#000000">
                                   <img src="<?= base_url('/assets/img/logo_telkomsat.png');?>" style="text-align: right; margin-right: 5px; width: 30%">
                                    
                                </td>
                            </tr>   
                            <tr><td colspan="3" style="border-top: 3px; padding-top: 10px;"></td></tr>
                        </table>
                    </div> -->
                    <div class="text_area" >
                    
                        <div style="text-align: center;"><center><h1><b>OFFICIAL RECEIPT</b></h1></center></div>
                        <div style="text-align: center; margin-top: -5px; font-size: 20px"><center>(Kwitansi)</center></div>
                        <div style="text-align: center; margin-top: 3px; font-size: 10px;">NPWP &nbsp;: 01.061.120.0-093.000</div>
                        <div style="text-align: center; margin-top: -3px; font-size: 10px">NPPKP : 01.061.120.0-093.000</div>
                        <div style="text-align: center; margin-top: -3px; font-size: 10px">Tgl. Pengukuhan : 17 Mei 2018</div>
                            <table width="100%" style="font-weight: bold; font-size: 11px">
                            <tr>
                                <td width="20%">No Kwitansi:</td>
                                <td width="30%"><?= $report_data->no_invoice;?></td>
                                <td>&nbsp;</td>
                                <td>Tanggal Jatuh Tempo: </td>
                                <td><?= tanggal($report_data->tgl_jthtempo);?></td>
                            </tr>
                        </table>
                    </div>
                    <div style="border-bottom: 1.5px solid black;"></div>
                    <div style="border-top: 1.5px solid black; margin-top: 2px;"></div>
                    <div style="margin-top: 10px">
                            <table width="100%" border="0">
                                <tbody>
                                    <tr>
                                        <td width="28%" valign="top">Terima dari</td>
                                        <td width="5%" valign="top">:</td>
                                        <td valign="top" style="line-height: 20px">&nbsp;<?= $report_data->nama_pelanggan;?> <br> &nbsp;<?= $report_data->alamat_invoice;?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="28%">NPWP</td>
                                        <td width="5%">:</td>
                                        <td style="line-height: 20px">&nbsp;<?= $report_data->npwp;?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="28%">Untuk Pembayaran</td>
                                        <td width="5%">:</td>
                                        <td>&nbsp;Rincian Biaya</td>
                                    </tr>
                                    <tr>
                                        <td width="28%"></td>
                                        <td width="5%"></td>
                                        <td>
                                            <?php
                                                $nominal = $report_data->nominal;
                                                $ppn = ceil((10 / 100) * $report_data->nominal);
                                                $total = $nominal + $ppn;
                                            ?>
                                            <table width="100%" > 
                                                <tbody>
                                                    <tr>
                                                        <td width="30%">No Invoice: </td>
                                                        <td><?= $report_data->no_invoice;?></td>
                                                        <td width="5px">Rp.</td>
                                                        <td class="text-right">&nbsp;&nbsp; <?=number_format($nominal, 0, ",", ".");?></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">PPN 10%</td>
                                                        <td width="40%">&nbsp;</td>
                                                        <td width="5px">Rp.</td>
                                                        <td class="text-right">&nbsp;&nbsp; <?=number_format($ppn, 0, ",", ".");?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="border-top: 1px solid;border-bottom: 1px solid;">
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%"  style="font-weight: bold">Total</td>
                                                        <td></td>
                                                        <td width="5px">Rp.</td>
                                                        <td class="text-right" style="font-weight: bold">&nbsp;&nbsp; <?=number_format($total, 0, ",", ".");?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="28%" valign="top">Terbilang</td>
                                        <td width="3%" valign="top">:</td>
                                        <td valign="top" style="font-style: italic" ><?= terbilang($total);?></td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                    </div>
                    <div style="margin-top: 20px;">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="61%"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td width="61%"></td>
                                    <td>Depok, &nbsp;&nbsp;&nbsp; <?php echo tanggal($report_data->tgl_invoice);?></td>
                                </tr>
                                <tr>
                                    <td width="61%"></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="61%"></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="61%"></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="61%"></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="61%"></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="61%"></td>
                                    <td>&nbsp;</td>
                                </tr>
                                
                                <tr>
                                    <td width="61%"></td>
                                    <td style=""><b><u>Erma Susilowati</u></b></td>
                                </tr>
                                <tr>
                                    <td width="61%"></td>
                                    <td>AVP Billing Management</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div style="margin-top: 20px">
                        <table width="40%">
                            <tbody>
                                <tr>
                                    <td><p>TOTAL</p></td>
                                </tr>
                                <tr style="background-color: rgb(218, 218, 228) color: black; font-weight: bold;  ">
                                    <td style="height: 35px; font-size: 15px; margin-left: 10px"><div><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rp. &nbsp;&nbsp;&nbsp;&nbsp; <?=number_format($total, 0, ",", ".");?></b></div></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div style="margin-top: 10px">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="font-size: 8px; font-weight: bold"><p>Please transfer the payment to </p></td>
                                </tr>
                                <tr>
                                    <td style="font-style: italic;"><p>Pembayaran ditransfer ke</p></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div style="margin-right: 4%; margin-top: -3px;line-height: 15px;">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td colspan="3"><b><?= $report_data->nama_bank;?> <?= $report_data->alamat1;?></b></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><i><?= $report_data->alamat2;?></i></td>
                                </tr>
                                <tr>
                                    <td width="18%">Rekening <?= $report_data->jenis_rekening;?></td>
                                    <td width="5%">:</td>
                                    <td><?= $report_data->no_rekening;?></td>
                                </tr>
                                <tr>
                                    <td width="18%">Atas Nama</td>
                                    <td width="5%">:</td>
                                    <td>PT. TELKOM SATELIT INDONESIA</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div style="margin-top: 10px">
                        <table>
                            <tbody>
                                <tr>
                                    <td style="font-size: 8px; font-weight: bold"><p>Please mention receipt number on payment/transfer</p></td>
                                </tr>
                                <tr>
                                    <td style="font-size: 8px;font-style: italic;"><p>Mohon cantumkan nomor kwitansi pada bukti pembayaran/transfer </p></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
        </div>

        <div class="page"> 
            <div id="templatemo_container_wrapper">
                <div id="templatemo_container">
                   <!--  <div id="" style="margin-top: 100px">
                        <table width="100%" border="0">
                            <tr>
                                <td width="250px" align="center" style="vertical-align:middle;"></td>
                                <td width="auto"></td>
                                <td width="300px" height="60" align="right" style="vertical-align:middle;font-size: 11px;color:#000000">
                                   <img src="<?= base_url('/assets/img/logo_telkomsat.png');?>" style="text-align: right; margin-right: 5px; width: 30%">
                                    
                                </td>
                            </tr>   
                            <tr><td colspan="3" style="border-top: 3px; padding-top: 10px;"></td></tr>
                        </table>
                    </div> -->

                    <div class="text_area">
                        <div style="text-align: center; font-family: 'Arial'"><center><h1><b>INVOICE</b></h1></center></div>
                        <div style="text-align: center; margin-top: -15px"><center><h3>BIAYA BERLANGGGANAN</h3></center></div>
                            <table class="table-print" width="100%" >
                                <thead>
                                <tr> 
                                    <th width="20%" style="font-family: 'Times New Roman';background-color: rgb(218, 218, 228)">NOMOR KWITANSI</th>
                                    <th width="15%" class="text-center" style="font-family: 'Times New Roman'; background-color: rgb(218, 218, 228)">TGL. INVOICE</th>
                                    <th style="font-family: 'Times New Roman'; background-color: rgb(218, 218, 228)">JATUH TEMPO</th>
                                    <th style="font-family: 'Times New Roman'; background-color: rgb(218, 218, 228)">NOMOR KONTRAK</th>
                                </tr>
                                </thead>
                                <tbody>
                               
                                <tr> 
                                    <td class="text-center"><?= $report_data->no_invoice;?></td>
                                    <td class="text-center"><?= date('d-M-Y',strtotime($report_data->tgl_invoice))?></td>
                                    <td class="text-center"><?= date('d-M-Y',strtotime($report_data->tgl_jthtempo))?></td>
                                    <td class="text-center"><?= $report_data->no_wo;?></td>
                                   
                                </tr>
                              
                                <tr> 
                                    <td style="text-align: left; background-color: rgb(218, 218, 228)" width="20%">&nbsp;Nama Badan</td>
                                    <td colspan="3" class="text-left">&nbsp;<?= strtoupper($report_data->nama_pelanggan);?></td>
                                </tr>
                                <tr> 
                                    <td style="text-align: left; background-color: rgb(218, 218, 228)" width="20%">&nbsp;Alamat</td>
                                    <td colspan="3" class="text-left">&nbsp;<?= $report_data->alamat_invoice;?></td>
                                </tr>
                                <tr> 
                                    <td style="text-align: left; background-color: rgb(218, 218, 228)" width="20%">&nbsp;NPWP</td>
                                    <td colspan="3" class="text-left">&nbsp;<?=wordwrap($report_data->npwp, 1, "\n", true) ;?></td>
                                </tr>
                                <tr> 
                                    <td style="text-align: left; background-color: rgb(218, 218, 228)" width="20%">&nbsp;Total Tagihan</td>
                                    <td colspan="3" class="text-left">&nbsp;Rp. &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=number_format($total, 0, ",", ".");?></td>
                                </tr>
                                <tr> 
                                    <td style="text-align: left; background-color: rgb(218, 218, 228)" width="20%" height="30px" valign="top">&nbsp;Terbilang</td>
                                    <td colspan="3" class="text-left" style="font-style: italic;">&nbsp;<?= terbilang($total)?></td>
                                </tr>

                                </tbody>
                            </table>
                        
                             <div style="margin-top: 10px">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td><!-- <p>Rincian Biaya </p> --></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div style="font-size: 15px; text-align: left; margin-left: 3px; margin-top: 30px; margin-bottom: 15px"><?=$report_data->nama_project;?></div>
                            
                            <div>

                                <?php 
                                $total_otc = 0;
                                $total_mtc = 0;
                                $total_cpe = 0;
                                ?>
                              
                                
                                <div style="border-bottom:1px solid #ccc;font-size: 13px; text-align: left; margin-bottom:5px; margin-left: 3px;  font-weight: bold"> ONE TIME CHARGE</div>
                                <?php foreach ($tagihan_otc as $key => $value) {?>
                                    <div style="font-size: 13px; text-align: left; margin-bottom:5px; margin-left: 3px;  font-weight: bold"> <?=$key;?></div>
                                    <table class="table-biaya" width="100%" style="font-weight:normal;font-family:">
                                        <thead>
                                            <tr  style="background-color: rgb(218, 218, 228)"> 
                                                <th>No.</th>
                                                <th>Deskripsi</th>
                                                <th>Speed</th>
                                                <th>Periode</th>
                                                <th>Biaya</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                       
                                        <?php 
                                            $no=0;
                                            $total_otc=0;
                                            foreach ($value as $key2) {
                                           
                                                $no++;
                                                $jml_biaya_otc = $key2['jumlah']*(int)$key2['jumlah_biaya'];
                                                $total_otc+=$jml_biaya_otc;
                                            
                                        ?>

                                            <tr> 
                                                <td class="text-center" width="5%" style="height: 30px"><?= $no;?></td>
                                                <td class="text-left" style="height: 30px" width="56%"><?= $key2['node_deskripsi'];?></td>
                                                <td class="text-center" width="10%" style="height: 30px"><?= $key2['speed'];?></td>
                                                <td class="text-center" width="10%" style="height: 30px">-</td>
                                                <td class="text-right" width="19%" style="height: 30px">Rp. <?= number_format($key2['jumlah_biaya'],'0','.','.');?></td>
                                            </tr>
                                        <?php
                                            
                                            }
                                        ?>                               
                                        

                                        </tbody>
                                        <!-- <tfoot>
                                            <tr>
                                                <td colspan="4" class="text-center" style="font-weight: bold">Total</td>
                                                <td class="text-right">Rp. <?= number_format($total_otc,'0','.','.');?></td>
                                            </tr>
                                        </tfoot> -->
                                    </table><br>

                                <?php } ?> <!-- end foreach-->

                                <div style="border-bottom:1px solid #ccc;font-size: 13px; text-align: left; margin-bottom:5px; margin-left: 3px;  font-weight: bold"> MONTHLY RECURRING</div>
                                <?php foreach ($tagihan_mrc as $key => $value) {?>
                                    <div style="font-size: 13px; text-align: left; margin-bottom:5px; margin-left: 3px;  font-weight: bold"> <?=$key;?></div>
                                    <table class="table-biaya" width="100%" style="font-weight:normal">
                                        <thead>
                                            <tr  style="background-color: rgb(218, 218, 228)"> 
                                                <th>No.</th>
                                                <th>Deskripsi</th>
                                                <th>Speed</th>
                                                <th colspan="3">Periode</th>
                                                <th>Biaya</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                       
                                        <?php 
                                            $no=0;
                                            $total=0;
                                            $total_mtc = 0;
                                            foreach ($value as $key2) {
                                            
                                                $no++;
                                                $jml_biaya_mtc = $key2['jumlah']*(int)$key2['jumlah_biaya'];
                                            //$total = $total+$jml_biaya;
                                                $total_mtc+=$jml_biaya_mtc;
                                            ?>

                                            <?php //for ($i=0; $i <3 ; $i++) { ?>
                                                <tr> 
                                                    <td class="text-center" width="5%" style="height: 30px"><?= $no;?></td>
                                                    <td class="text-left" style="height: 30px" width="41%"><?=$key2['node_deskripsi'];?></td>
                                                    <td class="text-center" width="10%" style="height: 30px"><?= $key2['speed'];?></td>
                                                    <td class="text-center" width="11%" style="height: 30px"><?= tanggal($key2['start_date'], 'd-m-y');?></td>
                                                    <td class="text-center" width="3%" style="height: 30px">-</td>
                                                    <td class="text-center" width="11%" style="height: 30px"><?= tanggal($key2['end_date'], 'd-m-y');?></td>
                                                    <td class="text-right" width="19%" style="height: 30px">Rp. <?= number_format($key2['jumlah_biaya'],'0','.','.');?></td>
                                                   
                                                </tr>
                                            <?php
                                            }
                                        ?>               
                                        

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="6" class="text-center" style="font-weight: bold">Total</td>
                                                <td class="text-right">Rp. <?= number_format($total_mtc,'0','.','.');?></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <br>

                                <?php } ?> <!-- end foreach-->

                                <div style="border-bottom:1px solid #ccc;font-size: 13px; text-align: left; margin-bottom:5px; margin-left: 3px;  font-weight: bold"> CPE</div>
                                <?php foreach ($tagihan_cpe as $key => $value) {?>
                                    <div style="font-size: 13px; text-align: left; margin-bottom:5px; margin-left: 3px;  font-weight: bold"> <?=$key;?></div>
                                    <table class="table-biaya" width="100%" style="font-weight:normal">
                                        <thead>
                                            <tr  style="background-color: rgb(218, 218, 228)"> 
                                                <th>No.</th>
                                                <th>Deskripsi</th>
                                                <th>Speed</th>
                                                <th colspan="3">Periode</th>
                                                <th>Biaya</th>
                                            
                                            </tr>
                                        </thead>
                                        <tbody>
                                       
                                        <?php 
                                            $no=0;
                                            $total=0;
                                            $total_mtc = 0;
                                            foreach ($value as $key2) {
                                            
                                                $no++;
                                                $jml_biaya_cpe = $key2['jumlah']*(int)$key2['jumlah_biaya'];
                                                $total_cpe+=$jml_biaya_cpe;
                                            ?>

                                            <?php //for ($i=0; $i <3 ; $i++) { ?>
                                                <tr> 
                                                    <td class="text-center" width="5%" style="height: 30px"><?= $no;?></td>
                                                    <td class="text-left" style="height: 30px" width="41%"><?=$key2['node_deskripsi'];?></td>
                                                    <td class="text-center" width="10%" style="height: 30px"><?= $key2['speed'];?></td>
                                                    <td class="text-center" width="11%" style="height: 30px"><?= tanggal($key2['start_date'], 'd-m-y');?></td>
                                                    <td class="text-center" width="3%" style="height: 30px">-</td>
                                                    <td class="text-center" width="11%" style="height: 30px"><?= tanggal($key2['end_date'], 'd-m-y');?></td>
                                                    <td class="text-right" width="19%" style="height: 30px">Rp. <?= number_format($key2['jumlah_biaya'],'0','.','.');?></td>
                                                    
                                                </tr>
                                            <?php
                                            }
                                        ?>               
                                        

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="6" class="text-center" style="font-weight: bold">Total</td>
                                                <td class="text-right">Rp. <?= number_format($total_cpe,'0','.','.');?></td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <br>

                                <?php } ?> <!-- end foreach-->


                                <?php 
                                $hitung_nilai_penambahan=0;
                                $hitung_nilai_pengurangan=0;
                                if(count($detail_inv_minus_data)>0){?>
                                    <div style="font-size: 13px; text-align: left; margin-bottom:5px; margin-left: 3px; font-weight: bold">ADJUSMENT</div>
                                <table class="table-biaya" width="100%" style="font-weight:normal">
                                    <thead>
                                        <tr style="background-color: rgb(218, 218, 228)"> 
                                            <th>No.</th>
                                            <th>Deskripsi</th>
                                            <!-- <th>Note</th> -->
                                            <th>Speed</th>
                                            <th colspan="3">Periode</th>
                                            <th width="19%">Biaya</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                   
                                    <?php 
                                        $no=0;
                                        $total=0;
                                        $hitung_nilai_penambahan = 0;
                                        $hitung_nilai_pengurangan = 0 ;
                                        foreach ($detail_inv_minus_data as $key) {
                                        $no++;
                                        $jml_biaya_minus = $key->jumlah*$key->biaya;
                                        if($key->type=="penambahan"){
                                            $jml_biaya = $key->jumlah*$key->biaya;
                                            $hitung_nilai_penambahan+=$jml_biaya;
                                        }else{
                                            $jml_biaya = $key->jumlah*$key->biaya;
                                            $hitung_nilai_pengurangan+=$jml_biaya;
                                        }
                                        //$total = $total+$jml_biaya;
                                    ?>

                                    <?php //for ($i=0; $i <3 ; $i++) { ?>
                                        <tr> 
                                            <td class="text-center" width="5%" style="height: 30px"><?= $no;?></td>
                                            <td class="text-left" width="41%" style="height: 30px"><?=$key->deskripsi;?></td>
                                            <!-- <td class="text-center" style="height: 30px" width="5%"><?=$key->type=='penambahan'?'+':'-';?></td> -->
                                            <td class="text-center" width="10%" style="height: 30px"><?= $key->speed;?></td>
                                            <td class="text-center" width="11%" style="height: 30px"><?= tanggal($key->tgl_awal, 'd-m-y');?></td>
                                            <td class="text-center" width="3%" style="height: 30px">-</td>
                                            <td class="text-right" width="11%" style="height: 30px"><?= tanggal($key->tgl_akhir, 'd-m-y');?></td>
                                            <td class="text-right" width="19%" style="height: 30px">Rp. <?= number_format($key->biaya,'0','.','.');?></td>
                                            
                                        </tr>
                                    <?php
                                        }
                                    ?>                                
                                    
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="6" class="text-center" style="font-weight: bold">Total</td>
                                            <td class="text-right" width="17%">Rp. <?= number_format(($hitung_nilai_penambahan - $hitung_nilai_pengurangan),'0','.','.');?></td>
                                        </tr>
                                    </tfoot>
                                </table>
                                <br>
                                <?php } ?>


                                <?php
                                   $nominal = $report_data->nominal;
                                   $ppn = ceil((10 / 100) * $report_data->nominal);
                                   $total = $nominal + $ppn;


                                ?>
                              
                                <!--deskripsi, qty, speed, periode, biaya, jml biya (qty*biaya)-->
                                <table class="table-biaya"  style="border-collapse: collapse; text-align: center" width="100%" >
                                   
                                    <tr>
                                        <td colspan="8" width="81%" style=" font-weight: bold; background-color: rgb(218, 218, 228)">Total Biaya</td>
                                        <td   width="19%" style="text-align:right;  font-weight: bold ;background-color: rgb(218, 218, 228)">Rp. <?= number_format($nominal,'0','.','.');?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style=" font-weight: bold;background-color: rgb(218, 218, 228)">PPN 10%</td>
                                        <td style="text-align:right; font-weight: bold;background-color: rgb(218, 218, 228)">Rp. <?= number_format($ppn,'0','.','.');?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style=" font-weight: bold;background-color: rgb(218, 218, 228)">Total Biaya Pemakaian Keseluruhan</td>
                                        <td style="text-align:right; font-weight: bold;background-color: rgb(218, 218, 228)">Rp. <?= number_format($total,'0','.','.');?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
        </div>

        
    </body>
</html>
