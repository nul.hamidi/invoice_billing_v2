<?php 
function angka($int){
    $hasil = number_format((float)$int, 2, '.', ','); 
    return $hasil;
}
function tanggal($date){
    $d = explode("-", $date);
    if($d[1]=='01'){
        $bulan = "Januari";
    }else if($d[1]=='02'){
        $bulan = "Februari";
    }else if($d[1]=='03'){
        $bulan = "Maret";
    }else if($d[1]=='04'){
        $bulan = "April";
    }else if($d[1]=='05'){
        $bulan = "Mei";
    }else if($d[1]=='06'){
        $bulan = "Juni";
    }else if($d[1]=='07'){
        $bulan = "Juli";
    }else if($d[1]=='08'){
        $bulan = "Agustus";
    }else if($d[1]=='09'){
        $bulan = "September";
    }else if($d[1]=='10'){
        $bulan = "Oktober";
    }else if($d[1]=='11'){
        $bulan = "November";
    }else if($d[1]=='12'){
        $bulan = "Desember";
    }else{
        $bulan = "";
    }
    $date_fix = intval($d[2])." ".$bulan." ".$d[0];
    return $date_fix;
}
function setTanggalNew($date){
    $d = explode("-", $date);
    $ret = $d[2].'/'.$d[1].'/'.$d[0];
    return $ret;
}

function Persentase($BeanCount,$KategoriUkuranBiji){
    $persen=0;
    if($BeanCount>0){
        $persen = round( ((float)$BeanCount/ (float)$KategoriUkuranBiji)*100 ,2);
    }
    
    return $persen;
}
?>
<!DOCTYPE  html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="utf-8">
        <title>Surat Jalan</title>

        <style type="text/css">

            @media all {
                .page-break	{ display: none; }
            }

            @media print {
                @page { margin: 0cm; padding:0cm; }
                .page-break  { display: block; page-break-before: always; }
                .page-break-after { display: block; page-break-after: always; }
                .page-break  {page-break-after: always;}
                .page {
                    margin: 0;
                    border: none;
                    border-radius: none;
                    width: initial;
                    min-height: initial;
                    box-shadow: none;
                    background: initial;
                    page-break-after: always;
                }
                .page_landscape {
                    margin: 0;
                    border: initial;
                    border-radius: initial;
                    width: initial;
                    min-height: initial;
                    box-shadow: initial;
                    background: initial;
                    page-break-after: always;
                }
                
                thead {display: table-header-group;}
                
                .page {
                    -webkit-box-shadow: none;
                    -moz-box-shadow:    none;
                    box-shadow:         none; 
                }

            }

            body {
                margin:0;
                padding:0;
                line-height: 1.5em;
                font-family: "Trebuchet MS", Verdana, Helvetica, Arial;
                font-size: 14px;
                color: #000000;
                background-color: #ffffff;
            }
            a:link, a:visited { color: #0066CC; text-decoration: none} 
            a:active, a:hover { color: #008800; text-decoration: underline}

            #templatemo_container_wrapper {
                /*background: url(images/templatemo_side_bg.gif) repeat-x;*/
                background: #ffffff;
            }
            #templatemo_container {
                margin: 0px auto;
                /*background: url(images/templatemo_content_bg.gif);*/
                background: #FFFFFF;
            }
            #templatemo_top {
                clear: left;
                height: 25px;	/* 'padding-top' + 'height' must be equal to the 'background image height' */
                padding-top: 42px;
                padding-left: 30px;
                background: url(images/templatemo_top_bg.gif) no-repeat bottom;
            }
            #templatemo_header {
                clear: left;
                padding-top: 2px;
                height: 60px;
                text-align: center;
                font-weight: bold;
                font-size: 24px;
                color: #000000;
                /*background: url(images/templatemo_header_bg.gif) no-repeat;*/
            }
            #inner_header {
                height: 30px;
                background: url(images/templatemo_header.jpg) no-repeat center center;
            }
            #templatemo_left_column {
                clear: left;
                float: left;
                width: 100%; 
            }
            #templatemo_right_column {
                float: right;
                width: 216px;
                padding-right: 15px;
            }
            #templatemo_footer {
                clear: both;
                /*padding-top: 18px;*/
                height: 15px;
                text-align: center;
                font-size: 11px;
                /*background: url(images/templatemo_footer_bg.gif) no-repeat;*/
                color: #ffffff;
            }
            #templatemo_footer a {
                color: #666666;
            }
            #templatemo_site_title {
                padding-top: 65px;
                font-weight: bold;
                font-size: 28px;
                color: #000000;
            }
            #templatemo_site_slogan {
                padding-top: 14px;
                font-weight: bold;
                font-size: 13px;
                color: #AAFFFF;
            }
            .templatemo_spacer {
                clear: left;
                height: 18px;
            }
            .templatemo_pic {
                float: left;
                margin-right: 10px;
                margin-bottom: 10px;
                border: 1px solid #000000;
            }
            .section_box {
                margin: 10px;
                padding: 10px;
                border: 1px dashed #ffffff;
                background: #FFFFFF;
                border: 1px solid #000000;
            }
            .section_box2 {
                clear: left;
                margin-top: 10px;
                background: #ffffff;
                color: #000000;
                font-weight: bold;
                border: 1px solid #000000;
            }
            .section_box3 {
                clear: left;
                margin-top: 10px;
                background: #ffffff;
                color: #000000;
                border: 1px;
            }
            .text_area {
                padding: 10px;
            }
            .publish_date {
                clear: both;
                margin-top: 10px;
                color: #999999;
                font-size: 11px;
                font-weight: bold;
            }
            .title {
                padding-bottom: 12px;
                font-size: 18px;
                font-weight: bold;
                color: #000000;
            }
            .subtitle {
                padding-bottom: 6px;
                font-size: 14px;
                font-weight: bold;
                color: #666666;
            }
            .post_title_main {
                padding: 6px;
                padding-left: 10px;
                background: #cccccc;
                font-size: 14px;
                font-weight: bold;
                color: #000000;
                border-bottom: 1px solid #000000;
                text-align:left;
            }
            .post_title {
                padding: 6px;
                padding-left: 10px;
                background: #cccccc;
                font-size: 14px;
                font-weight: bold;
                color: #000000;
                border-bottom: 1px solid #000000;
                text-align:left;
            }
            .templatemo_menu {
                list-style-type: none;
                margin: 10px;
                margin-top: 0px;
                padding: 0px;
                width: 195px;
            }
            .templatemo_menu li a{
                background: #F4F4F4 url(images/button_default.gif) no-repeat;
                font-size: 13px;
                font-weight: bold;
                color: #000000;
                display: block;
                width: auto;
                margin-bottom: 2px;
                padding: 5px;
                padding-left: 12px;
                text-decoration: none;
            }
            * html .templatemo_menu li a{ 
                width: 190px;
            }
            .templatemo_menu li a:visited, .templatemo_menu li a:active{
                color: #000000;
            }
            .templatemo_menu li a:hover{
                background: #EEEEEE url(images/button_active.gif) no-repeat;
                color: #FF3333;
            }#templatemo_container_wrapper #templatemo_container #templatemo_left_column .text_area .section_box2 .post_title_main strong td {
                color: #000000;
            }
            #templatemo_container_wrapper #templatemo_container #templatemo_left_column .text_area .section_box2 .post_title_main {
                color: #000000;
            }
            div {
                color: #000000;
            }
            .page {
                width: 21cm;
                height:26.7cm;
                padding: 2cm;
                margin: 1cm auto;
                border: 1px #D3D3D3 solid;
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
                
            }
            .page_mini {
                width: 8.5cm;
                height:10cm;
                padding: 0cm;
                margin: 0.5cm auto;
                border: 1px #D3D3D3 solid;
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }
            .page_landscape {
                width: 28cm;
                height:21cm;
                padding: 2cm;
                margin: 1cm auto;
                border: 1px #D3D3D3 solid;
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }
            .table-print {
                font-size: 10pt;
                font-family: verdana;
                font-weight: normal;
                padding: 0px;
                margin: 0px;
                border-top: 1.5px solid #333333;
                border-left: 1.5px solid #333333;
                border-collapse: collapse;
            }

            .table-print th {
                text-align: center;
                font-weight: bold;
                border-right: 1.5px solid #333333;
                border-bottom: 1.5px solid #333333;
                padding: 5px;
                margin:0px;
            }

            .table-print td {
                
                border-right: 1.5px solid #333333;
                border-bottom: 1.5px solid #333333;
                padding: 1px;
                margin:0px;
                font-weight: normal;
            }
            .text-center{
                text-align:center;
            }
            .text-left{
                text-align:left;
            }
            .text-right{
                text-align:right;
            }

           .transform{
            -ms-transform: rotate(180deg); /* IE 9 */
            -webkit-transform: rotate(180deg); /* Safari 3-8 */
            transform: rotate(180deg);
           }


        </style>
    </head>
    <body>
        
        <div class="page">
            <div id="templatemo_container_wrapper">
                <div id="templatemo_container" style="margin-top: -50px;">
                    <div id="">
                        <table width="100%" border="0">
                            <tr>
                                <td width="250px" align="center" style="vertical-align:middle;">
                                    <!-- <?php 
                                        if($batch->LogoCompany!=''){ echo '<img src="'.base_url().$batch->LogoCompany.'" width="100" height="100">';}
                                        else{echo '<img src="'.base_url('images/kudeng1.png').'" width="250" height="100">';}
                                    ?> -->
                                </td>
                                <td width="auto"></td>
                                <td width="300px" height="60"  align="left" style="vertical-align:middle;font-size: 11px;color:#000000">
                                   
                                    <table border='0' width="100%">
                                        <tr valign="top">
                                            <td ><b><strong><span >Office</span></strong></b></td>
                                            <td>:</td><td> Jl.Biruen-Takengon Km.6 Desa Juli Mee Teungoh Kec.juli Kabupaten Biruen Provinsi Aceh 24250</td>
                                        </tr>
                                        <tr>
                                            <td><b><strong>Phone</strong></b></td>
                                            <td>:</td><td>0811 6842522</td>
                                        </tr>
                                        <tr>
                                            <td><b><strong>Email</strong></b></td>
                                            <td>:</td><td>  kudengosugata@gmail.com</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>   
                            <tr><td colspan="3" style="border-top: 3px solid; padding-top: 15px;"></td></tr>
                        </table>
                    </div>
                    <center><h1><b><u>Surat Jalan</u></b></h1></center>
                    

                    <div class="text_area">
                        Biruen, <?php echo tanggal(date('Y-m-d'));?> <br>
                        Kepada Yth,<br>
                        <strong><?php echo $batch->Destination;?></strong><br>
                        Di<br>
                        Tempat<br><br><br>

                        Dengan Hormat,<br>
                        <p align="justify">Bersama Surat ini kami mengirimkan barang berupa biji kakao fermentasi ke <?php echo $batch->Destination;?>
                        dengan menggunakan Jasa Ekspedisi umum yaitu <?php echo $batch->DestTransport;?> dengan Rincian Sebagai Berikut :</p>
                        <br>
                        <table class="table-print" width="100%" style="font-weight:normal">
                            <thead>
                            <tr> 
                                <th>No.</th>
                                <th>Nama Barang</th>
                                <!-- <th>Satuan</th> -->
                                <th>Jumlah</th>
                                <th>Berat (Kg)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr> 
                                <td class="text-center">1</td>
                                <td class="text-left"><?php echo $batch->BeanTypeName;?></td>
                                <!-- <td class="text-center">Bag</td> -->
                                <td class="text-center"><?php echo $batch->DestJumlahKarung;?></td>
                                <td class="text-center"><?php echo $batch->DestWeight;?> Kg</td>
                            </tr>

                            <tr> 
                                <th colspan="2"><b>Total</b></th>
                                <!-- <td></td> -->
                                <td class="text-center"><?php echo $batch->DestJumlahKarung;?></td>
                                <td class="text-center"><?php echo $batch->DestWeight;?> Kg</td>
                            </tr>

                            </tbody>

                        </table>
                        <br><br><br>
                        Kudeungo Sugata
                        <br><br><br><br><br>



                        <u><b>Mirza</b></u>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- packing list-->
        <div class="page"> 
            <div id="templatemo_container_wrapper">
                <div id="templatemo_container" style="margin-top: -50px;">
                    <div id="">
                        <table width="100%" border="0">
                            <tr>
                                <td width="250px" align="center" style="vertical-align:middle;">
                                    <?php 
                                        if($batch->LogoCompany!=''){ echo '<img src="'.base_url().$batch->LogoCompany.'" width="100" height="100">';}
                                        else{echo '<img src="'.base_url('images/kudeng1.png').'" width="250" height="100">';}
                                    ?>
                                </td>
                                <td width="auto"></td>
                                <td width="300px height=""  align="left" style="vertical-align:middle;font-size: 11px;">
                                    
                                    <table border='0' width="100%">
                                        <tr valign="top">
                                            <th ><b><strong><span >Office</span></strong></b></th>
                                            <td>:</td><td> Jl.Biruen-Takengon Km.6 Desa Juli Mee Teungoh Kec.juli Kabupaten Biruen Provinsi Aceh 24250</td>
                                        </tr>
                                        <tr>
                                            <th><b><strong>Phone</strong></b></th>
                                            <td>:</td><td> 0811 6842522</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>:</td><td>  kudengosugata@gmail.com</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>   
                            <tr><td colspan="3" style="border-top: 3px solid; padding-top: 15px;"></td></tr>
                        </table>
                    </div>
                    <center><h1><b><u>Packing List</u></b></h1></center>
                    

                    <div class="text_area">
                        <table>
                            <tr>
                                <td width="130px">Packing List No.</td><td>:</td>
                                <td> <!-- KDSG-PL-19/IIIV-07 --> <?php echo $batch->PackingListNumber;?></td>
                                
                            </tr>
                            <tr>
                                <td>Tanggal Pengiriman</td><td>:</td>
                                <td><?php echo tanggal($batch->DeliveryDate);?></td>
                            </tr>
                            <tr>
                                <td>Customer</td><td>:</td>
                                <td><b><?php echo$batch->Destination;?></b></td>
                            </tr>
                        </table>
                        <br><br>
                        <table width="100%">
                            <tr>
                                <td width="130px">Dari</td><td valign="top">:</td>
                                <td><b>Kudeungo Sugata</b></td><td width="50px"></td>
                                <td>Kepada</td><td valign="top">:</td>
                                <td><b><?php echo$batch->Destination;?></b></td>
                            </tr>
                            <tr>
                                <td valign="top">Alamat</td><td valign="top">:</td>
                                <td>Jl. Biruen - Tekengon Desa Juli Meunasah Mee Kec.Juli Kab. Biruen Provinsi Aceh</td><td></td>
                                <td valign="top">Alamat</td><td valign="top">:</td>
                                <td><?php echo $batch->DestinationAddress;?></td>
                            </tr>
                            <tr>
                                <td>Phone</td><td valign="top">:</td>
                                <td>0811 6842522 (CP.Mirza)</td><td></td>
                                <td>Phone</td><td valign="top">:</td>
                                <td><?php echo $batch->DestinationPhone!=""?$batch->DestinationPhone:$batch->DestDriverHp; echo" ( ".$batch->DestDriver." )";?></td>
                            </tr>
                        </table>
                        <br><br>
                        <table width="100%">
                            <tr>
                                <td width="130px">Metode Pengiriman</td><td valign="top">:</td>
                                <td>Pengangkutan Darat</td><td width="50px"></td>
                                <td>Ekspedisi</td><td valign="top">:</td>
                                <td><?php echo $batch->DestTransport;?></td>
                            </tr>
                            <tr>
                                <td valign="top">Estimasi Tiba</td><td valign="top">:</td>
                                <td>5 Hari</td><td></td>
                                <td valign="top">Plat Ekspedisi</td><td valign="top">:</td>
                                <td><?php echo $batch->DestNoPolisi;?></td>
                            </tr>
                            <tr>
                                <td>Phone</td><td valign="top">:</td>
                                <td>0811 6842522 (CP.Mirza)</td><td></td>
                                <td>Phone</td><td valign="top">:</td>
                                <td><?php echo $batch->DestDriverHp. "( ".$batch->DestDriver." )";?></td>
                            </tr>
                        </table>
                        
                        <br> <br>
                        <table class="table-print" width="100%" style="font-weight:normal">
                            <thead>
                            <tr> 
                                <th>No.</th>
                                <th>Nama Barang</th>
                                <th>Bag ID</th>
                                <th>Jumlah</th>
                                <th>Berat (Kg)</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $no=0;$totalPackage=0;$totalKg=0;
                            foreach ($trans as $tr) {
                                $no++;
                                $totalPackage =$totalPackage+$tr['FAQNumberPackage'];
                                $totalKg=$totalKg+$tr['FAQVolumeNetto'];
                            ?>
                            <tr> 
                                <td class="text-center"><?php echo$no;?></td>
                                <td class="text-left"><?php echo lang($tr['BeanTypeName']);?></td>
                                <td class="text-center"><?php echo $tr['FakturNumber'];?></td>
                                <td class="text-center"><?php echo $tr['FAQNumberPackage'];?></td>
                                <td class="text-center"><?php echo number_format($tr['FAQVolumeNetto'],2);?> Kg</td>
                            </tr>
                            <?php
                            }
                            ?>
                            <tr> 
                                <th colspan="3"><b>Total</b></th>
                                <td class="text-center"><?php echo$totalPackage;?></td>
                                <td class="text-center"><?php echo number_format($totalKg,2);?> Kg</td>
                            </tr>

                            </tbody>
                        </table>
                        <p>Demikian yang dapat kami sampaikan dan terima kasih atas kerjasamanya</p>
                        <br>
                        Kudeungo Sugata
                        <br><br><br><br><br>


                        <u><b>Mirza</b></u>

                    </div>
                </div>
            </div>
        </div>
        <div class="page-break"></div>

        <!-- invoice -->
        <div class="page"> 
            <div id="templatemo_container_wrapper">
                <div id="templatemo_container" style="margin-top: -50px;">
                    <div id="">
                        <table width="100%" border="0">
                            <tr>
                                <td width="250px" align="center" style="vertical-align:middle;">
                                    <?php 
                                        if($batch->LogoCompany!=''){ echo '<img src="'.base_url().$batch->LogoCompany.'" width="100" height="100">';}
                                        else{echo '<img src="'.base_url('images/kudeng1.png').'" width="250" height="100">';}
                                    ?>
                                </td>
                                <td width="auto"></td>
                                <td width="300px height=""  align="left" style="vertical-align:middle;font-size: 11px;">
                                    
                                    <table border='0' width="100%">
                                        <tr valign="top">
                                            <th ><b><strong><span >Office</span></strong></b></th>
                                            <td>:</td><td> Jl.Biruen-Takengon Km.6 Desa Juli Mee Teungoh Kec.juli Kabupaten Biruen Provinsi Aceh 24250</td>
                                        </tr>
                                        <tr>
                                            <th><b><strong>Phone</strong></b></th>
                                            <td>:</td><td> 0811 6842522</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>:</td><td>  kudengosugata@gmail.com</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>   
                            <tr><td colspan="3" style="border-top: 3px solid; padding-top: 15px;"></td></tr>
                        </table>
                    </div>
                    <center><h1><b><u>INVOICE</u></b></h1></center>
                    
                    <div class="text_area">
                        <table>
                            <tr>
                                <td width="130px">No. Invoice</td><td>:</td>
                                <td><?php echo $batch->InvoiceBatchNumber;?></td>
                                <!-- <td>KDSG-INV-19/IX-08</td> -->
                            </tr>
                            <tr>
                                <td>Tanggal</td><td>:</td>
                                <td><?php echo tanggal($batch->DeliveryDate);?></td>
                            </tr>
                            <tr>
                                <td>PO.NO</td><td>:</td>
                                <td><b><?php echo $batch->DestPO;?></b></td>
                            </tr>
                        </table>
                        <br><br>
                        
                        <table width="100%">
                            <tr>
                                <td width="130px">Dari</td><td valign="top">:</td>
                                <td><b>Kudeungo Sugata</b></td><td width="50px"></td>
                                <td>Kepada</td><td valign="top">:</td>
                                <td><b><?php echo$batch->Destination;?></b></td>
                            </tr>
                            <tr>
                                <td valign="top">Alamat</td><td valign="top">:</td>
                                <td>Jl. Biruen - Tekengon Desa Juli Meunasah Mee Kec.Juli Kab. Biruen Provinsi Aceh</td><td></td>
                                <td valign="top">Alamat</td><td valign="top">:</td>
                                <td><?php echo $batch->DestinationAddress;?></td>
                            </tr>
                            <tr>
                                <td>Phone</td><td valign="top">:</td>
                                <td>0811 6842522 (CP.Mirza)</td><td></td>
                                <td>Phone</td><td valign="top">:</td>
                                <td><?php echo $batch->DestinationPhone!=""?$batch->DestinationPhone:$batch->DestDriverHp; echo" ( ".$batch->DestDriver." )";?></td>
                            </tr>
                        </table>

                        <br> <br>
                        <table class="table-print" width="100%" style="font-weight:normal">
                            <thead>
                            <tr> 
                                <th>No.</th>
                                <th>Deskripsi</th>
                                <th>Jumlah Bag</th>
                                <th>Jumlah</th>
                                <th>Satuan</th>
                                <th>Harga Satuan</th>
                                <th>Total</th>
                            </tr>
                            </thead>
                            <tbody>
                           
                            <?php 
                            $no=1;
                            foreach ($DeskripsiInvoice as $key) {
                                $no++;
                                $total = $key->TotalPayment+$total;
                            ?>
                                <tr> 
                                <td class="text-center"><?php echo$no;?></td>
                                <td class="text-left"><?php echo lang($key->Deskripsi);?></td>
                                <td class="text-center"><?php echo $key->PackageNumber;?></td>
                                <td class="text-center"><?php echo $key->Weight;?></td>
                                <td class="text-center">Kg</td>
                                <td class="text-right"><?php echo number_format($key->Price,2);?></td>
                                <td class="text-right">
                                    <?php 
                                        echo number_format($key->TotalPayment,2);
                                    ?>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>

                            <tr> 
                                <td colspan="5" rowspan="2"><b>Terbilang : <i><?php echo $this->rupiah->terbilang($total);?> rupiah</i><b></td>
                                <td><b>Total</b></td>
                                <td class="text-right"><b>Rp. <?php echo number_format($total,2);?></b></td>
                            </tr>
                            <tr>
                                <td><b>Total Invoice</b></td>
                                <td class="text-right"><b>Rp. <?php echo number_format($total,2);?></b></td>
                            </tr>

                            </tbody>
                        </table>
                        <p>Pembayaran Tagihan ini agar di transfer ke rekening Perusahaan: </p>
                        <table>
                            <tr>
                                <td>Nama Bank</td>
                                <td> : <b><?php echo $batch->Bank;?></b></td>
                            </tr>
                            <tr>
                                <td>No. Rekening</td>
                                <td> : <b><?php echo $batch->AccountNumber;?></b></td>
                            </tr>
                            <tr>
                                <td>An. Nama</td>
                                <td> : <b><?php echo $batch->AccountName;?></b></td>
                            </tr>
                        </table>
                        <p>Demikian Invoice ini kami ajukan dan atas kerjasamanya kami ucapkan terima kasih.</p>
                        <br>
                        Kudeungo Sugata
                        <br><br><br><br><br>


                        <u><b>Mirza</b></u>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="page-break"></div>
               
        <div class="page_landscape">
            <div id="templatemo_container_wrapper">
                <div id="templatemo_container">

                    <center><h1>Keterlusuran Biji Kakao Fermentasi</h1></center>
                    <div class="text_area">
                        <table class="table-print" width="100%" >
                            <tr class="post_title_main">
                                <th width="5%">Provinsi</th>
                                <th>District</th>
                                <th >Subdistrict</th>
                                <th >Village</th>
                                <th >Fermentasi Date</th>
                                <th >Farmers</th>
                                <th >Wet Beans</th>
                                <th >Dry Beans</th>
                                <th >Fermentasi Officer</th>
                                <th >Buyer</th>
                            </tr>
                            <?php 
                           foreach ($Farmers as $farm) {
                            $totalDryBean = $farm->Netto_Dry+$totalDryBean;
                            ?>
                            <tr>
                                <td><?php echo$farm->Province;?></td>
                                <td><?php echo$farm->District;?></td>
                                <td><?php echo$farm->SubDistrict;?></td>
                                <td><?php echo$farm->Village;?></td>
                                <td align="center"><?php echo$farm->FermentationDate;?></td>
                                <td><?php echo$farm->FarmerName;?></td>
                                <td align="right"><?php echo$farm->Netto_Wet;?></td>
                                <td align="right"><?php echo number_format($farm->Netto_Dry,2);?></td>
                                <td><?php echo$farm->Name;?></td>
                                <td></td>
                            </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                <td colspan="7"></td> 
                                <td><b><?php echo number_format($totalDryBean,2);?> Kg</b></td>
                                <td colspan="2"></td>
                                
                            </tr>
                        </table>
                        <br>
                        
                    </div>
                </div>
            </div>
        </div> 

        <!-- cut test beans  -->
        <?php 
        foreach ($Evaluation as $ev) {
        ?>
        <div class="page"> 
            <div id="templatemo_container_wrapper">
                <div id="templatemo_container" style="margin-top: -50px;border:3px solid #000">
                   
                    <div style="background-color:#7bc200;color:white;font-size:18px;padding:5px;">
                        <center><b>Kudeungo Sugata <i>Cut Test Beans</i></b></center>
                    </div>
                    <div style="border-top: 3px solid;"></div>
                    <table class="table-print" width="100%">
                        <tr>
                            
                            <td width="60" rowspan="6" style="background-color:#d3d3d3;"><b>Source Bean</b></td>
                            <td width="390px">Asal Biji</td>
                            <td>: <?php echo$ev->Province;?></td>
                        </tr>
                        <tr>
                            <td >District</td>
                            <td>: <?php echo$ev->District;?></td>
                        </tr>
                        <tr>
                            <td >Petugas Fermentasi</td>
                            <td>:  <?php echo$ev->Name;?></td>
                        </tr>
                        <!-- <tr>
                            <td >Jenis Clone</td>
                            <td>:   TSH 858, S2, S1 dan Lokal (Dominan)</td> 
                        </tr> -->
                        <tr>
                            <td > Lama Fermentasi</td>
                            <td>: <?php echo $ev->LamaFermentasi?$ev->LamaFermentasi.' Hari':'-';?> <!-- 5 Hari (Pembalikan setelah 48 Jam) --> </td>
                        </tr>
                    </table>
                    <table class="table-print" width="100%">
                        <tr>
                            <td width="60px" rowspan="5" style="background-color:#d3d3d3"><b>Spec Bean</b></td>
                            <td width="390px">Jumah Biji</td>
                            <td colspan="2">: <?php echo $ev->BeanCount;?> Kg</td>
                        </tr>
                        <tr>
                            <td >Jumlah Sample</td>
                            <td colspan="2">: <?php echo $ev->SampleCount;?> Kg</td>
                        </tr>
                        <tr>
                            <td >Kadar Air</td>
                            <td colspan="2">: <?php echo $ev->KadarAir;?></td>
                        </tr>
                        <!-- <tr>
                            <td >Jenis Clone</td>
                            <td colspan="2">: TSH 858, S2, S1 dan Lokal (Dominan) </td>
                        </tr> -->
                        <tr>
                            <td >Kategori Ukuran Biji</td>
                            <td>: <b>Jumlah Biji/100 gr</b> </td>
                            <td width="80px" align="center"> <b><?php echo $ev->BeanSizeCategory;?></b></td>
                        </tr>
                    </table>

                    <table class="table-print" width="100%">
                        <tr>
                            <td width="60px" rowspan="19" style="background-color:#d3d3d3;"><b>Hasil Cut Test Bean</b></td>
                            <td width="390px" style="background-color:#d3d3d3;padding:3px"><b class="title">Parameter Pengecekan</b> </td>
                            <td style="background-color:#d3d3d3;text-align:center"><b class="title">Bean Count</b></td>
                            <td width="80px" style="background-color:#d3d3d3;text-align:center"><b class="title" >%</b></td>
                        </tr>
                        <tr>
                            <td >1. Well Fermented (Brown Colour) </td>
                            <td  align="center"><?php echo $ev->WellFermentedBrown;?></td>
                            <td  align="right"><?php echo $WellFermentedBrownPersen= Persentase($ev->WellFermentedBrown,$ev->BeanSizeCategory);?></td>
                        </tr>
                        <tr>
                            <td >2. Well Fermented (Pale = Pucat) </td>
                            <td  align="center"><?php echo $ev->WellFermentedPale;?></td>
                            <td  align="right"><?php echo $WellFermentedPalePersen= Persentase($ev->WellFermentedPale,$ev->BeanSizeCategory);?></td>
                        </tr>
                        <tr>
                            <td >3. Brown Turning Violet (Sedikit Ungu)  </td>
                            <td  align="center"><?php echo$ev->BrownTurningViolet;?></td>
                            <td  align="right"><?php echo $BrownTurningVioletPersen = Persentase($ev->BrownTurningViolet,$ev->BeanSizeCategory);?></td>
                        </tr>
                        <tr>
                            <td >4. Slaty Brown (Tekstur flat warna coklat) </td>
                            <td  align="center"><?php echo$ev->SlatyBrown;?></td>
                            <td  align="right"><?php echo $SlatyBrownPersen= Persentase($ev->SlatyBrown,$ev->BeanSizeCategory);?></td>
                        </tr>

                        <tr>
                            <td  style="background-color:#7bc200;padding:3px"><b>Total Biji Kakao Baik</b> </td>
                            <td style="background-color:#7bc200;padding:3px;text-align:center">
                                <b><?php echo $TotalBijiBaik = $ev->WellFermentedBrown+$ev->WellFermentedPale+$ev->BrownTurningViolet+$ev->SlatyBrown;?>
                                </b>
                            </td>
                            <td style="background-color:#7bc200;padding:3px;text-align:right">
                                <b><?php echo $TotalBijiBaikPersen=$WellFermentedBrownPersen+$WellFermentedPalePersen+$BrownTurningVioletPersen+$SlatyBrownPersen;?></b>
                            </td>
                        </tr>
                        <tr>
                            <td >5. Slighty Over Fermented (Hitam) </td>
                            <td  align="center"><?php echo$ev->SlightyOverFermented;?></td>
                            <td  align="right"><?php echo $SlightyOverFermentedPersen= Persentase($ev->SlightyOverFermented,$ev->BeanSizeCategory);?></td>
                        </tr>
                        <tr>
                            <td >6. Violet (Fermentasi kurang Sempurna)  </td>
                            <td  align="center"><?php echo$ev->Violet;?></td>
                            <td  align="right"><?php echo $VioletPersen= Persentase($ev->Violet,$ev->BeanSizeCategory);?></td>
                        </tr>
                        <tr>
                            <td >7. Slaty Violet (Tekstur flat warna ungu) </td>
                            <td  align="center"><?php echo$ev->SlatyViolet;?></td>
                            <td  align="right"><?php echo $SlatyVioletPersen= Persentase($ev->SlatyViolet,$ev->BeanSizeCategory);?></td>
                        </tr>
                        <tr>
                            <td >8. Slaty Pale (Tekstur flat warna pucat)  </td>
                            <td  align="center"><?php echo$ev->SlatyPale;?></td>
                            <td  align="right"><?php echo $SlatyPalePersen= Persentase($ev->SlatyPale,$ev->BeanSizeCategory);?></td>
                        </tr>
                        <tr>
                            <td >9. Gray Turning violet (abu-abu sedikit ungu)  </td>
                            <td  align="center"><?php echo$ev->GrayTurningViolet;?></td>
                            <td  align="right"><?php echo $GrayTurningVioletPersen= Persentase($ev->GrayTurningViolet,$ev->BeanSizeCategory);?></td>
                        </tr>

                        <tr>
                            <td >10. Violet Turning Brown (Sedikit Coklat)  </td>
                            <td  align="center"><?php echo$ev->VioletTurningBrown;?></td>
                            <td  align="right"><?php echo $VioletTurningBrownPersen= Persentase($ev->VioletTurningBrown,$ev->BeanSizeCategory);?></td>
                        </tr>
                        <tr>
                            <td >11. Mouldy (Berjamur, ada bintik putih di dalam)  </td>
                            <td  align="center"><?php echo$ev->Mouldy;?></td>
                            <td  align="right"><?php echo $MouldyPersen= Persentase($ev->Mouldy,$ev->BeanSizeCategory);?></td>
                        </tr>
                        <tr>
                            <td >12. Infested (ada hama atau biji berlubang)   </td>
                            <td  align="center"><?php echo$ev->Infested;?></td>
                            <td  align="right"><?php echo $InfestedPersen= Persentase($ev->Infested,$ev->BeanSizeCategory);?></td>
                        </tr>

                        <tr>
                            <td  style="background-color:#ffff31;padding:3px"><b>Total Biji tidak Baik</b> </td>
                            <td style="background-color:#ffff31;padding:3px;text-align:center">
                                <b><?php echo$TotalBijiTdkBaik= $ev->SlightyOverFermented+$ev->Violet+$ev->SlatyViolet+$ev->SlatyPale+
                                         $ev->GrayTurningViolet+$ev->VioletTurningBrown+$ev->Mouldy+$ev->Infested;?> </b>
                            </td>
                            <td style="background-color:#ffff31;padding:3px;text-align:right">
                                <b><?php echo$TotalBijiTdkBaikPersen = $SlightyOverFermentedPersen+$VioletPersen+$SlatyVioletPersen+
                                     $SlatyPalePersen+$GrayTurningVioletPersen+$VioletTurningBrownPersen+$MouldyPersen+$InfestedPersen;?></b>
                            </td>
                        </tr>

                        <tr>
                            <td >Beans Cluster (Biji Menyatu)   </td>
                            <td  align="center"><?php echo$ev->BeansCluster;?></td>
                            <td  align="right"><?php echo $BeansClusterPersen= Persentase($ev->BeansCluster,$ev->BeanSizeCategory);?></td>
                        </tr>
                        <tr>
                            <td>Flat Beans (Gepeng)   </td>
                            <td  align="center"><?php echo$ev->FlatBeans;?></td>
                            <td  align="right"><?php echo $FlatBeansPersen= Persentase($ev->FlatBeans,$ev->BeanSizeCategory);?></td>
                        </tr>
                        <tr>
                            <td >Germinated Beans (Biji Berkecambah)  </td>
                            <td  align="center"><?php echo$ev->GerminatedBeans;?></td>
                            <td  align="right"><?php echo $GerminatedBeansPersen= Persentase($ev->GerminatedBeans,$ev->BeanSizeCategory);?></td>
                        </tr>
                        <tr>
                            <td width="50%" style="background-color:#ff0000;padding:3px"><b>Total Biji Kakao Tidak Bisa di Pakai</b> </td>
                            <td style="background-color:#ff0000;padding:3px;text-align:center">
                                <b><?php echo$TotalBijiTdkPakai =$ev->BeansCluster+$ev->FlatBeans+$ev->GerminatedBeans;?></b>
                            </td>
                            <td style="background-color:#ff0000;padding:3px;text-align:right">
                                <b><?php echo$TotalBijiTdkPakaiPersen=$BeansClusterPersen+$FlatBeansPersen+$GerminatedBeansPersen;?> </b>
                            </td>
                        </tr>
                    </table>
                    <table class="table-print" width="100%">
                        <tr>
                            <td  width="448px" style="background-color:#d3d3d3;padding:3px;text-align:right"><b class="title">Total</b> </td>
                            <td style="background-color:#d3d3d3;text-align:center">
                                <b class="title"><?php echo $total = $TotalBijiBaik+$TotalBijiTdkBaik+$TotalBijiTdkPakai;?></b>
                            </td>
                            <td width="80px" style="background-color:#d3d3d3;text-align:center">
                                <b class="title" ><?php echo$TotalPersen=$TotalBijiBaikPersen+$TotalBijiTdkBaikPersen+$TotalBijiTdkPakaiPersen;?> </b>
                            </td>
                        </tr>
                    </table>
                    
                   
                </div>
            </div>
        </div>
        <div class="page-break"></div>

        <?php
        }
        ?>
        


        <!-- tabel fermentasi -->
            <div id="templatemo_container_wrapper">
                <div id="templatemo_container" style="margin-top: -50px;border:3px solid #000">
                    <div id="">
                        <table width="100%" border="0">
                            <tr>
                                <td width="250px" align="center" style="vertical-align:middle;">
                                    <?php 
                                        if($batch->LogoCompany!=''){ echo '<img src="'.base_url().$batch->LogoCompany.'" width="100" height="100">';}
                                        else{echo '<img src="'.base_url('images/kudeng1.png').'" width="250" height="100">';}
                                    ?>
                                </td>
                                <td width="auto"></td>
                                <td width="300px height=""  align="left" style="vertical-align:middle;font-size: 11px;">
                                    
                                    <table border='0' width="100%">
                                        <tr valign="top">
                                            <th ><b><strong><span >Office</span></strong></b></th>
                                            <td>:</td><td> Jl.Biruen-Takengon Km.6 Desa Juli Mee Teungoh Kec.juli Kabupaten Biruen Provinsi Aceh 24250</td>
                                        </tr>
                                        <tr>
                                            <th><b><strong>Phone</strong></b></th>
                                            <td>:</td><td> 0811 6842522</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>:</td><td>  kudengosugata@gmail.com</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>   
                        </table>
                    </div>
                    <div style="border-top: 3px solid; padding-top: 5px;"></div>
                    <center><b class="title">Tabel Temperature Fermentasi</b></center>
                    <div style="border-top: 3px solid; padding-top: 5px;"></div>
                    
                    <div class="text_area">
                        <table>
                            <tr>
                                <td width="130px">Nama Petani</td><td>:</td>
                                <td>Hassanudin</td>
                                <td width="25%"><td>
                                <td width="130px">Kabupaten</td><td>:</td>
                                <td>Aceh Tenggara</td>
                            </tr>
                            <tr>
                                <td width="130px">Kelompok</td><td>:</td>
                                <td>Aramiko</td>
                                <td width="50px"><td>
                                <td width="130px">Media Fermentasi</td><td>:</td>
                                <td>Kotak</td>
                            </tr>
                            <tr>
                                <td width="130px">Desa</td><td>:</td>
                                <td>Cinta Damai</td>
                                <td width="50px"><td>
                                <td width="130px">Jumlah Biji Basah</td><td>:</td>
                                <td><b>669,5 Kg</b></td>
                            </tr>
                            <tr>
                                <td width="130px">Kecamatan</td><td>:</td>
                                <td>Kuning</td>
                                <td width="50px"><td>
                                <td width="130px">Jumlah Biji Kering</td><td>:</td>
                                <td><b>228 Kg</b></td>
                            </tr>
                            
                        </table>
                        <table class="table-print" width="100%">
                            <tr>
                                <TH >Tanggal</TH>
                                <TH>Hari</TH>
                                <TH>No.</TH>
                                <TH>Pukul</TH>
                                <TH>Temperature &#8451</TH>
                                <TH>Keterangan</TH>
                            </tr>

                            <tr class="text-center">
                                <td>20/08/2019</td>
                                <td>Selasa</td>
                                <td>0</td>
                                <td>18.00</td>
                                <td>20.5</td>
                                <td class="text-left">Biji Kakao Masuk kedalam Kotak</td>
                            </tr>
                            <tr class="text-center">
                                <td>21/08/2019</td>
                                <td>Rabu</td>
                                <td>1</td>
                                <td>07.00</td>
                                <td>26.6</td>
                                <td class="text-left">Hari Pertama</td>
                            </tr>
                            <tr class="text-center">
                                <td>20/08/2019</td>
                                <td>Selasa</td>
                                <td>0</td>
                                <td>18.00</td>
                                <td>20.5</td>
                                <td class="text-left">Biji Kakao Masuk kedalam Kotak</td>
                            </tr>
                            <tr class="text-center">
                                <td>20/08/2019</td>
                                <td>Selasa</td>
                                <td>0</td>
                                <td>18.00</td>
                                <td>20.5</td>
                                <td class="text-left">Biji Kakao Masuk kedalam Kotak</td>
                            </tr>
                            
                        </table>

                        
                      
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="page-break"></div>

       
         <div class="page"> 
            <div id="templatemo_container_wrapper">
                <div id="templatemo_container" style="margin-top: -50px;border:3px solid #000">
                    <div id="">
                        <table width="100%" border="0">
                            <tr>
                                <td width="230px" align="center" style="vertical-align:middle;">
                                    <?php 
                                        if($batch->LogoCompany!=''){ echo '<img src="'.base_url().$batch->LogoCompany.'" width="100" height="100">';}
                                        else{echo '<img src="'.base_url('images/kudeng1.png').'" width="250" height="100">';}
                                    ?>
                                </td>
                                <td width="auto"></td>
                                <td width="300px height=""  align="left" style="vertical-align:middle;font-size: 11px;">
                                    
                                    <table border='0' width="100%">
                                        <tr valign="top">
                                            <th ><b><strong><span >Office</span></strong></b></th>
                                            <td>:</td><td> Jl.Biruen-Takengon Km.6 Desa Juli Mee Teungoh Kec.juli Kabupaten Biruen Provinsi Aceh 24250</td>
                                        </tr>
                                        <tr>
                                            <th><b><strong>Phone</strong></b></th>
                                            <td>:</td><td> 0811 6842522</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>:</td><td>  kudengosugata@gmail.com</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>   
                        </table>
                    </div>
                    <div style="border-top: 3px solid; padding-top: 5px;"></div>
                    <center><b class="title">Tabel Temperature Fermentasi</b></center>
                    <div style="border-top: 3px solid; padding-top: 5px;"></div>
                    
                    <div class="text_area">
                        <table>
                            <tr>
                                <td width="130px">Nama Petani</td><td>:</td>
                                <td>Hassanudin</td>
                                <td width="25%"><td>
                                <td width="130px">Kabupaten</td><td>:</td>
                                <td>Gayo Lues</td>
                            </tr>
                            <tr>
                                <td width="130px">Kelompok</td><td>:</td>
                                <td>Aramiko</td>
                                <td width="50px"><td>
                                <td width="130px">Media Fermentasi</td><td>:</td>
                                <td>Kotak</td>
                            </tr>
                            <tr>
                                <td width="130px">Desa</td><td>:</td>
                                <td>Marpunge</td>
                                <td width="50px"><td>
                                <td width="130px">Jumlah Biji Basah</td><td>:</td>
                                <td><b>537 Kg</b></td>
                            </tr>
                            <tr>
                                <td width="130px">Kecamatan</td><td>:</td>
                                <td>Putri Betung</td>
                                <td width="50px"><td>
                                <td width="130px">Jumlah Biji Kering</td><td>:</td>
                                <td><b>172 Kg</b></td>
                            </tr>
                            
                        </table>
                        <table class="table-print" width="100%">
                            <tr>
                                <TH >Tanggal</TH>
                                <TH>Hari</TH>
                                <TH>No.</TH>
                                <TH>Pukul</TH>
                                <TH>Temperature &#8451</TH>
                                <TH>Keterangan</TH>
                            </tr>

                            <tr class="text-center">
                                <td>20/08/2019</td>
                                <td>Selasa</td>
                                <td>0</td>
                                <td>18.00</td>
                                <td>20.5</td>
                                <td class="text-left">Biji Kakao Masuk kedalam Kotak</td>
                            </tr>
                            <tr class="text-center">
                                <td>21/08/2019</td>
                                <td>Rabu</td>
                                <td>1</td>
                                <td>07.00</td>
                                <td>26.6</td>
                                <td class="text-left">Hari Pertama</td>
                            </tr>
                            <tr class="text-center">
                                <td>20/08/2019</td>
                                <td>Selasa</td>
                                <td>0</td>
                                <td>18.00</td>
                                <td>20.5</td>
                                <td class="text-left">Biji Kakao Masuk kedalam Kotak</td>
                            </tr>
                            <tr class="text-center">
                                <td>20/08/2019</td>
                                <td>Selasa</td>
                                <td>0</td>
                                <td>18.00</td>
                                <td>20.5</td>
                                <td class="text-left">Biji Kakao Masuk kedalam Kotak</td>
                            </tr>
                            
                        </table>

                        
                      
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="page-break"></div>




    </body>
</html>
