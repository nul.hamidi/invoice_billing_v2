<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('layouts/alert'); ?>
            <?php // echo '<pre>'; print_r($this->session->userdata()); ?>

            <div class="nav-tabs-custom-aqua">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Detail Invoice</h3>
                        <div class="pull-right">
                            <a target ="blank" href="<?= base_url('report/preview/'.$this->uri->segment(3));?>"><button class='btn btn-warning btn-sm'><span class='glyphicon glyphicon-print'></span> Print Dokumen</button></a>
                        </div>
                    </div>
                </div>
                <div class="tab-content"  >

                    <!-- /.tab-pane -->
                        <!-- <label>Detail Invoice</label>
                        <div class="pull-right">
                            <a target ="blank" href="<?= base_url('report/preview/'.$this->uri->segment(3));?>"><button class='btn btn-warning btn-sm'><span class='glyphicon glyphicon-print'></span> Print Dokumen</button></a>
                        </div><hr> -->
                        <!-- <div class="pull-right">
                            <a data-toggle='modal' data-target='#upload_dok' class="btn btn-default btn-sm btn-flat">
                                <i class="fa fa-upload"></i> Import Excel
                            </a>
                        </div> -->
                        
                    

                        <div class="row">
                            <div class="col-lg-12">
                                <table width="20%">
                                    <tr>
                                        <td>No Invoice</td>
                                        <td>:</td>
                                        <td>&nbsp;<?= $no_invoice;?></td>
                                    </tr>
                                   
                                    <tr>
                                        <td>Kode Project</td>
                                        <td>:</td>
                                        <td>&nbsp;<?= $kode_project;?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <br><br>
                    <div class="tab-pane active">

                        <div class="row">
                            <div class="col-md-12">
                                <caption><b>List Detail Penambahan</b></caption>
                                <div class="pull-right">
                                    <a data-toggle='modal' data-target='#AddModal'><button class='btn btn-info btn-sm'><span class='glyphicon glyphicon-plus'></span> Tambah</button></a>
                                </div><br><br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table  class="table table-striped table-bordered table-hover" id="invoice-plus" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="10">No</th>
                                            <th width="20%">Deskripsi</th>
                                            <th >Qty</th>
                                            <th >Speed</th>
                                            <th >Tgl Awal</th>
                                            <th >Tgl Akhir</th>
                                            <th >OTC</th>
                                            <th >MRC</th>
                                            <th >CPE</th>
                                            <th >Jml Biaya</th>
                                            <th width="70">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        $no=0;
                                        $total=0;
                                        foreach ($detail_plus as $key) {
                                        $no++;
                                        $jml_biaya = ($key->jumlah*(int)$key->otc) + ($key->jumlah*(int)$key->mtc) + ($key->jumlah* (int) $key->cpe);
                                        $total = $total+$jml_biaya;
                                    ?>
                                        <tr>
                                            <td><?php echo$no;?></td>
                                            <td><?php echo$key->deskripsi;?></td>
                                            <td><?php echo number_format($key->jumlah,'0','.','.');?></td>
                                            <td><?php echo $key->speed;?></td>
                                            <td><?php echo date('d/m/Y',strtotime($key->tgl_awal));?></td>
                                            <td><?php echo date('d/m/Y',strtotime($key->tgl_akhir));?></td>
                                            <td><?php echo number_format($key->otc,'0','.','.');?></td>
                                            <td><?php echo number_format($key->mtc,'0','.','.');?></td>
                                            <td><?php echo number_format($key->cpe,'0','.','.');?></td>
                                            <td><?php echo number_format($jml_biaya,'0','.','.');?></td>
                                            <td>
                                                <button class="btn btn-info btn-xs edit-penambahan" data-toggle="modal" title="edit" 
                                                    data-deskripi='<?= $key->deskripsi;?>' 
                                                    data-qty='<?= number_format($key->jumlah,'0','.','.');?>' 
                                                    data-speed='<?= $key->speed;?>'
                                                    data-tgl_awal='<?= date('d-m-Y',strtotime($key->tgl_awal));?>'
                                                    data-tgl_akhir='<?= date('d-m-Y',strtotime($key->tgl_akhir));?>'
                                                    data-otc='<?= number_format($key->otc,'0','.','.');?>'
                                                    data-mtc='<?= number_format($key->mtc,'0','.','.');?>'
                                                    data-cpe='<?= number_format($key->cpe,'0','.','.');?>'
                                                    data-value='<?= encrypt_url($key->detail_id); ?>'
                                                    data-invoice='<?= encrypt_url($key->invoice_id); ?>'
                                                    data-target='#EditModalPenambahan'

                                                ><i class="fa fa-edit"></i></button>
                                                
                                                <button class="btn btn-danger btn-xs hapus-penambahan" data-toggle="tooltip" title="hapus-penambahan" value="<?= encrypt_url($key->detail_id); ?>"  data-invoice='<?= encrypt_url($key->invoice_id); ?>'><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    ?>
                                    <tr>
                                        <td colspan='9' align="right"><b>Total</b></td>
                                        <td colspan="2"><?php echo number_format($total,0,',','.');?></td>

                                    </tr>
                                   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <caption><b>Ticketing</b></caption>
                                <div class="pull-right">
                                    <a data-toggle='modal' data-target='#AddModalMinus'><button class='btn btn-info btn-sm'><span class='glyphicon glyphicon-plus'></span> Tambah</button></a>
                                </div><br><br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table  class="table table-striped table-hover table-bordered" id="invoice-minus" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="10">No</th>
                                            <th width="20%">Deskripsi</th>
                                            <th >Qty</th>
                                            <th >Speed</th>
                                            <th >Tgl Awal</th>
                                            <th >Tgl Akhir</th>
                                            <th>Type</th>
                                            <th >Biaya</th>
                                            <!-- <th >MTC/Biaya</th>
                                            <th >CPE/Prorate</th> -->
                                            <th >Jml Biaya</th>
                                            <th width="70">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        $no=0;
                                        $total_min =0;
                                        $jml_biaya=0;
                                        $hitung_nilai_penambahan =0;
                                        $hitung_nilai_pengurangan =0;
                                        $total_biaya = 0;
                                        foreach ($detail_minus as $key) {

                                            if($key->type=="penambahan"){
                                                $jml_biaya = $key->jumlah*$key->biaya;
                                                $hitung_nilai_penambahan+=$jml_biaya;
                                            }else{
                                                $jml_biaya = $key->jumlah*$key->biaya;
                                                $hitung_nilai_pengurangan+=$jml_biaya;
                                            }
                                        $no++;
                                        //$jml_biaya = $key->jumlah* ( (int)$key->otc + (int)$key->mtc + (int) $key->cpe );
                                        
                                        //$total_min=$total_min+$jml_biaya;
                                    ?>
                                        <tr>
                                            <td><?php echo$no;?></td>
                                            <td><?php echo$key->deskripsi;?></td>
                                            <td><?php echo number_format($key->jumlah,'0','.','.');?></td>
                                            <td><?php echo $key->speed;?></td>
                                            <td><?php echo date('d/m/Y',strtotime($key->tgl_awal));?></td>
                                            <td><?php echo date('d/m/Y',strtotime($key->tgl_akhir));?></td>
                                            <td><?php echo ucfirst($key->type);?></td>
                                            <td><?php echo number_format($key->biaya,'0','.','.');?></td>
                                            <!-- <td><?php echo number_format($key->mtc,'0','.','.');?></td>
                                            <td><?php echo number_format($key->cpe,'0','.','.');?></td> -->
                                            <td><?php echo number_format($jml_biaya,'0','.','.');?></td>
                                            <td>
                                                <button class="btn btn-info btn-xs edit-pengurangan" data-toggle="modal" 
                                                    data-deskripi='<?= $key->deskripsi;?>' 
                                                    data-qty='<?= number_format($key->jumlah,'0','.','.');?>' 
                                                    data-biaya='<?= number_format($key->biaya,'0','.','.');?>' 
                                                    data-speed='<?= $key->speed;?>'
                                                    data-type='<?= $key->type;?>'
                                                    data-tgl_awal='<?= date('d-m-Y',strtotime($key->tgl_awal));?>'
                                                    data-tgl_akhir='<?= date('d-m-Y',strtotime($key->tgl_akhir));?>'
                                                    data-value='<?= encrypt_url($key->detail_id); ?>'
                                                    data-invoice='<?= encrypt_url($key->invoice_id); ?>'
                                                    data-target='#EditModalPengurangan'
                                                    title="edit"><i class="fa fa-edit"></i></button>
                                                
                                                <button class="btn btn-danger btn-xs hapus-pengurangan" data-toggle="tooltip" title="hapus-pengurangan" value="<?= encrypt_url($key->detail_id); ?>"  data-invoice='<?= encrypt_url($key->invoice_id); ?>'><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    ?>

                                    <?php
                                        
                                        $kalkulasi_detail_min = $hitung_nilai_penambahan - $hitung_nilai_pengurangan;

                                        if($hitung_nilai_penambahan>0 && $hitung_nilai_pengurangan>0){
                                            
                                            $total_biaya = ($total + $hitung_nilai_penambahan) - $hitung_nilai_pengurangan;
                                        
                                        }else if($hitung_nilai_penambahan>0 && $hitung_nilai_pengurangan<=0){

                                            $total_biaya = ($total + $hitung_nilai_penambahan);

                                        }else if($hitung_nilai_penambahan<=0 && $hitung_nilai_pengurangan>0){

                                            $total_biaya = ($total - $hitung_nilai_pengurangan);
                                        
                                        }else if($hitung_nilai_penambahan==0 && $hitung_nilai_pengurangan==0){

                                            $total_biaya = ($total);
                                        }



                                        //$total_biaya = $total+$kalkulasi_detail_min;
                                        $ppn = (10/100)*$total_biaya;
                                        $biaya_keseluruhan = $total_biaya+$ppn;

                                    ?>
                                    <tr>
                                        <td colspan='8' align="right"><b>Total Ticketing</b></td>
                                        <td colspan="2"><?php echo number_format($kalkulasi_detail_min,0,',','.');?></td>
                                    </tr>
                                    <tr>
                                        <td colspan='8' align="right"><b>Total Biaya(Detail Penambahan & Ticketing)</b></td>
                                        <td colspan="2"><?php echo number_format($total_biaya,0,',','.');?></td>
                                    </tr>
                                    <tr>
                                        <td colspan='8' align="right"><b>PPN 10%</b></td>
                                        <td colspan="2"><?php echo number_format($ppn,0,',','.');?></td>
                                    </tr>
                                    <tr>
                                        <td colspan='8' align="right"><b>Total Biaya Keseluruhan</b></td>
                                        <td colspan="2"><?php echo number_format($biaya_keseluruhan,0,',','.');?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('report/import_detail_invoice');?>
<?php $this->load->view('invoice/modal_penambahan');?>
<?php $this->load->view('invoice/modal_pengurangan');?>

<script type="text/javascript">
$(document).ready(function () {
    $('body').on('hidden.bs.modal', '.modal', function () {
      $(this).removeData('bs.modal');
    });

   /* $('#invoice-plus').DataTable({});
    $('#invoice-minus').DataTable({});*/

    $('.input_mask').mask('000.000.000.000', {reverse: true});

    $('#tanggal-add-tgl-awal .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });
    $('#tanggal-add-tgl-akhir .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });
    $('#tanggal-edit-tgl-awal .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    }); 
    $('#tanggal-edit-tgl-akhir .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });

    $('#tanggal-add-minus-tgl-awal .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });
    $('#tanggal-add-minus-tgl-akhir .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });
    $('#tanggal-edit-minus-tgl-awal .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });
    $('#tanggal-edit-minus-tgl-akhir .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });
   



    $(document).on("click",".hapus-penambahan",function(){
        var detail_min = this.value;
        var invoice = $(this).attr('data-invoice');

        swal({
            title: "Yakin Hapus Data ini ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, Hapus!",
            closeOnConfirm: false
        }, function () {

            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>invoice/delete_detail_plus/",
                dataType: "JSON",
                data : "data-detail="+detail_min+"&data-inv="+invoice,
                success:function(data){
                    
                    if(data.rc=='0000'){
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Success Delete Data",
                                imageUrl: '<?= base_url("assets/img/success.png");?>'
                            }, function() {
                               location.reload();
                            });
                        }, 1000);
                    }else{
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Delete Failed",
                                imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                            }, function() {
                                location.reload();
                            });
                        }, 1000);
                    }
                    
                }

            });
           
        });
            
    });

    $(document).on("click",".hapus-pengurangan",function(){
        var detail_min = this.value;
        var invoice = $(this).attr('data-invoice');
        
        swal({
            title: "Yakin Hapus Data ini ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, Hapus!",
            closeOnConfirm: false
        }, function () {

            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>invoice/delete_detail_minus/",
                dataType: "JSON",
                data : "data-detail="+detail_min+"&data-inv="+invoice,
                success:function(data){
                    
                    if(data.rc=='0000'){
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Success Delete Data",
                                imageUrl: '<?= base_url("assets/img/success.png");?>'
                            }, function() {
                               location.reload();
                            });
                        }, 1000);
                    }else{
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Delete Failed",
                                imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                            }, function() {
                                location.reload();
                            });
                        }, 1000);
                    }
                    
                }

            });
           
        });
            
    });

    $('.edit-penambahan').click(function(){
        $('#edit-value').val($(this).attr('data-value'));
        $('#edit-cpe').val($(this).attr('data-cpe'));
        $('#edit-qty').val($(this).attr('data-qty'));
        $('#edit-speed').val($(this).attr('data-speed'));
        $('#edit-deskripsi').val($(this).attr('data-deskripi'));
        $('#edit-tgl_awal').val($(this).attr('data-tgl_awal'));
        $('#edit-tgl_akhir').val($(this).attr('data-tgl_akhir'));
        $('#edit-otc').val($(this).attr('data-otc'));
        $('#edit-mtc').val($(this).attr('data-mtc'));
        $('#edit-value-inv').val($(this).attr('data-invoice'));
    });
    $('.edit-pengurangan').click(function(){
        $('#edit-value-pengurangan').val($(this).attr('data-value'));
        //$('#edit-cpe-pengurangan').val($(this).attr('data-cpe'));
        $('#edit-qty-pengurangan').val($(this).attr('data-qty'));
        $('#edit-biaya-pengurangan').val($(this).attr('data-biaya'));
        $('#edit-speed-pengurangan').val($(this).attr('data-speed'));
        $('#edit-deskripsi-pengurangan').val($(this).attr('data-deskripi'));
        $('#edit-tgl_awal-pengurangan').val($(this).attr('data-tgl_awal'));
        $('#edit-tgl_akhir-pengurangan').val($(this).attr('data-tgl_akhir'));
        $('#edit-value-inv-pengurangan').val($(this).attr('data-invoice'));

        if($(this).attr('data-type')=="penambahan"){
            $('.type-plus').prop('checked', true);
        }else{
            $('.type-minus').prop('checked', true);

        }
    });
    



});
</script>