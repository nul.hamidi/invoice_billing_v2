<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('layouts/alert'); ?>
            <?php // echo '<pre>'; print_r($this->session->userdata()); ?>

            <div class="nav-tabs-custom-aqua">
                <ul class="nav nav-tabs">
                    
                    <?php
                    //if($this->session->userdata('subdit')['nama']=="BILLING & COLLECTION"){
                    ?>
                    
                    <!-- <li class="pull-right">
                        <a href="<?php echo site_url('invoice/tambah'); ?>" class="btn btn-default btn-flat">
                            <i class="glyphicon glyphicon-plus"></i> Tambah Data
                        </a>
                    </li> -->
                    <!-- <li class="pull-right">
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle btn-flat" type="button" data-toggle="dropdown" style="padding: 10px 15px;" data-toggle='modal' data-target='#upload_dok'><i class="fa fa-upload"></i> Import Excel
                                <span class="caret"></span>
                            </button>
                             <ul class="dropdown-menu">
                                <li><a >Project</a></li>
                                <li><a href="#">Invoice</a></li>
                                
                            </ul>
                        </div>
                    </li> -->
                    <?php 
                    //}
                    ?>

                    <!-- <li class="pull-right">
                        <a data-toggle='modal' data-target='#upload_dok' class="btn btn-default btn-flat">
                            <i class="fa fa-upload"></i> Import Excel
                        </a>
                    </li> -->
                    
                </ul>
                
                <div class="tab-content"  style="overflow-y:auto">
                    <!-- /.tab-pane -->
                    
                    <div class="tab-pane active">
                        <table  class="table table-striped" id="invoice" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th width="3%">No</th>
                                    <th width="8%">No Invoice</th>
                                    <th width="15%">Nama Klien</th>
                                    <th width="20%">Nama Proyek</th>
                                    <th width="10%">No WO</th>
                                    <th width="10%">Tanggal Invoice</th>
                                    <th width="10%">Tanggal Jth Tempo</th>
                                    <th width="10%">Nominal</th>
                                    <th width="5%">Status</th>
                                    <th width="8%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('report/import_project');?>

<script type="text/javascript">
$(document).ready(function () {
    oTable = $('#invoice').DataTable({
            processing: true, 
            serverSide: true, 
            order: [], 
            ajax: {
                url : "<?php echo site_url("invoice/get_invoice") ?>",
                type : 'get',
                          
            },
            columnDefs: [
                {
                    targets: 8,
                    orderable: false,
                    className: 'text-center',

                }

            ],
        });

    $(document).on("click",".hapus-invoice",function(){
        var encrypt = this.value;
        
        swal({
            title: "Yakin Hapus Data ini ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya, Hapus!",
            closeOnConfirm: false
        }, function () {

            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>invoice/delete/",
                dataType: "JSON",
                data : "data="+encrypt+'&ib='+$(this).attr('data-ib'),
                success:function(data){
                    
                    if(data.rc=='0000'){
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Success Delete Data",
                                imageUrl: '<?= base_url("assets/img/success.png");?>'
                            }, function() {
                               oTable.ajax.reload();
                            });
                        }, 1000);
                    }else{
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Delete Failed",
                                imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                            }, function() {
                                oTable.ajax.reload();
                            });
                        }, 1000);
                    }
                    
                }

            });
           
        });
            
    });

    $(document).on("click",".approve_invoice",function(){

       var invoice_id=$(this).attr("id");
    
        swal({
            title: "Setujui data ini ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#008d4c",
            confirmButtonText: "Ya, Approved!",
            closeOnConfirm: false
        }, function () {

            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>invoice/approve_invoice/",
                dataType: "JSON",
                data : "invoice_id="+invoice_id,
                success:function(data){
                    
                    setTimeout(function() {
                        swal({
                            title: "Notification!",
                            text: "Success, Invoice Approved",
                            imageUrl: '<?= base_url("assets/img/success.png");?>'
                        }, function() {
                            oTable.ajax.reload();
                        });
                    }, 1000);
                    
                }
            });
        });    
    });


});
</script>