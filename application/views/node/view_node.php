<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('layouts/alert'); ?>
            <?php // echo '<pre>'; print_r($this->session->userdata()); ?>

            <div class="nav-tabs-custom-aqua">
                <div class="box box-info">
                    
                    <div class="box-header with-border">
                    
                        <div class="col-lg-12">
                        <ul class="nav nav-tabs">
                            <?php $this->load->view('layouts/header_menu');?>
                            <div class="pull-right">
                                <a data-toggle='modal' data-target='#upload_excel_node'> 
                                    <button class="btn btn-info btn-sm">
                                        <i class="fa fa-upload"></i> Upload Excel Node
                                    </button>
                                </a>
                                    <button class="btn btn-info btn-sm" data-toggle='modal' data-target='#Addnode'>
                                        <i class="glyphicon glyphicon-plus"></i> Tambah Data
                                    </button>
                            </div>
                        </ul>
                            
                        </div>
                    </div>
                   

                </div>

                <div class="tab-content"  >
                     
                    <div class="tab-pane active">

                       
                        <div class="row">
                            <div class="col-md-12">
                                <table  class="table table-striped table-bordered table-hover" id="table-node" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <!-- <th width="7%">No</th> -->
                                            <th>Deskripsi</th>
                                            <th>Produk</th>
                                            <th>Speed</th>
                                            <th>Tanggal Tagih</th>
                                            <th>Tanggal Off</th>
                                            <th>OTC</th>
                                            <th>MRC</th>
                                            <th>CPE</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>                                    
                                   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                       
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-center">
                                 <button class="btn btn-info btn-sm btn-generate"><i class="fa fa-cog"></i> Generate Tagihan</button>
                            </div>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="col-md-12 tagihan-temp" style="display:none">
                            <div style="overflow-x:scroll">
                                <div id="div-tagihan-temp"></div>
                            </div>
                            <div class="text-right">
                               
                                <button type="submit" class="btn btn-success btn-sm btn-simpan-generate" value="<?php echo $id_project;?>"><i class="fa fa-save"></i> Simpan</button>
                               
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
        </div>

    </div>
    
</section>

<?php $this->load->view('node/modal_add_node');?>
<?php $this->load->view('node/modal_edit_node');?>
<?php $this->load->view('node/import_excel_node');?>

<script type="text/javascript">
    
    $(document).ready(function(){
        $('#table-generate').DataTable();

        var data_project = "<?= $id_project;?>";
      oTable = $('#table-node').DataTable({
            processing: true, 
            serverSide: true, 
            order: [], 
            ajax: {
                url : "<?php echo site_url("node/get-data-node") ?>",
                type : 'get',
                data:{
                    data: data_project
                } 


            },
            columnDefs: [
                {
                    targets: [5,6,7],
                    orderable: false,
                    className: 'text-left',

                },{
                    targets: [8],
                    orderable: false,
                    className: 'text-center',

                }

            ],
        });
        $(document).on("click",".hapus-node",function(){
            var encrypt = this.value;
            
            swal({
                title: "Yakin Hapus Data ini ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya, Hapus!",
                closeOnConfirm: false
            }, function () {

                $.ajax({
                    type : "POST",
                    url  : "<?php echo base_url();?>node/delete/",
                    dataType: "JSON",
                    data : "data="+encrypt,
                    success:function(data){
                        
                        if(data.rc=='0000'){
                            setTimeout(function() {
                                swal({
                                    title: "Notification!",
                                    text: "Success Delete Data",
                                    imageUrl: '<?= base_url("assets/img/success.png");?>'
                                }, function() {
                                   oTable.ajax.reload();
                                });
                            }, 1000);
                        }else{
                            setTimeout(function() {
                                swal({
                                    title: "Notification!",
                                    text: "Delete Failed",
                                    imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                                }, function() {
                                     oTable.ajax.reload();
                                });
                            }, 1000);
                        }
                        
                    }

                });
               
            });
                
        });

        $(document).on("click",".btn-simpan-generate",function(){
 
            var project_id = this.value;
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>node/simpan_generate_tagihan/",
                dataType: "JSON",
                data : "data="+project_id,
                success:function(response){
                    console.log(response.rc);

                     if(response.rc=='0000'){
                            setTimeout(function() {
                                swal({
                                    title: "Notification!",
                                    text: "Success simpan tagihan",
                                    imageUrl: '<?= base_url("assets/img/success.png");?>'
                                }, function() {
                                   window.location.href = "<?= site_url('tagihan/data_tagihan/'.$id_project);?>";
                                });
                            }, 1000);
                        }else{
                            setTimeout(function() {
                                swal({
                                    title: "Notification!",
                                    text: "Gagal simpan tagihan",
                                    imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                                }, function() {
                                    location.reload();
                                });
                            }, 1000);
                        }
                    /*console.log(JSON.parse(response));*/
                    /*$.each(JSON.parse(response), function( index, item ) {*/
                        //console.log(item.rc);
                        /*$('#div-tagihan-temp').html(response.output);
                        $('.tagihan-temp').show();*/
                        
                    //});
                    
                }
            });
        });

        $(document).on("click",".btn-update",function(){
            $('#edit-deskripsi').val($(this).attr('data-deskripsi'));
            $('#edit-value').val($(this).attr('data-value'));
            $('#edit-cpe').val($(this).attr('data-cpe'));
            $('#edit-speed').val($(this).attr('data-speed'));
            $('#edit-tgl_tagih').val($(this).attr('data-tgl_tagih'));
            $('#edit-tgl_off').val($(this).attr('data-tgl_off'));
            $('#edit-otc').val($(this).attr('data-otc'));
            $('#edit-mrc').val($(this).attr('data-mrc'));
            $("#edit-pilih_produk").val($(this).attr('data-produk'));
            $('#edit-pilih_produk').select2().trigger('change');
        });

        $(document).on("click",".btn-generate",function(){
            var segmen = "<?= $this->uri->segment(3)?>"
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url();?>node/generate_tagihan/",
                dataType: "JSON",
                data : "data="+segmen,
                success:function(response){
                    //console.log(response.rc);
                    /*console.log(JSON.parse(response));*/
                    /*$.each(JSON.parse(response), function( index, item ) {*/
                        //console.log(item.rc);
                        $('#div-tagihan-temp').html(response.output);
                        $('.tagihan-temp').show();
                        
                    //});
                    
                }

            });
        });
    });
</script>