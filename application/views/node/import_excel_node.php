<style type="text/css">
    .panel-info>.panel-heading {
        background-color: #00c0ef !important;
        border-color: #00acd6 !important;
        color: #fff;
    }
</style>
<div class="modal fade" id="upload_excel_node" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="defaultModalLabel">Import Data</h4>
            </div>
            <div class="modal-body">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h5 class="panel-title">
                            <b>Import Excel Node</b>
                            <span class="pull-right clickable" data-menu="penlok"><a data-toggle="collapse" href="#collapseOne"><i class="glyphicon glyphicon-chevron-down" style="color:white"></i></a></span>
                        </h5>
                    </div>
                    <div id="collapseOne" class="panel-collapse ">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <form action="#" id="form-invoice"  enctype="multipart/form-data">
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <label>File Excel Node</label>
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="file" name="node" id="file-project">
                                                    <input type="hidden" name="input-file-node" value="node">
                                                    <input type="hidden" name="project_id" value="<?= $this->uri->segment(3)?>">
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row clearfix">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <!-- <label>&nbsp;&nbsp;</label> -->
                                                    <button type="submit" class="btn btn-primary btn-md" id="btn-node">Upload</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="progress pgrs-project" style="display: none;">
                                        <div class="progress-bar progress-bar-striped active persentase-project" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">0%</div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#file-project').filestyle({
        btnClass : 'btn-success',
        text : 'Select File',
        htmlIcon : '<span class="fa fa-folder"></span> ',
    }); 
    

    $("#btn-node").click(function(e){
        //alert();
        e.preventDefault();

        
        $(".tbody-content").html('');
        $(".persentase-project").attr('aria-valuenow','0');
        $(".persentase-project").css('width','0%');
        $(".persentase-project").html('0%');
        $(".pgrs-project").show();
        $.ajax({
            xhr: function() {
                var xhr = new window.XMLHttpRequest();

                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                        console.log(percentComplete);
                        $(".persentase-project").attr('aria-valuenow',percentComplete);
                        $(".persentase-project").css('width',percentComplete+'%');
                        $(".persentase-project").html(percentComplete+'%');
                        if (percentComplete === 100) {
                            setTimeout(function() {
                              $(".pgrs-project").fadeOut('slow');
                              $('#form-invoice')[0].reset();
                            }, 5000);

                        }

                    }
                }, false);

                return xhr;
            },
            url: "<?= base_url('node/import_node');?>",
            type: "POST",
            data:  new FormData($("#form-invoice")[0]),
            contentType: false,
            cache: false,
            processData:false,
            success: function(response) {
                
                $.each(JSON.parse(response), function( index, item ) {
                    if(item.rc=='0000'){
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Success Import Data",
                                imageUrl: '<?= base_url("assets/img/success.png");?>'
                            }, function() {
                               oTable.ajax.reload();
                            });
                        }, 1000);

                    }else if(item.rc=='0005'){
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Gagal import data",
                                imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                            }, function() {
                                oTable.ajax.reload();
                            });
                        }, 1000);
                    }else if(item.rc=='0001'){
                        setTimeout(function() {
                            swal({
                                title: "Notification!",
                                text: "Terjadi kesalahan Database / File!",
                                imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                            }, function() {
                                oTable.ajax.reload();
                            });
                        }, 1000);
                    }
                });
            }          
        });
        
        return false;
    });

</script>