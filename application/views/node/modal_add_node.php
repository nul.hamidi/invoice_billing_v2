<!-- Modal -->
<div class="modal fade" id="Addnode" tabindex="-1" role="dialog" aria-hidden="true" enctype="multipart/form-data">
    <div class="modal-dialog modal-md" >
        <div class="modal-content">
            <div class="modal-header bg-info">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="false">&times;</button>
                <h4 class="modal-title" id="myModalLabel"> Tambah Node</h4>
            </div>
            <div class="modal-body">
                <div>
                <form method="post" action="<?php echo $action_url_add;?>" class="form-horizontal form-add" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Produk</label>
                                    <div class="col-sm-9">
                                        <select class="select2 form-control" name="pilih_produk" id="pilih_produk">
                                            <option value=""></option>
                                           <?php foreach ($select_produk as $value) { ?>
                                                
                                                <option value='<?= encrypt_url($value->produk_id); ?>'><?= $value->nama_produk;?></option>
                                            <?php
                                                }
                                            ?>  
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Deskripsi</label>
                                    <div class="col-sm-9">
                                        <textarea  name="deskripsi" id="deskripsi" placeholder="" class="form-control input-sm"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Speed</label>
                                    <div class="col-sm-9">
                                        <input type="text"  name="speed" id="speed" placeholder="" class="form-control input-sm">
                                    </div>
                                </div>
                                
                                <div class="form-group">                    
                                    <label class="col-sm-3 control-label">Tgl Tagih</label>
                                    <div class="col-sm-9" id="tanggal">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input autocomplete="off" type="text" class="form-control input-sm" name="tgl_tagih" id="tgl_awal" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">                    
                                    <label class="col-sm-3 control-label">Tgl Off</label>
                                    <div class="col-sm-9" id="tanggal">
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input autocomplete="off" type="text" class="form-control input-sm" name="tgl_off" id="tgl_akhir" >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">OTC</label>
                                    <div class="col-sm-9">
                                        <input type="text"  name="otc" id="otc" placeholder="" class="form-control input-sm input_mask">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">MRC</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="mrc" id="mrc" placeholder="" class="form-control input-sm input_mask">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">CPE</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="cpe" id="cpe" placeholder="" class="form-control input-sm input_mask">
                                    </div>
                                </div>
    

                            </div> <!-- end col-12 -->
                            
                        </div><!-- end row -->
                        
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        
                        <div class="">
                            <div class="pull-right"> 
                                <button type="button" class="btn btn-default" data-dismiss="modal" aria-hidden="false">Close</button>
                                <button type="submit" class="btn btn-info ">Simpan</button>
                                
                            </div>
                        </div>
                        
                    </div>
                    <!-- /.box-footer -->
                    </form>
                </div>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->

<script type="text/javascript">
    $('.select2').select2({
        placeholder: "Please Select"
    });


    $('#tanggal .input-group.date').datepicker({
        format: "d-m-yyyy",
        viewMode: "date", 
        minViewMode: "date"
    });

    $('.input_mask').mask('000.000.000.000', {reverse: true});

    $(".form-add").validate({
            rules: {
                pilih_produk: "required",
                speed: "required",
                tgl_tagih: "required",
                tgl_off: "required",
                deskripsi: "required",
                otc: "required",
                mrc: "required",
                cpe: "required",

            },
            messages: {
                pilih_produk:{
                    required:"<i class='fa fa-times'></i> Produk harus diisi"
                },
                speed:{
                    required:"<i class='fa fa-times'></i> Speed harus diisi"
                }, 
                deskripsi:{
                    required:"<i class='fa fa-times'></i> Deskripsi harus diisi"
                }, 
                tgl_tagih:{
                    required:"<i class='fa fa-times'></i> Tgl tagih harus diisi"
                },
                tgl_off:{
                    required:"<i class='fa fa-times'></i> Tgl Off harus diisi"
                }, 
                otc:{
                    required:"<i class='fa fa-times'></i> OTC harus diisi"
                }, 
                mrc:{
                    required:"<i class='fa fa-times'></i> MRC harus diisi"
                },
                cpe:{
                    required:"<i class='fa fa-times'></i> CPE harus diisi"
                },
                
            },
            highlight: function (element) {
                $(element).parent().parent().addClass("has-error")
                $(element).parent().addClass("has-error")
            },
            unhighlight: function (element) {
                $(element).parent().removeClass("has-error")
                $(element).parent().parent().removeClass("has-error")
            }
    });
</script>

<style type="text/css">
    .select2 {
width:100%!important;
}
</style>