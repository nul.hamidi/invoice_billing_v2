<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('layouts/alert'); ?>
            <?php // echo '<pre>'; print_r($this->session->userdata()); ?>

            <div class="nav-tabs-custom-aqua">
                <div class="box box-info">
                    
                    <div class="box-header with-border">
                    
                        <div class="col-lg-12">
                        <ul class="nav nav-tabs">
                            <?php $this->load->view('layouts/header_menu');?>
                        </ul>
                            
                        </div>
                    </div>
                   

                </div>

                <div class="tab-content"  >
                     
                    <div class="tab-pane active">

                       
                        <div class="row">
                            <div class="col-md-12">
                                <table  class="table table-striped table-bordered table-hover" id="table-tagihan" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <!-- <th width="7%">No</th> -->
                                            <th><input type="checkbox" name="tagihan_id[]" align="center" id="checkAll"></th>
                                            <th>Nama</th>
                                            <th>Biaya</th>
                                            <th>Periode Pemakaian</th>
                                            <th>Jumlah</th>
                                            <th>PPN (10%)</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>                                    
                                   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <input id="#array_spb_id" type="hidden">
                        
                       
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Adjusment</label>
                                <table  class="table table-striped table-bordered table-hover" id="table-tagihan-minus" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <!-- <th width="7%">No</th> -->
                                            <th>Nama</th>
                                            <th>Biaya</th>
                                            <th>Periode Pemakaian</th>
                                            <th>Jumlah</th>
                                            <th>PPN</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>                                    
                                   
                                    </tbody>
                                </table>
                            </div>

                        <div class="col-md-12">
                            <div class="text-left">
                                 <button class="btn btn-success btn-sm btn-tambah-adjusment"><i class="fa fa-plus"></i> Tambah Adjusment</button>
                            </div>
                        </div>
                    </div><br><Br>
                    <div class="row">
                        <div class="col-md-12">
                            <form action="#" id="form-validasi">
                                <div class="hidden-content" style="display: none"></div>
                                <div class="text-left">
                                     <button class="btn btn-danger btn-sm btn-generate"><i class="fa fa-cog"></i> Generate Invoice</button>
                                </div>
                            </form>
                        </div>
                    </div><br>
                    
                </div>
                <!-- /.tab-content -->
            </div>
        </div>

    </div>
    
</section>


<script type="text/javascript">

    function sum_data(value){
        
        var id = [];
        var rows = document.getElementsByName('check[]');
        var selectedRows = [];
        for (var i = 0, l = rows.length; i < l; i++) {
            if (rows[i].checked) {
                selectedRows.push(rows[i]);
                id.push(rows[i].id);
            }
        }

        //$('#array_spb_id').val(id.join());

        if(selectedRows.length>0){
            console.log(selectedRows);
            //$('.BayarSPB').show()
        }else{
            //$('.BayarSPB').hide()
        }
        
    }
    function remove_element(id_element){
        console.log(id_element);
        $("#"+id_element).remove();
    }
    
    $(document).ready(function(){

        var arr = [];

        var array_id_tagihan ='';
        var data_project = "<?= $id_project;?>";
        oTable = $('#table-tagihan').DataTable({
            initComplete: function (settings, json) {
                $('.checkbox').change(function () {
                    //var array_id_tagihan = "";
                    //alert();
                    //alert($('.checkbox:checked').length);
                    //uncheck "select all", if one of the listed checkbox item is unchecked
                    if (false == $(this).prop("checked")) { //if this item is unchecked
                        $("#checkAll").prop('checked',false); //change "select all" checked status to false
                        
                       
                    }
                    //check "select all" if all checkbox items are checked
                    if ($('.checkbox').length > 1) {
                        if ($('.checkbox:checked').length == $('.checkbox').length) {
                            $("#checkAll").prop('checked', true);
                            
                        }
                    } else {
                        $("#checkAll").prop('checked', false);
                    }

                  


                    //$('.checkbox:checked').each(function (i) {
                        if($(this).prop("checked")==true){

                            array_id_tagihan ="<input type='hidden' name='tagihan_id[]' value='"+$(this).attr('data-tagihan')+"' id='"+$(this).attr('data-checkbox')+"'>";
                            $(".hidden-content").append(array_id_tagihan);
                        }else{
                            console.log('masuk else');
                            //console.log($("#"+$(this).attr('data-tagihan')).val());
                            //$("#68").remove();
                            remove_element($(this).attr('data-checkbox'));
                            /*$("#test-d2REUURKTkMxRG9GMDBWUkxwRHI5dz09").remove();
                            $(".hidden-content").find(":hidden").filter("input[value='"+$(this).attr('data-tagihan')+"']").remove();*/
                           /* console.log($('input').attr({
                                type: 'hidden',
                                name: 'tagihan_id[]',
                                value: $(this).attr('data-tagihan')
                            }));
                            $( "input[value='"+$(this).attr('data-tagihan')+"']" ).remove();
                            $('input').attr({
                                type: 'hidden',
                                name: 'tagihan_id[]',
                                value: $(this).attr('data-tagihan')
                            }).remove();*/
                        }
                    //});

                    

                    //array_id_tagihan ='';
                   

                });
            },
            drawCallback: function (settings) {
                $('.checkbox').change(function () {
                    console.log('drawCallback');
                    //var array_id_tagihan = "";
                    //alert();
                    //alert($('.checkbox:checked').length);
                    //uncheck "select all", if one of the listed checkbox item is unchecked
                    if (false == $(this).prop("checked")) { //if this item is unchecked
                        $("#checkAll").prop('checked',false); //change "select all" checked status to false
                        
                       
                    }
                    //check "select all" if all checkbox items are checked
                    if ($('.checkbox').length > 1) {
                        if ($('.checkbox:checked').length == $('.checkbox').length) {
                            $("#checkAll").prop('checked', true);
                            
                        }
                    } else {
                        $("#checkAll").prop('checked', false);
                    }

                  


                    //$('.checkbox:checked').each(function (i) {
                        if($(this).prop("checked")==true){
                            //remove_element($(this).attr('data-checkbox'));
                            array_id_tagihan ="<input type='hidden' name='tagihan_id[]' value='"+$(this).attr('data-tagihan')+"' id='"+$(this).attr('data-checkbox')+"'>";
                            //$(".hidden-content").append(array_id_tagihan);
                        }else{
                            console.log('masuk else drawCallback');
                            //console.log($("#"+$(this).attr('data-tagihan')).val());
                            //$("#68").remove();
                            remove_element($(this).attr('data-checkbox'));
                            /*$("#test-d2REUURKTkMxRG9GMDBWUkxwRHI5dz09").remove();
                            $(".hidden-content").find(":hidden").filter("input[value='"+$(this).attr('data-tagihan')+"']").remove();*/
                           /* console.log($('input').attr({
                                type: 'hidden',
                                name: 'tagihan_id[]',
                                value: $(this).attr('data-tagihan')
                            }));
                            $( "input[value='"+$(this).attr('data-tagihan')+"']" ).remove();
                            $('input').attr({
                                type: 'hidden',
                                name: 'tagihan_id[]',
                                value: $(this).attr('data-tagihan')
                            }).remove();*/
                        }
                    //});

                    

                    //array_id_tagihan ='';
                   

                });
                
            
            },
            processing: true, 
            serverSide: true, 
            order: [], 
            ajax: {
                url : "<?php echo site_url("tagihan/get-data-tagihan") ?>",
                type : 'get',
                data:{
                    data: data_project
                } 


            },
            columnDefs: [
                {
                    targets: [0],
                    orderable: false,
                    className: 'text-center',

                },{
                    targets: [3,4,5,6],
                    orderable: false,
                    className: 'text-left',

                }

            ],
        });

        $("#form-validasi").on('submit', (function (e) {
            // alert();
            e.preventDefault();
            var ceklis = $("input[name='status_data']:checked").val();
            
            alert(ceklis);
            /*if(ceklis==1){
                $.ajax({
                url: "<?= base_url('bpjt/act_surat_bpjt');?>",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    
                },
                error: function (e) {
                   
                }
            
                });
            }
            else{
                
            }*/
            
        }));


        $("#checkAll").change(function () { //"select all" change
            //console.log('checkall');
           
            if (false == $(this).prop("checked")) { //if this item is unchecked
                //console.log('if');
                $(".checkbox").prop('checked', false); //change "select all" checked status to false
                
                $(".hidden-content").html("");
                array_id_tagihan = "";
                

            } else {
                //console.log('else');

                $(".checkbox").prop('checked', $(this).prop("checked"));
                //console.log();
                if ($('.checkbox:checked').length > 0) {

                    $('.checkbox:checked').each(function (i) {
                        array_id_tagihan +="<input type='hidden' name='tagihan_id[]' value='"+$(this).attr('data-tagihan')+"' id='"+$(this).attr('data-checkbox')+"'>";
                    });

                    $(".hidden-content").html(array_id_tagihan);
 
                    array_id_tagihan = "";
                   
                }

            }

        });
       
    });
   

</script>