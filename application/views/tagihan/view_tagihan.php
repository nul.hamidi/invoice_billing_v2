<style type="text/css">
    .panel-info>.panel-heading {
        background-color: #00c0ef !important;
        border-color: #00acd6 !important;
        color: #fff;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        /* background-color: #e4e4e4; */
        /* border: 1px solid #aaa; */
        border-radius: 4px;
        cursor: default;
        float: left;
        margin-right: 5px;
        margin-top: 5px;
        padding: 0 5px;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        /* color: #999; */
        cursor: pointer;
        display: inline-block;
        font-weight: bold;
        margin-right: 2px;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background-color: #3c8dbc !important;
        border-color: #367fa9  !important;
        padding: 1px 10px;
        color: #fff;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
        color: rgba(255, 255, 255, 0.7) !important;
    }
</style>
<section class="content">
    <!-- Main row -->
    <div class="row">
        <div class="col-md-12">
            <?php $this->load->view('layouts/alert'); ?>
            <?php // echo '<pre>'; print_r($this->session->userdata()); ?>

            <div class="nav-tabs-custom-aqua">
                <div class="box box-info">
                    
                    <div class="box-header with-border">
                    
                        <div class="col-lg-12">
                        <ul class="nav nav-tabs">
                            <?php $this->load->view('layouts/header_menu');?>
                        </ul>
                            
                        </div>
                    </div>
                   

                </div>                     

                <div class="tab-content"  >
                     
                    <div class="tab-panel active">
                   
                        <!--  <div class="form-group"> -->
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-6">
                                    
                                        <select name="bulan[]" id="filter_bulan" onchange="FilterBulanFunc()" multiple="multiple" class="form-control select2 input-md" style="width:100%">
                                        <option value=""></option>
                                        <?php foreach ($dictinct_bulan as $db) { ?>
                                            
                                            <option value='<?= $db->bulan;?>'><?= $db->bulan;?></option>
                                        <?php
                                            }
                                        ?>  
                                    </select>
                                    <input type="hidden" name="arr_filter" id="arr_filter" value="" class="form-control input-md">
                                </div>
                                <div class="col-sm-3" style="display:none">
                                  
                                    <input type="search" name="search" value="test" id="search" class="form-control input-md" placeholder="">
                                </div>
                                

                                <div class="col-sm-3">
                                    
                                    <button type="button" class="btn btn-primary btn-md btn-filter-bulan"> Cari </button>
                                    <!-- <a href="<?php echo site_url('tagihan/data_tagihan/'.$this->uri->segment(3).'/flush') ?>"
                                        class="btn btn-danger btn-md"> Bersihkan </a> -->
                                </div>
                            </div>
                        </div>
                        <!--  </div> -->
                    
                    </div>
                        
                       
                        <div class="row">
                            <div class="col-md-12">
                                
                                <table  class="table table-striped table-bordered table-hover" id="table-tagihan" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <!-- <th width="7%">No</th> -->
                                            <th><input type="checkbox"   name="tagihan_id[]" align="center" id="checkAll"></th>
                                            <th>Deskripsi</th>
                                            <th>Produk</th>
                                            <th>Biaya</th>
                                            <th>Periode Pemakaian</th>
                                            <th>Jumlah</th>
                                            <th>PPN (10%)</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>                                    
                                   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        
                       
                    </div>
                  
                    <div class="row">
                        <div class="col-md-12">
                            <form action="#" id="form-validasi">
                                <div class="hidden-content" style="display: none"></div>
                                <div class="text-left" >
                                    <input type="hidden" name="array_tagihan_id" id="array_tagihan_id">
                                    <input type="hidden" name="project_id" id="project_id" value="<?php echo $id_project;?>">
                                    <button type="button" style="display:none" class="btn btn-success btn-sm btn-generate"><i class="fa fa-cog"></i> Generate Invoice</button>
                                </div>
                            </form>
                        </div>
                    </div><br>
                    
                </div>
                <!-- /.tab-content -->
            </div>
        </div>

    </div>
    
</section>


<script type="text/javascript">

    function sum_data(){
        //alert('test');
        var id = [];
        var rows = document.getElementsByName('check[]');
        var selectedRows = [];
        for (var i = 0, l = rows.length; i < l; i++) {
            if (rows[i].checked) {
                selectedRows.push(rows[i]);
                id.push(rows[i].id);
            }
        }

        $('#array_tagihan_id').val(id.join());
        console.log(id.join());
        if(selectedRows.length>0){
            //console.log(selectedRows);
            $('.btn-generate').show()
        }else{
            $('.btn-generate').hide()
        }
        
    }

    function FilterBulanFunc() {
        var bulan = $('#filter_bulan').val();
        var realvalues = new Array();
        $('#filter_bulan :selected').each(function(i, selected) {
            realvalues[i] = $(selected).val();
        });
        $('#arr_filter').val(realvalues.join());
    }

    function remove_element(id_element){
        console.log(id_element);
        $("#"+id_element).remove();
    }
    
    $(document).ready(function(){

        var arr = [];

        $('.select2').select2({
            placeholder: "Pilih bulan"
        });

        $(".btn-filter-bulan").click(function(){
            
            oTable.ajax.reload();
        });

        var array_id_tagihan ='';
        var data_project = "<?= $id_project;?>";
        oTable = $('#table-tagihan').DataTable({
            processing: true, 
            serverSide: true,
            searching:false,
            order: [], 
            ajax: {
                url : "<?php echo site_url("tagihan/get-data-tagihan") ?>",
                type : 'get',
                data: function ( data ) {
                    data.data = data_project;
                    data.tagihan = $('#arr_filter').val(); 
               
                }

            },
            columnDefs: [
                {
                    targets: [0],
                    orderable: false,
                    className: 'text-center',

                },{
                    targets: [3,4,5,6,7],
                    orderable: false,
                    className: 'text-left',

                }

            ],
        });

        $(document).on("click",".btn-generate",function(){
            let arr_tagihan =  $('#array_tagihan_id').val();
            let project_id =  $('#project_id').val();
            
            //alert(arr_tagihan);
            swal({
                title: "Yakin Ganerate Tagihan ?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#008d4c",
                confirmButtonText: "Generate",
                closeOnConfirm: false
            }, function () {

                $.ajax({
                    type : "POST",
                    url  : "<?php echo base_url();?>invoice/generate_invoice/",
                    dataType: "JSON",
                    data : "arr_tagihan="+arr_tagihan+"&project_id="+project_id,
                    success:function(data){
                        
                        if(data.rc=='0000'){
                            //setTimeout(function() {
                                swal({
                                    title: "Notification!",
                                    text: "Success Generate Invoice",
                                    imageUrl: '<?= base_url("assets/img/success.png");?>'
                                }, function() {
                                oTable.ajax.reload();
                                $('.btn-generate').hide();
                                });
                            //}, 1000);
                        }else{
                            //setTimeout(function() {
                                swal({
                                    title: "Notification!",
                                    text: "Generate Invoice Gagal",
                                    imageUrl: '<?= base_url("assets/img/danger-red2.png");?>'
                                }, function() {
                                    oTable.ajax.reload();
                                });
                            //}, 1000);
                        } 
                    }
                });
            });   
        });

        $("#form-validasi").on('submit', (function (e) {
            // alert();
            e.preventDefault();
            var ceklis = $("input[name='status_data']:checked").val();
            
            alert(ceklis);
            /*if(ceklis==1){
                $.ajax({
                url: "<?= base_url('bpjt/act_surat_bpjt');?>",
                type: "POST",
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    
                },
                error: function (e) {
                   
                }
            
                });
            }
            else{
                
            }*/
            
        }));

        $('#checkAll').on('change', function () {
            if ($(this).prop('checked') == true) {
                oTable.$('input[type=checkbox]').prop('checked', true);
                oTable.$('input[type=checkbox]').closest('tr').css('background', '#F1F9FF');
            } else {
                oTable.$('input[type=checkbox]').prop('checked', false);
                oTable.$('input[type=checkbox]').each(function (k, v) {
                    if ($(this).closest('tr').hasClass('odd')) {
                        $(this).closest('tr').css('background', '#F7FAFF');
                    } else {
                        $(this).closest('tr').css('background', 'white');
                    }
                });
            }
            sum_data();
        });


        $("#checkAllx").change(function () { //"select all" change
            //console.log('checkall');
           
            if (false == $(this).prop("checked")) { //if this item is unchecked
                //console.log('if');
                $(".checkbox").prop('checked', false); //change "select all" checked status to false
                
                $(".hidden-content").html("");
                array_id_tagihan = "";
                

            } else {
                //console.log('else');

                $(".checkbox").prop('checked', $(this).prop("checked"));
                //console.log();
                if ($('.checkbox:checked').length > 0) {
                    
                    $('.checkbox:checked').each(function (i) {
                        array_id_tagihan +="<input type='hidden' name='tagihan_id[]' value='"+$(this).attr('data-tagihan')+"' id='"+$(this).attr('data-checkbox')+"'>";
                    });

                    $(".hidden-content").html(array_id_tagihan);
 
                    array_id_tagihan = "";
                   
                }

            }

        });
         $(document).on('click', '.panel-heading span.clickable', function (e) {
            var $this = $(this);
            if (!$this.hasClass('panel-collapsed')) {
                //$this.parents('.panel').find('.panel-body').slideUp();
                $this.addClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
            } else {
                // $this.parents('.panel').find('.panel-body').slideDown();
                $this.removeClass('panel-collapsed');
                $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
            }
        })
       
    });
   

</script>