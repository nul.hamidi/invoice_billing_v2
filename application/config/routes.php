<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['invoice/create_invoice'] = 'Invoice/tambah';
$route['invoice/saves_insert'] = 'Invoice/simpan_invoice';
$route['invoice/update_data'] = 'Invoice/ubah/';
$route['invoice/update_data/(:any)'] = 'Invoice/ubah/$1';
$route['invoice/save_update/(:any)'] = 'Invoice/update_invoice/$1';
$route['invoice/delete'] = 'Invoice/delete_invoice';
$route['invoice/detail_invoice/(:any)'] = 'Invoice/detail/$1';
$route['invoice/saves_detail_plus'] = 'Invoice/lists';
$route['invoice/saves_detail_plus/(:any)'] = 'Invoice/simpan_detail_plus/$1';
$route['invoice/saves_detail_minus/(:any)'] = 'Invoice/simpan_detail_minus/$1';
$route['invoice/delete_detail_plus'] = 'Invoice/delete_invoice_plus';
$route['invoice/delete_detail_minus'] = 'Invoice/delete_invoice_minus';
$route['invoice/saves_update_detail_minus'] = 'Invoice/update_invoice_minus';
$route['invoice/saves_update_detail_plus'] = 'Invoice/update_invoice_plus';
$route['invoice/get_wo_id/(:any)'] = 'Invoice/get_data_wo_by_id/$1';

$route['invoice/saves_otc'] = 'Invoice/simpan_invoice_tagihan';
$route['invoice/saves_cpe'] = 'Invoice/simpan_invoice_tagihan';
$route['invoice/saves_mrc'] = 'Invoice/simpan_invoice_tagihan';

$route['invoice/delete_otc'] = 'Invoice/delete_invoice_tagihan';
$route['invoice/delete_cpe'] = 'Invoice/delete_invoice_tagihan';
$route['invoice/delete_mrc'] = 'Invoice/delete_invoice_tagihan';


$route['report/preview/(:any)'] = 'Report/preview_dok/$1';
$route['report/download/(:any)'] = 'Report/download_file/$1';
$route['report/import_data_proinv'] = 'Report/import_data_proinv';
$route['report/import_data_detail_inv'] = 'Report/import_data_detinv';

$route['pelanggan/data_pelanggan'] = 'Pelanggan';
$route['pelanggan/create_pelanggan'] = 'Pelanggan/tambah';
$route['pelanggan/saves_insert'] = 'Pelanggan/simpan_pelanggan';
$route['pelanggan/update_data'] = 'Pelanggan/ubah/';
$route['pelanggan/update_data/(:any)'] = 'Pelanggan/ubah/$1';
$route['pelanggan/save_update/(:any)'] = 'Pelanggan/update_pelanggan/$1';
$route['pelanggan/delete'] = 'Pelanggan/delete_pelanggan';
$route['pelanggan/get-data-pelanggan'] = 'Pelanggan/get_pelanggan';


$route['bank/data_bank'] = 'Bank';
$route['bank/create_bank'] = 'Bank/tambah';
$route['bank/saves_insert'] = 'Bank/simpan_bank';
$route['bank/update_data'] = 'Bank/ubah/';
$route['bank/update_data/(:any)'] = 'Bank/ubah/$1';
$route['bank/save_update/(:any)'] = 'Bank/update_bank/$1';
$route['bank/delete'] = 'Bank/delete_bank';

$route['project/data_project'] = 'Project';
$route['project/create_project'] = 'Project/tambah';
$route['project/saves_insert'] = 'Project/simpan_project';
$route['project/update_data'] = 'Project/ubah/';
$route['project/update_data/(:any)'] = 'Project/ubah/$1';
$route['project/save_update/(:any)'] = 'Project/update_project/$1';
$route['project/delete'] = 'Project/delete_project';
$route['project/get-data-project'] = 'Project/get_project';
$route['project/import_project'] = 'Project/import_excel_project';

$route['node/data_node/(:any)'] = 'Node/index/$1';
$route['node/create_node'] = 'Node/tambah';
$route['node/saves_insert/(:any)'] = 'Node/simpan_node/$1';
$route['node/update_data'] = 'Node/update_node';
$route['node/save_update/(:any)'] = 'Node/update_project/$1';
$route['node/delete'] = 'Node/delete_node';
$route['node/get-data-node'] = 'Node/get_node';
$route['node/import_node'] = 'Node/import_excel_node';

$route['tagihan/data_tagihan/(:any)'] = 'Tagihan/index/$1';
$route['tagihan/data_tagihan/(:any)/flush'] = 'Tagihan/index/$1/$2';
$route['tagihan/get-data-tagihan'] = 'Tagihan/get_tagihan';